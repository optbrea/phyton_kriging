#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Common functions to classes """

from copy import deepcopy
from math import factorial
import numpy as np

from numpy import (array, exp, ones_like, pi, linspace, tile, angle, zeros)

import scipy.ndimage as ndimage
from scipy.signal import fftconvolve
from numpy.fft import fft, ifft
from scipy.ndimage import rank_filter

from . import mm


def rotate(X, Y, angle, position=(0, 0)):
    """Rotation of X,Y with respect to position

    Parameters:
        angle (float): angle to rotate, in radians
        position (float, float): position of center of rotation
    """

    x0, y0 = position

    Xrot = (X - x0) * np.cos(angle) + (Y - y0) * np.sin(angle)
    Yrot = -(X - x0) * np.sin(angle) + (Y - y0) * np.cos(angle)
    return Xrot, Yrot


def find_local_extrema(kind, y, x, pixels_interpolation=0, pixels_filter=0):
    """Determine local minima in a numpy array.

    Args:
        kind (str): 'maxima', 'minima'
        y (numpy.ndarray): variable with local minima.
        x (numpy.ndarray): x position
    Returns:
        (numpy.ndarray): i positions of local minima.

    Todo:
        Add a filter to remove noise.
    """

    if pixels_filter == 0:
        pass
    else:
        pass

    if kind == 'minima':
        y_erode = rank_filter(y, -0, size=3)
        Trues = y_erode == y
    elif kind == 'maxima':
        y_dilate = rank_filter(y, -1, size=3)
        Trues = y_dilate == y
    else:
        print("bad parameter in find_local_extrema: only 'maxima or 'minima'")

    i_pos_integer = np.where(Trues == True)[0]
    i_pos_integer = i_pos_integer[1:-1]
    x_minima = x[i_pos_integer]
    y_minima = y[i_pos_integer]

    if pixels_interpolation == 0:
        x_minima_frac = x_minima
        y_minima_frac = y_minima
    else:
        x_minima_frac = np.zeros_like(x_minima, dtype=float)
        y_minima_frac = np.zeros_like(x_minima, dtype=float)
        for i_j, j in enumerate(i_pos_integer):
            # print(j, i_j)
            js = np.array(
                np.arange(j - pixels_interpolation,
                          j + pixels_interpolation + 1))
            # print(js)
            p_j = np.polyfit(x[js], y[js], 2)
            y_minima_interp = np.poly1d(p_j)
            # print(p_j)
            x_minima_frac[i_j] = -p_j[1] / (2 * p_j[0])
            # print(y_minima_frac[i_j])

            y_minima_frac[i_j] = y_minima_interp(x_minima_frac[i_j])

    return x_minima_frac, y_minima_frac, i_pos_integer


def sampling2(xy, sampling):
    """Esta funcion pone 1 donde hay un dato y 0 en el resto tambien manda una funcion identica para los valores de x"""

    xComb = zeros(len(sampling))  # array con 1 en posiciones
    yComb = zeros(len(sampling))  # array con y(i) en posiciones
    imenores, _, _ = nearest2(sampling, xy[:, 0])  # @UnusedVariable
    iy = 0
    for i in imenores:
        xComb[i] = xComb[i] + 1
        yComb[i] = xy[iy, 1]
        iy = iy + 1
    return xComb, yComb


def samplingXY(xy_data, Sampling2D):
    """Places 1 at nearest position of a data, the rest is 0. 
    
    Args:
        xy_data ( np.array - num_data x 3 (x,y,z)): positions of data
        sampling (): 

    Returns:
        xyComb ():
        zComb  ():
    """

    X_sampling, Y_sampling = Sampling2D

    x_sampling = np.unique(X_sampling)
    y_sampling = np.unique(Y_sampling)

    x_sampling.sort()
    y_sampling.sort()

    print(x_sampling.shape)
    xyComb = np.zeros(X_sampling.shape)
    zComb = np.zeros(X_sampling.shape)

    imenores_x, _, _ = nearest2(x_sampling, xy_data[:, 0])

    jmenores_y, _, _ = nearest2(y_sampling, xy_data[:, 1])

    for k in range(len(xy_data[:, 0])):
        i = imenores_x[k]
        j = jmenores_y[k]
        xyComb[i, j] = xyComb[i, j] + 1
        zComb[i, j] = xy_data[k, 2]

    return xyComb, zComb


def distance(x1, x2):
    """Compute distance between two vectors.

    Parameters:
        x1 (numpy.array): vector 1
        x2 (numpy.array): vector 2

    Returns:
        (float): distance between vectors.
    """
    if len(x1) != len(x2):
        raise Exception('distance: arrays with different number of elements')
    else:
        return np.linalg.norm(x2 - x1)


def nearest(vector, number):
    """Computes the nearest element in vector to number.

    Parameters:
        vector (numpy.array): array with numbers
        number (float):  number to determine position

    Returns:
        (int): index - index of vector which is closest to number.
        (float): value  - value of vector[index].
        (float): distance - difference between number and chosen element.
    """
    indexes = np.abs(vector - number).argmin()
    values = vector.flat[indexes]
    distances = values - number
    return indexes, values, distances


def nearest2(vector, numbers):
    """Computes the nearest element in vector to numbers.

    Parameters:
        vector (numpy.array): array with numbers
        number (numpy.array):  numbers to determine position

    Returns:
        (numpy.array): index - indexes of vector which is closest to number.
        (numpy.array): value  - values of vector[indexes].
        (numpy.array): distance - difference between numbers and chosen elements.
    """

    indexes = np.abs(np.subtract.outer(vector, numbers)).argmin(0)
    values = vector[indexes]
    distances = values - numbers
    return indexes, values, distances


def find_extrema(array2D, x, y, kind='max', verbose=False):
    """In a 2D-array, formed by vectors x, and y, the maxima or minima are found

    Parameters:
        array2D (np. array 2D): 2D array with variable
        x (np.array 1D): 1D array with x axis
        y (np.array 1D): 1D array with y axis
        kind (str): 'min' or 'max': detects minima or maxima
        verbose (bool): If True prints data.

    Returns:
        indexes (int,int): indexes of the position
        xy_ext (float, float): position of maximum
        extrema (float): value of maximum
    """

    if kind == 'max':
        result = np.where(array2D == np.amax(array2D))
    elif kind == 'min':
        result = np.where(array2D == np.min(array2D))

    listOfCordinates = list(zip(result[1], result[0]))

    num_extrema = len(listOfCordinates)

    indexes = np.zeros((num_extrema, 2), dtype=int)
    xy_ext = np.zeros((num_extrema, 2))
    extrema = np.zeros((num_extrema))

    for i, cord in enumerate(listOfCordinates):
        indexes[i, :] = cord[0], cord[1]
        xy_ext[i, 0] = x[cord[0]]
        xy_ext[i, 1] = y[cord[1]]
        extrema[i] = array2D[cord[1], cord[0]]

    if verbose is True:
        for cord in listOfCordinates:
            print(cord, x[cord[0]], y[cord[1]], array2D[cord[1], cord[0]])

    return indexes, xy_ext, extrema


def ndgrid(*args, **kwargs):
    """n-dimensional gridding like Matlab's NDGRID.

    Parameters:
        The input *args are an arbitrary number of numerical sequences, e.g. lists, arrays, or tuples.
        The i-th dimension of the i-th output argument
        has copies of the i-th input argument.

    Example:

        >>> x, y, z = [0, 1], [2, 3, 4], [5, 6, 7, 8]

        >>> X, Y, Z = ndgrid(x, y, z)
            # unpacking the returned ndarray into X, Y, Z

        Each of X, Y, Z has shape [len(v) for v in x, y, z].

        >>> X.shape == Y.shape == Z.shape == (2, 3, 4)
            True

        >>> X
            array([[[0, 0, 0, 0],
                            [0, 0, 0, 0],
                            [0, 0, 0, 0]],
                    [[1, 1, 1, 1],
                            [1, 1, 1, 1],
                            [1, 1, 1, 1]]])
        >>> Y
            array([[[2, 2, 2, 2],
                            [3, 3, 3, 3],
                            [4, 4, 4, 4]],
                    [[2, 2, 2, 2],
                            [3, 3, 3, 3],
                            [4, 4, 4, 4]]])
        >>> Z
            array([[[5, 6, 7, 8],
                            [5, 6, 7, 8],
                            [5, 6, 7, 8]],
                    [[5, 6, 7, 8],
                            [5, 6, 7, 8],
                            [5, 6, 7, 8]]])

        With an unpacked argument list:

        >>> V = [[0, 1], [2, 3, 4]]

        >>> ndgrid(*V) # an array of two arrays with shape (2, 3)
            array([[[0, 0, 0],
                [1, 1, 1]],
                [[2, 3, 4],
                [2, 3, 4]]])

        For input vectors of different data kinds,
        same_dtype=False makes ndgrid()
        return a list of arrays with the respective dtype.
        >>> ndgrid([0, 1], [1.0, 1.1, 1.2], same_dtype=False)
        [array([[0, 0, 0], [1, 1, 1]]),
        array([[ 1. ,  1.1,  1.2], [ 1. ,  1.1,  1.2]])]

        Default is to return a single array.

        >>> ndgrid([0, 1], [1.0, 1.1, 1.2])
            array([[[ 0. ,  0. ,  0. ], [ 1. ,  1. ,  1. ]],
                [[ 1. ,  1.1,  1.2], [ 1. ,  1.1,  1.2]]])
    """
    same_dtype = kwargs.get("same_dtype", True)
    V = [array(v) for v in args]  # ensure all input vectors are arrays
    shape = [len(v) for v in args]  # common shape of the outputs
    result = []
    for i, v in enumerate(V):
        # reshape v so it can broadcast to the common shape
        # http://docs.scipy.org/doc/numpy/user/basics.broadcasting.html
        zero = zeros(shape, dtype=v.dtype)
        thisshape = ones_like(shape)
        thisshape[i] = shape[i]
        result.append(zero + v.reshape(thisshape))
    if same_dtype:
        return array(result)  # converts to a common dtype
    else:
        return result  # keeps separate dtype for each output


def fft_convolution2d(x, y):
    """ 2D convolution, using FFT

    Parameters:
        x (numpy.array): array 1 to convolve
        y (numpy.array): array 2 to convolve

    Returns:
        convolved function
    """
    return fftconvolve(x, y, mode='same')


def fft_convolution1d(x, y):
    """ 1D convolution, using FFT

    Parameters:
        x (numpy.array): array 1 to convolve
        y (numpy.array): array 2 to convolve

    Returns:
        convolved function
    """

    return fftconvolve(x, y, mode='same')


def fft_filter(x, y, normalize=False):
    """ 1D convolution, using FFT

    Parameters:
        x (numpy.array): array 1 to convolve
        y (numpy.array): array 2 to convolve

    Returns:
        convolved function
    """

    y = y / y.sum()

    return fftconvolve(x, y, mode='same') / fftconvolve(
        x, np.ones_like(y) / sum(y), mode='same')


def fft_correlation1d(x, y):
    """ 1D correlation, using FFT (fftconvolve)

    Parameters:
        x (numpy.array): array 1 to convolve
        y (numpy.array): array 2 to convolve

    Returns:
        numpy.array: correlation function
    """
    return fftconvolve(x, y[::-1], mode='same')


def fft_correlation2d(x, y):
    """Parameters:
        x (numpy.array): array 1 to convolve
        y (numpy.array): array 2 to convolve

    Returns:
        numpy.array: 2d correlation function
    """

    return fftconvolve(x, y[::-1, ::-1], mode='same')
