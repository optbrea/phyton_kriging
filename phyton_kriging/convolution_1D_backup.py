# !/usr/local/bin/python3
# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------
# Name:		krigConv.py
# Purpose:	 Kriging por convolution 1D
#
# Author:	  Luis Miguel Sanchez Brea
#
# Created:	 10/12/2011
# RCS-ID:
# Copyright:
# Licence:	 GPL
# ----------------------------------------------------------------------
"""Kriging por convolution

TODO: 
1. meter errorReal como diferencia entre estimation y signal con data según sampling
2. hacer function para hacer simplificado (utilizable en la optimización)
3. separar la estimation y solo hacerla cuando data no sea nulo
4. hacer lo mismo para el kriging estándard
"""

import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
from scipy.signal import convolve

from .variogram_1D import theoretical
from .utils_math import sampling2

um = 1
mm = 1000 * um
degrees = sp.pi / 180


class Convolution_1D(object):
    """
    Clase en la que se hace la convolución 1D del kriging
    """

    def __init__(self, sensors, variogram, sampling, y_ideal=None):
        """

        Args:
            sensors (_type_): _description_
            variogram (_type_): _description_
            sampling (_type_): _description_
        """
        self.sensors = sensors
        self.variogram = variogram
        self.sampling = sampling
        self.x = sampling
        self.y_ideal = y_ideal

        self.kriging = None
        self.estimation = None

        self.Lambda = None
        self.MD = None
        self.NEQ = None

        # DM(x,y)
        x = self.sampling - self.sampling.mean()
        var = variogram.get(x)
        self.sc = var.min()
        self.DM = self.sc**2 / (2 * var - self.sc**2)

        self.DM_normalize()
        self.compute_error()
        self.compute_estimation(sensors)

    def compute_error(self):
        """Compute error

        Args:
            sensors (_type_): _description_
        """
        xComb, ZComb = sampling2(xy=self.sensors, sampling=self.sampling)

        # NEQ(x,y)
        self.NEQ = convolve(xComb, self.DM, 'same')
        # self.NEQ = convolve(
        #     xComb, self.DM,
        #     'same') + (self.sc)**2 / self.variogram.get(self.x).max()

        # error(x,y)
        # de momento no se como colocar cuando hay distintas precisiones
        self.error = sp.sqrt(self.sensors[0, 2]**2 + self.sc**2 / self.NEQ)

    def compute_estimation(self, sensors=None):
        """_summary_

        Args:
            sensors (_type_): _description_
        """

        numsensors = len(self.sensors)
        period_aproximado = (self.sampling.max() -
                             self.sampling.min()) / numsensors

        # temporal
        self.Lambda = sp.sinc(.25 * (self.sampling - self.sampling.mean()) /
                              period_aproximado) * self.DM**.5

        # estimation(x,y)
        if sensors is None:
            sensors = self.sensors

        xComb, zComb = sampling2(sensors, sampling=self.sampling)

        estimation = convolve(zComb, self.Lambda, 'same')
        lambda_normalized = convolve(xComb, self.Lambda, 'same')
        self.estimation = estimation / lambda_normalized

    def DM_normalize(self, remove_background=True):
        """Normalize DM function so that DM.min=0 and DM.max=1

        Args:
            remove_background (bool, optional): Makes DM.min=0. Defaults to True.
        """
        DM = self.DM
        if remove_background is True:
            self.DM = (DM - DM.min()) / (1 - DM.min())
        self.DM = self.DM / self.DM.max()

    def get(self, kind):
        """_summary_

        Args:
            kind (_type_): _description_

        Returns:
            _type_: _description_
        """
        xm = self.sampling

        if kind == 'data':
            output = (self.sensors[:, 0], self.sensors[:, 1], self.sensors[:,
                                                                           2])

        elif kind == 'DM':
            output = (xm - xm.mean(), self.DM)

        elif kind == 'NEQ':
            output = (xm, self.NEQ)

        elif kind == 'error':
            output = (xm, self.error)

        elif kind == 'lambda':
            output = (xm - xm.mean(), self.Lambda)

        elif kind == 'estimation':
            output = (xm, self.estimation)

        else:
            print("not proper kind parameter")
            output = None

        return output

    def draw(self, kind='DM'):
        """_summary_

        Args:
            kind (str, optional): _description_. Defaults to 'DM'.
        """

        if kind == 'compare':
            plt.figure()
            plt.plot(self.sensors[:, 0],
                     self.sensors[:, 1],
                     'r.',
                     label="signal")
            plt.plot(self.sampling, self.estimation, 'r', label=r"convolution")
            if self.y_ideal is not None:
                plt.plot(self.sampling, self.y_ideal, 'k--', label=r"ideal")
            plt.plot(self.sampling,
                     self.estimation + self.error,
                     'b-.',
                     label=r"error")
            plt.plot(self.sampling, self.estimation - self.error, 'b-.')
            plt.fill_between(self.sampling,
                             self.estimation - self.error,
                             self.estimation + self.error,
                             color='b',
                             alpha=0.25)
            plt.xlabel("$x$")
            plt.ylabel("$I(x)$")
            plt.legend()
            plt.xlim(self.sampling[0], self.sampling[-1])
            plt.ylim(1.25 * self.sensors[:, 1].min(),
                     1.25 * self.sensors[:, 1].max())
            error = abs(self.y_ideal - self.estimation)

            plt.figure()
            plt.plot(self.sampling, self.error, 'k', label=r"convolution")
            if self.y_ideal is not None:
                plt.plot(self.sampling, error, 'b-.', label=r"real")
            plt.plot(self.sensors[:, 0],
                     self.sensors[:, 2],
                     'r.',
                     label="I(x)")
            plt.plot(self.sensors[:, 0],
                     np.sqrt(self.sensors[:, 2]**2 + self.sc**2),
                     'r-.',
                     label="$\sqrt{\sigma^2+I^2}$")
            plt.xlim(self.sampling[0], self.sampling[-1])
            plt.ylim(0, error.max())
            plt.xlabel("$x$")
            plt.ylabel("$\sigma$(x)")
            plt.legend()

        if kind == 'data':
            x, y, error = self.get('data')
            print(x.shape, y.shape, error.shape)
            plt.errorbar(x,
                         y,
                         error,
                         lw=0,
                         color='k',
                         marker='o',
                         label='data')

        if kind == 'var':
            self.variogram.draw()

        if kind == 'error':
            x, y = self.get('error')
            self.__draw__(x, y, label='error')
            plt.plot(self.sensors[:, 0],
                     self.sensors[:, 2],
                     'b.',
                     label='sensors')
            plt.plot(self.sensors[:, 0],
                     np.sqrt(self.sensors[:, 2]**2 + self.sc**2),
                     'r-.',
                     label="$\sqrt{\sigma^2+I^2}$")
            plt.ylim(ymin=0)
            plt.legend()

        if kind == 'DM':
            x, y = self.get('DM')
            self.__draw__(x, y, label='DM(x)')
            plt.ylim(0)

        if kind == 'NEQ':
            x, y = self.get('NEQ')
            self.__draw__(x, y, label='NEQ(x)')
            plt.plot(self.sensors[:, 0],
                     sp.zeros_like(self.sensors[:, 0]),
                     'ko',
                     lw=4)
            plt.ylim(ymin=0)

        if kind == 'lambda':
            x, y = self.get('lambda')
            self.__draw__(x, y, label='$\lambda$(x)')

        if kind == 'estimation':
            x = self.sampling
            y = self.estimation
            plt.figure()
            plt.plot(x, y, 'b', label='estimation')

            plt.plot(x, y + self.error, 'b-.', label='error band')
            plt.plot(x, y - self.error, 'b-.')
            plt.plot(self.sensors[:, 0],
                     self.sensors[:, 1],
                     'k.',
                     label='data')
            plt.fill_between(x, y - self.error, y + self.error, alpha=0.25)
            plt.xlim(x.min(), x.max())
            plt.ylim((y - self.error).min(), (y + self.error).max())

            plt.legend()

    def __draw__(self, x, y, label=''):
        """_summary_

        Args:
            x (_type_): _description_
            y (_type_): _description_
        """
        plt.figure()
        plt.plot(x, y, 'k', label=label)
        plt.xlim(x.min(), x.max())
        plt.ylabel(label)
        plt.xlabel('x')
