# !/usr/local/bin/python3
# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------
# Name:		krigConv.py
# Purpose:	 Kriging por convolution 1D
#
# Author:	  Luis Miguel Sanchez Brea
#
# Created:	 10/12/2011
# ----------------------------------------------------------------------
"""Kriging por convolution

TODO: 
1. meter errorReal como diferencia entre estimation y signal con data según sampling
2. hacer function para hacer simplificado (utilizable en la optimización)
3. separar la estimation y solo hacerla cuando data no sea nulo
4. hacer lo mismo para el kriging estándard
"""

import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
from scipy.signal import convolve

from .variogram_1D import theoretical
from .standard_2D import kriging2D
from .utils_math import nearest, samplingXY, sampling2, nearest2, distance


class Convolution_2D(kriging2D):
    """
    Clase en la que se hace la convolución 1D del kriging
    """

    def __init__(self, sensors, variogram, sampling, y_ideal=None):
        """equal than Scalar_field_X"""
        super().__init__(sensors, variogram, sampling, y_ideal)

        self.sc = None
        self.DM = None
        self.Lambda = None
        self.NEQ = None
        self.error_conv = None
        self.estimation_conv = None

        xsampling, ysampling = self.sampling
        self.Sampling = np.meshgrid(xsampling, ysampling)

        x = xsampling - xsampling.mean()
        y = ysampling - ysampling.mean()

        self.standard_radial(filtering=True)
        # self.compute_DM()
        # self.normalize_DM()
        # self.center_lambdas()

    def get(self, kind):
        """Get data from kriging interpolation

        Args:
            kind (string): _description_

        Returns:
            _type_: _description_
        """
        xm, ym = self.sampling
        Xm, Ym = self.Sampling

        if kind == 'kriging':
            output = (xm, ym, self.estimation)

        elif kind == 'error':
            output = (xm, ym, self.error)

        elif kind == 'lambda':
            output = (xm - xm.mean(), ym - ym.mean(), self.lambdas)

        elif kind == 'errorReal':
            output = (xm, ym, abs(self.sampling.y - self.estimation))

        elif kind == 'DM':
            self.compute_DM()
            output = (xm, ym, self.DM)

        else:
            print("not proper kind parameter")
            output = None

        return output

    def compute_DM(self):
        """_summary_

        Returns:
            _type_: _description_
        """

        Xsampling, Ysampling = self.Sampling

        h = np.sqrt(Xsampling**2 + Ysampling**2)

        variogr = self.variogram.get(h)
        self.sc = np.sqrt(variogr.min())
        self.DM = self.sc**2 / (2 * variogr - self.sc**2)

        # #Trampeo
        # self.DM = np.abs(self.Lambda)
        # self.DM = self.DM/ self.DM.max()

        return self.DM

    def normalize_DM(self):
        """_summary_

        Returns:
            _type_: _description_
        """
        self.compute_DM()
        self.DM = self.DM - self.DM.min()
        self.DM = self.DM / self.DM.max()

        return self.DM

    def center_lambdas(self):
        """Esta función toma varias functiones lambdas y las traslada colocando los máximos en el centro
		Los que falta de la función (al ser trasladada hay partes no definidas) las rellena de Nulos        
        """
        pass

    def center_lambdas(self):
        """Esta función toma varias functiones lambdas y las traslada colocando los máximos en el centro
		Los que falta de la función (al ser trasladada hay partes no definidas) las rellena de Nulos        

        """
        m = self.sampling
        s = self.sensors

        samplingx, samplingy = self.sampling
        incr_x = samplingx[1] - samplingx[0]
        incr_y = samplingy[1] - samplingy[0]

        range_x = (samplingx[-1] - samplingx[0])
        range_y = (samplingy[-1] - samplingy[0])

        num_x = len(samplingx)
        num_y = len(samplingy)

        xmin = samplingx[0] - range_x - incr_x / 2
        xmax = samplingx[-1] + range_x - incr_y / 2
        X = np.arange(xmin, xmax, incr_x)

        ymin = samplingy[0] - range_y
        ymax = samplingy[-1] + range_y
        Y = np.arange(ymin, ymax, incr_y)

        lambdas_centered = np.zeros((len(X), len(Y), len(s)), float)
        lambdas_centered.fill(np.nan)

        for i in range(len(s)):

            lambda_i = self.lambdas[:, :, i]
            pos_max_x, pos_max_y = (np.argwhere(
                lambda_i == np.max(lambda_i))[0]).squeeze()

            pos_ini_x = int(round(len(X) / 2) - pos_max_x)
            pos_ini_y = int(round(len(Y) / 2) - pos_max_y)

            lambdas_centered[pos_ini_x:pos_ini_x + num_x,
                             pos_ini_y:pos_ini_y + num_y, i] = lambda_i

        std_lambda = np.nanstd(lambdas_centered, axis=2)
        Lambda = np.nanmean(lambdas_centered, axis=2)

        lambdas_centered[lambdas_centered > Lambda[:, :, np.newaxis] +
                         2 * std_lambda[:, :, np.newaxis]] = np.nan
        lambdas_centered[lambdas_centered < Lambda[:, :, np.newaxis] -
                         2 * std_lambda[:, :, np.newaxis]] = np.nan
        Lambda = np.nanmean(lambdas_centered, axis=2)

        i_min, _, _ = nearest(X, samplingx[0])
        i_max, _, _ = nearest(X, samplingx[-1])

        j_min, _, _ = nearest(Y, samplingy[0])
        j_max, _, _ = nearest(Y, samplingy[-1])

        i_max = i_max + 1
        j_max = j_max + 1

        Lambda = Lambda[i_min:i_max, j_min:j_max]
        X = X[i_min:i_max]
        Y = Y[j_min:j_max]
        lambdas_centered = lambdas_centered[i_min:i_max, j_min:j_max, :]
        std_lambda = std_lambda[i_min:i_max, j_min:j_max]
        self.lambdas_centered = lambdas_centered
        self.Lambda = Lambda
        self.stdLambda = std_lambda
        # plt.figure()
        # plt.imshow(std_lambda)
        # plt.colorbar()
        # plt.clim(0)

        return self

    def compute_NEQ(self):

        xyComb, zComb = samplingXY(xy_data=self.sensors,
                                   sampling=self.sampling)
        self.NEQ = convolve(xyComb, self.DM, 'same')
        # plt.figure()
        # plt.imshow(zComb)

    def compute_estimation_convolution(self, sensors=None):
        """_summary_

        Args:
            sensors (_type_): _description_
        """

        if sensors is None:
            sensors = self.sensors

        xyComb, zComb = samplingXY(xy_data=self.sensors,
                                   Sampling2D=self.sampling)
        estimation = convolve(zComb, self.Lambda, 'same')
        lambda_normalized = convolve(xyComb, self.Lambda, 'same')
        self.estimation_conv = estimation / lambda_normalized
        return self.estimation_conv

    def compute_error_convolution(self):
        """Compute error

        Args:
            sensors (_type_): _description_
        """
        xyComb, zComb = samplingXY(xy_data=self.sensors,
                                   Sampling2D=self.sampling)
        # NEQ(x,y)
        print(xyComb)

        self.NEQ = convolve(xyComb, self.DM, 'same')
        # self.NEQ = convolve(
        #     xComb, self.DM,
        #     'same') + (self.sc)**2 / self.variogram.get(self.x).max()

        # error(x,y)
        # de momento no se como colocar cuando hay distintas precisiones
        self.error_conv = np.sqrt(self.sensors[0, 3]**2 +
                                  self.sc**2 / self.NEQ)

        self.Zcomb = zComb
        self.xyComb = xyComb
        return self.error_conv

    def get(self, kind):
        """Get data from kriging interpolation

        Args:
            kind (string): _description_

        Returns:
            _type_: _description_
        """
        xm, ym = self.sampling
        Xm, Ym = self.Sampling

        if kind == 'data':
            output = (self.sensors[:, 0], self.sensors[:, 1], self.sensors[:,
                                                                           2])
        if kind == 'estimation_kriging':
            output = (xm, ym, self.estimation)

        elif kind == 'error_kriging':
            output = (xm, ym, self.error)

        if kind == 'estimation_convolution':
            self.compute_estimation_convolution()
            output = (xm, ym, self.estimation_conv)

        elif kind == 'error_convolution':
            self.compute_error_convolution()
            output = (xm, ym, self.error_conv)

        elif kind == 'errorReal':
            output = (xm, ym, abs(self.sampling.y - self.estimation))

        elif kind == 'lambdas':
            output = (xm, ym, self.lambdas)

        elif kind == 'lambdas_centered':
            output = (xm, ym, self.lambdas_centered)

        elif kind == 'Lambda':
            output = (xm, ym, self.Lambda)

        elif kind == 'DM':
            self.compute_DM()
            output = (xm, ym, self.DM)

        elif kind == 'NEQ':
            self.compute_NEQ()
            output = (xm, ym, self.NEQ)

        else:
            print("not proper kind parameter")
            output = None

        return output

    def draw(self, kind='kriging', circles=True, cmap='', clim=None):
        """		Draws kriging: data, variogram, lambdas, error, kriging, 

        Args:
            kind (str, optional): _description_. Defaults to 'kriging'.
            xlabel (str, optional): _description_. Defaults to 'x'.
            ylabel (str, optional): _description_. Defaults to 'I'.
            num_lambda (_type_, optional): _description_. Defaults to all.
        """
        from ipywidgets import interact

        sensors = self.sensors

        xsensors = self.sensors[:, 0]
        ysensors = self.sensors[:, 1]
        data = self.sensors[:, 2]
        I = self.sensors[:, 3]
        num_sensors = len(xsensors)

        xsampling, ysampling = self.sampling

        extension = [
            xsampling.min(),
            xsampling.max(),
            ysampling.min(),
            ysampling.max()
        ]

        if cmap != '':
            plt.set_cmap(cmap)

        # self.compute_error_convolution()
        # self.compute_NEQ()
        # self.compute_estimation_convolution()

        if kind == 'data':
            sensors = self.sensors

            plt.figure()
            # plt.scatter(sensors[:, 0],
            #             sensors[:, 1],
            #             100 * sensors[:, 2] / sensors[:, 2].max(),
            #             marker='o')
            plt.imshow(self.Zcomb,
                       interpolation='bilinear',
                       aspect='equal',
                       origin='lower',
                       extent=extension,
                       cmap='seismic')
            plt.colorbar()

        elif kind == 'ideal':
            self.__draw__(self.ideal,
                          xsampling,
                          ysampling,
                          label=r"$Z_{ideal}(x,y)$",
                          cmap='hot')

            if clim is None:
                maximum = np.nanmax(np.ravel(self.ideal))
                minimum = np.nanmin(np.ravel(self.ideal))
                plt.clim(maximum, minimum)
            else:
                plt.clim(clim[0], clim[1])

            if circles is True:
                plt.scatter(xsensors, ysensors, c='w', s=35)

        elif kind == 'variogram':
            self.variogram.draw()
            plt.ylim(ymin=0)

        elif kind == 'lambdas':
            maximum = np.nanmax(np.ravel(self.lambdas))
            minimum = np.nanmin(np.ravel(self.lambdas))
            extreme = max(abs(maximum), abs(minimum))

            fig = plt.figure()
            ax1 = plt.subplot(111)
            plt.subplots_adjust(left=0.15, bottom=0.25)
            plt.title('$\lambda(x)$')

            line = plt.imshow(np.squeeze(self.lambdas[:, :, 0].transpose()),
                              interpolation='bilinear',
                              aspect='equal',
                              origin='lower',
                              extent=extension,
                              cmap='seismic')
            plt.xlim(xmin=xsampling.min(), xmax=xsampling.max())
            plt.ylim(ymin=ysampling.min(), ymax=ysampling.max())
            plt.colorbar(orientation='vertical')
            plt.clim(vmin=-extreme, vmax=extreme)
            plt.scatter(sensors[:, 0],
                        sensors[:, 1],
                        10,
                        color='red',
                        marker='o')

            def update(i=(0, num_sensors - 1, 1)):
                i = int(i)
                line.set_data(self.lambdas[:, :, i].transpose())
                fig.canvas.draw_idle()

            interact(update)

        elif kind == 'kriging':
            self.__draw__(self.estimation,
                          xsampling,
                          ysampling,
                          label=r"$Z_{kriging}(x,y)$",
                          cmap='hot')

            if clim is None:
                pass
            else:
                plt.clim(clim[0], clim[1])

            if circles is True:
                plt.scatter(xsensors, ysensors, c='w', s=35)

        elif kind == 'error_kriging':
            self.__draw__(self.error,
                          xsampling,
                          ysampling,
                          label=r"$error_{kriging}(x,y)$",
                          cmap='hot')

            if clim is None:
                maximum = np.nanmax(np.ravel(self.error))
                minimum = np.nanmin(np.ravel(self.error))
                plt.clim(minimum, maximum)
            else:
                plt.clim(clim[0], clim[1])

            if circles is True:
                plt.scatter(xsensors, ysensors, c='w', s=35)

        elif kind == 'convolution' or kind == 'estimation_convolution':
            self.get('estimation_convolution')
            self.__draw__(self.estimation_conv,
                          xsampling,
                          ysampling,
                          label=r"$Z_{conv}(x,y)$",
                          cmap='hot')

            if clim is None:
                pass
            else:
                plt.clim(clim[0], clim[1])

            if circles is True:
                plt.scatter(xsensors, ysensors, c='w', s=35)

        elif kind == 'error_convolution':
            if self.error_conv is None:
                self.compute_error_convolution()
            self.__draw__(self.error_conv,
                          xsampling,
                          ysampling,
                          label=r"$error_{conv}(x,y)$",
                          cmap='hot')

            if clim is None:
                pass
            else:
                plt.clim(clim[0], clim[1])

            if circles is True:
                plt.scatter(xsensors, ysensors, c='w', s=35)

        elif kind == 'lambdas_centered':
            self.center_lambdas()
            maximum = np.nanmax(np.ravel(self.lambdas_centered))
            minimum = np.nanmin(np.ravel(self.lambdas_centered))
            extreme = max(abs(maximum), abs(minimum))
            print(extreme)
            fig = plt.figure()
            ax1 = plt.subplot(111)
            plt.subplots_adjust(left=0.15, bottom=0.25)
            plt.title('$\lambda(x)$')

            line = plt.imshow(np.squeeze(self.lambdas[:, :, 0].transpose()),
                              interpolation='bilinear',
                              aspect='equal',
                              origin='lower',
                              extent=extension,
                              cmap='seismic')
            plt.xlim(xmin=xsampling.min(), xmax=xsampling.max())
            plt.ylim(ymin=ysampling.min(), ymax=ysampling.max())
            plt.colorbar(orientation='vertical')
            plt.clim(vmin=-extreme, vmax=extreme)
            plt.scatter(sensors[:, 0],
                        sensors[:, 1],
                        10,
                        color='red',
                        marker='o')

            def update(i=(0, num_sensors - 1, 1)):
                i = int(i)
                line.set_data(self.lambdas_centered[:, :, i].transpose())
                fig.canvas.draw_idle()

            interact(update)

        elif kind == 'Lambda':
            x, y, Lambda = self.get('Lambda')
            self.__draw__(Lambda, x, y, label=r'$\Lambda(x,y)$')
            maximum = np.nanmax(np.ravel(self.Lambda))
            minimum = np.nanmin(np.ravel(self.Lambda))
            extreme = max(abs(maximum), abs(minimum))
            plt.clim(vmin=-extreme, vmax=extreme)

        elif kind == 'DM':
            self.compute_DM()
            x, y, DM = self.get('DM')
            self.__draw__(DM, x, y, label='DM(x,y)', cmap='hot')
            plt.clim(0)

        elif kind == 'NEQ':
            self.compute_NEQ()
            x, y, NEQ = self.get('NEQ')
            self.__draw__(NEQ, x, y, label='NEQ(x,y)', cmap='hot')
            if circles is True:
                plt.scatter(xsensors, ysensors, c='w', s=35)

    def __draw__(self, z, x, y, label='', cmap='seismic'):
        """_summary_

        Args:
            x (_type_): _description_
            y (_type_): _description_
        """
        plt.figure(figsize=(8, 8))
        extension = [x[0], x[-1], y[0], y[-1]]
        plt.imshow(z.transpose(),
                   interpolation='bilinear',
                   origin='lower',
                   extent=extension,
                   cmap=cmap)

        plt.xlabel('x', fontsize=18)
        plt.ylabel('y', fontsize=18)
        plt.title(label, fontsize=22)
        plt.colorbar()
        plt.tight_layout()