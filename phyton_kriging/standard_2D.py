#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import scipy as sp
import matplotlib.pyplot as plt

from .variogram_1D import theoretical
from .variogram_2D import theoretical
import ipywidgets as widgets

um = 1
mm = 1000 * um
degrees = np.pi / 180


class kriging2D(object):
    """Class for unidimensional 2D Kriging.

    Parameters:
        sensors (numpy.array): (x, y, fxy) array with positions and values of sensors
        variogram (numpy.array): Theoretical variogram.
        sampling (dict): 2D (x,y) array sampling positions
    Attributes:
        self.sensors 
        self.variogram
        self.sampling
        self.kriging
    """

    def __init__(self, sensors, variogram, sampling, z_ideal=None):

        self.sensors = sensors
        self.variogram = variogram
        self.sampling = sampling
        self.z_ideal = z_ideal

        self.kriging = None

        self.lambdas = None

        x_sampling, y_sampling = self.sampling
        X_sampling, Y_sampling = np.meshgrid(x_sampling, y_sampling)
        self.Sampling = X_sampling, Y_sampling

    def get(self, kind):
        """Get data from kriging interpolation

        Args:
            kind (string): _description_

        Returns:
            _type_: _description_
        """
        xm, ym = self.sampling
        Xm, Ym = self.Sampling

        if kind == 'kriging':
            output = (xm, ym, self.estimation)

        elif kind == 'error':
            output = (xm, ym, self.error)

        elif kind == 'lambda':
            output = (xm - xm.mean(), ym - ym.mean(), self.lambdas)

        elif kind == 'errorReal':
            output = (xm, ym, abs(self.sampling.y - self.estimation))

        else:
            print("not proper kind parameter")
            output = None

        return output

    def standard_radial(self, filtering=True):
        """Standard kriging

        Args:
            * filtering: Si true se aplica la operacion LM para poder filtrar (tesis y articulo de AO)
                        Si False segun libros standard de kriging

        Outputs:
            * estimation
            * error
            * lambdas
            
        Todo:
            * meter valores no regularmente distribuidos
        """

        xsensors = self.sensors[:, 0]
        ysensors = self.sensors[:, 1]
        data = self.sensors[:, 2]
        I = self.sensors[:, 3]

        Xsampling, Ysampling = self.Sampling
        Xsampling_size = len(Xsampling)
        Ysampling_size = len(Ysampling)

        xsampling = Xsampling.flatten()
        ysampling = Ysampling.flatten()

        XS1, XS2 = np.meshgrid(xsensors, xsensors)
        XS, XM = np.meshgrid(xsampling, xsensors)

        YS1, YS2 = np.meshgrid(ysensors, ysensors)
        YS, YM = np.meshgrid(ysampling, ysensors)

        # distance entre sensors
        # distSensores = np.sqrt((XS2 - XS1)**2 + (YS2 - YS1)**2)
        distSensores_X = np.abs(XS2 - XS1)
        distSensores_Y = np.abs(YS2 - YS1)
        # distances entre sensors y points de sampling
        # distSensoresMuestreo = np.sqrt((XS - XM)**2 + (YS - YM)**2)
        distSensoresMuestreo_X = np.abs(XS - XM)
        distSensoresMuestreo_Y = np.abs(YS - YM)

        # distance entre sensors
        distSensores = np.sqrt((XS2 - XS1)**2 + (YS2 - YS1)**2)
        # distances entre sensors y points de sampling
        distSensoresMuestreo = np.sqrt((XS - XM)**2 + (YS - YM)**2)

        # matrices principales
        Gamma = np.mat(self.variogram.get(distSensores))
        gamma = np.mat(self.variogram.get(distSensoresMuestreo))

        # Modificaciones LM según artículo AO
        if filtering == True:
            s0 = self.variogram.get(0.)
            I1, I2 = np.meshgrid(I, I)

            gamma = gamma - s0 / 2
            Gamma = Gamma - I1 * I2
            for i in range(len(xsensors)):
                Gamma[i, i] = -I[i]**2
        else:
            for i in range(len(xsensors)):
                Gamma[i, i] = 0

        # definición de matrices
        U = np.mat(np.ones((len(xsensors), 1), dtype='float'))
        u = np.mat(np.ones((len(xsampling), 1), dtype='float'))
        Ut = np.transpose(U)
        ut = np.transpose(u)
        invGamma = Gamma.I

        g = (ut - Ut * invGamma * gamma)
        h = (Ut * invGamma * U).I
        lambdas = np.mat(np.transpose(gamma + U * h * g) * invGamma)

        error2 = np.squeeze(
            np.diag(
                np.transpose(gamma) * invGamma * gamma -
                np.transpose(g) * h * g))
        # alguna vez me ha salido un valor negativo. cuidado.
        self.error = np.array(np.sqrt(error2))
        self.estimation = np.squeeze(
            np.array(lambdas * np.transpose(np.mat(data))))

        self.error = np.transpose(
            self.error.reshape(Xsampling_size, Ysampling_size))
        self.estimation = np.transpose(
            self.estimation.reshape(Xsampling_size, Ysampling_size))

        lambdas = np.array(lambdas)

        self.lambdas = lambdas.reshape(
            (Xsampling_size, Ysampling_size, len(xsensors)))

        return self

    def standard_backup(self, filtering=True):
        """Standard kriging

        Args:
            * filtering: Si true se aplica la operacion LM para poder filtrar (tesis y articulo de AO)
                        Si False segun libros standard de kriging

        Outputs:
            * estimation
            * error
            * lambdas
        """

        xsensors = self.sensors[:, 0]
        ysensors = self.sensors[:, 1]
        data = self.sensors[:, 2]
        I = self.sensors[:, 3]

        Xsampling, Ysampling = self.Sampling
        Xsampling_size = len(Xsampling)
        Ysampling_size = len(Ysampling)
        print(Xsampling_size, Ysampling_size)

        xsampling = Xsampling.flatten()
        ysampling = Ysampling.flatten()

        XS1, XS2 = np.meshgrid(xsensors, xsensors)
        XS, XM = np.meshgrid(xsampling, xsensors)

        YS1, YS2 = np.meshgrid(ysensors, ysensors)
        YS, YM = np.meshgrid(ysampling, ysensors)

        # distance entre sensors
        distSensores = np.sqrt((XS2 - XS1)**2 + (YS2 - YS1)**2)
        # distances entre sensors y points de sampling
        distSensoresMuestreo = np.sqrt((XS - XM)**2 + (YS - YM)**2)

        # matrices principales
        Gamma = np.mat(self.variogram.get(distSensores))
        gamma = np.mat(self.variogram.get(distSensoresMuestreo))

        # Modificaciones LM según artículo AO
        if filtering == True:
            s0 = self.variogram.get(0.)
            s0 = self.variogram.get((0., 0.))
            I1, I2 = np.meshgrid(I, I)

            gamma = gamma - s0 / 2
            Gamma = Gamma - I1 * I2
            for i in range(len(xsensors)):
                Gamma[i, i] = -I[i]**2
        else:
            for i in range(len(xsensors)):
                Gamma[i, i] = 0

        # definición de matrices
        U = np.mat(np.ones((len(xsensors), 1), dtype='float'))
        u = np.mat(np.ones((len(xsampling), 1), dtype='float'))
        Ut = np.transpose(U)
        ut = np.transpose(u)
        invGamma = Gamma.I

        g = (ut - Ut * invGamma * gamma)
        h = (Ut * invGamma * U).I
        lambdas = np.mat(np.transpose(gamma + U * h * g) * invGamma)

        error2 = np.squeeze(
            np.diag(
                np.transpose(gamma) * invGamma * gamma -
                np.transpose(g) * h * g))
        self.error = np.array(np.sqrt(error2))
        self.estimation = np.squeeze(
            np.array(lambdas * np.transpose(np.mat(data))))

        self.error = np.transpose(
            self.error.reshape(Xsampling_size, Ysampling_size))
        self.estimation = np.transpose(
            self.estimation.reshape(Xsampling_size, Ysampling_size))

        lambdas = np.array(lambdas)

        self.lambdas = lambdas.reshape(
            (Xsampling_size, Ysampling_size, len(xsensors)))

    def error(self, filtering=True):
        """gets only error (not interpolation)

        Args:
            * filtering: Si true se aplica la operacion LM para poder filtrar (tesis y articulo de AO)
                        Si False segun libros standard de kriging

        Outputs:
            * error
        """

        xsensors = self.sensors[:, 0]
        ysensors = self.sensors[:, 1]
        data = self.sensors[:, 2]
        I = self.sensors[:, 3]

        Xsampling, Ysampling = self.Sampling
        Xsampling_size = len(Xsampling)
        Ysampling_size = len(Ysampling)
        print(Xsampling_size, Ysampling_size)

        xsampling = Xsampling.flatten()
        ysampling = Ysampling.flatten()

        XS1, XS2 = np.meshgrid(xsensors, xsensors)
        XS, XM = np.meshgrid(xsampling, xsensors)

        YS1, YS2 = np.meshgrid(ysensors, ysensors)
        YS, YM = np.meshgrid(ysampling, ysensors)

        # distance entre sensors
        distSensores = np.sqrt((XS2 - XS1)**2 + (YS2 - YS1)**2)
        # distances entre sensors y points de sampling
        distSensoresMuestreo = np.sqrt((XS - XM)**2 + (YS - YM)**2)

        # matrices principales
        Gamma = np.mat(self.variogram.get(distSensores))
        gamma = np.mat(self.variogram.get(distSensoresMuestreo))

        # Modificaciones LM según artículo AO
        if filtering == True:
            s0 = self.variogram.get(0.)
            I1, I2 = np.meshgrid(I, I)

            gamma = gamma - s0 / 2
            Gamma = Gamma - I1 * I2
            for i in range(len(xsensors)):
                Gamma[i, i] = -I[i]**2
        else:
            for i in range(len(xsensors)):
                Gamma[i, i] = 0

        # definición de matrices
        U = np.mat(np.ones((len(xsensors), 1), dtype='float'))
        u = np.mat(np.ones((len(xsampling), 1), dtype='float'))
        Ut = np.transpose(U)
        ut = np.transpose(u)
        invGamma = Gamma.I

        g = (ut - Ut * invGamma * gamma)
        h = (Ut * invGamma * U).I
        lambdas = np.mat(np.transpose(gamma + U * h * g) * invGamma)

        error2 = np.squeeze(
            np.diag(
                np.transpose(gamma) * invGamma * gamma -
                np.transpose(g) * h * g))
        self.error = np.array(np.sqrt(error2))
        self.estimation = np.squeeze(
            np.array(lambdas * np.transpose(np.mat(data))))

        self.error = np.transpose(
            self.error.reshape(Xsampling_size, Ysampling_size))
        self.estimation = np.transpose(
            self.estimation.reshape(Xsampling_size, Ysampling_size))

        lambdas = np.array(lambdas)

        self.lambdas = lambdas.reshape(
            (Xsampling_size, Ysampling_size, len(xsensors)))

    def standard_non_uniform(self, filtering=True):
        """Standard kriging. Los datos no tienen por que estar equipuestreados.

		Args:
            * filtering (bool): Si true se aplica la operacion LM para poder filtrar (tesis y articulo de AO)
                        Si False segun libros standard de kriging

		Outputs:
            * lambdas
            * interpolation
            * error
		"""

        # 1. Detecta dimensión de los data
        # 2. Calcula los values del kriging estándar
        # 3. Guarda los data en data1D o data2D

        xsensors = self.sensors[:, 0]
        ysensors = self.sensors[:, 1]
        data = self.sensors[:, 2]
        I = self.sensors[:, 3]

        Xsampling, Ysampling = self.Sampling
        xsampling = Xsampling.flatten()
        ysampling = Ysampling.flatten()

        XS1, XS2 = np.meshgrid(xsensors, xsensors)
        XS, XM = np.meshgrid(xsampling, xsensors)

        YS1, YS2 = np.meshgrid(ysensors, ysensors)
        YS, YM = np.meshgrid(ysampling, ysensors)

        distSensores_X = np.abs(XS2 - XS1)
        distSensores_Y = np.abs(YS2 - YS1)

        distSensoresMuestreo_X = np.abs(XS - XM)
        distSensoresMuestreo_Y = np.abs(YS - YM)

        # distance entre sensors
        distSensores = np.sqrt((XS2 - XS1)**2 + (YS2 - YS1)**2)
        # distances entre sensors y points de sampling
        distSensoresMuestreo = np.sqrt((XS - XM)**2 + (YS - YM)**2)

        # matrices principales
        Gamma = np.mat(self.variogram.get((distSensores_X, distSensores_Y)))
        gamma = np.mat(
            self.variogram.get(
                (distSensoresMuestreo_X, distSensoresMuestreo_Y)))

        # Modificaciones LM según artículo AO
        if filtering is True:
            s0 = self.variogram.get((0., 0.))

            I1, I2 = np.meshgrid(I, I)

            gamma = gamma - s0 / 2
            Gamma = Gamma - I1 * I2
            for i in range(len(xsensors)):
                Gamma[i, i] = -I[i]**2
        else:
            for i in range(len(xsensors)):
                Gamma[i, i] = 0

        # definición de matrices
        U = np.mat(np.ones((len(xsensors), 1), dtype='float'))
        u = np.mat(np.ones((len(xsampling), 1), dtype='float'))
        Ut = np.transpose(U)
        ut = np.transpose(u)
        invGamma = Gamma.I

        g = (ut - Ut * invGamma * gamma)
        h = (Ut * invGamma * U).I
        lambdas = np.mat(np.transpose(gamma + U * h * g) * invGamma)

        error2 = np.squeeze(
            np.diag(
                np.transpose(gamma) * invGamma * gamma -
                np.transpose(g) * h * g))
        self.error = np.array(np.sqrt(error2))
        self.interpolation = np.squeeze(
            np.array(lambdas * np.transpose(np.mat(data))))

        self.error = np.transpose(self.error)
        self.interpolation = np.transpose(self.interpolation)

        lambdas = np.array(lambdas)
        self.lambdas = lambdas

    def draw(self, kind='kriging', circles=True, cmap='', clim=None):
        """_summary_

        Args:
            kind (str, optional): _description_. Defaults to 'kriging'.
            circles (bool, optional): _description_. Defaults to True.
            cmap (str, optional): _description_. Defaults to ''.
            clim (_type_, optional): _description_. Defaults to None.
        """
        from ipywidgets import interact

        sensors = self.sensors

        xsensors = self.sensors[:, 0]
        ysensors = self.sensors[:, 1]
        data = self.sensors[:, 2]
        I = self.sensors[:, 3]
        num_sensors = len(xsensors)

        xsampling, ysampling = self.sampling

        extension = [
            xsampling.min(),
            xsampling.max(),
            ysampling.min(),
            ysampling.max()
        ]

        if cmap != '':
            plt.set_cmap(cmap)

        if kind == 'data':
            sensors = self.sensors

            plt.figure()
            plt.scatter(sensors[:, 0],
                        sensors[:, 1],
                        100 * sensors[:, 2] / sensors[:, 2].max(),
                        marker='o')
            plt.colorbar()

        elif kind == 'ideal':
            self.__draw__(self.ideal.transpose(),
                          xsampling,
                          ysampling,
                          label=r"$Z_{ideal}(x,y)$",
                          cmap='hot')

            if clim is None:
                maximum = np.nanmax(np.ravel(self.ideal))
                minimum = np.nanmin(np.ravel(self.ideal))
                plt.clim(maximum, minimum)
            else:
                plt.clim(clim[0], clim[1])

            if circles is True:
                plt.scatter(xsensors, ysensors, c='w', s=35)

        elif kind == 'variogram':
            self.variogram.draw()
            plt.ylim(ymin=0)

        elif kind == 'lambdas':
            maximum = np.nanmax(np.ravel(self.lambdas))
            minimum = np.nanmin(np.ravel(self.lambdas))
            extreme = max(abs(maximum), abs(minimum))

            fig = plt.figure()
            ax1 = plt.subplot(111)
            plt.subplots_adjust(left=0.15, bottom=0.25)
            plt.title('$\lambda(x)$')

            line = plt.imshow(np.squeeze(self.lambdas[:, :, 0]),
                              interpolation='bilinear',
                              aspect='equal',
                              origin='lower',
                              extent=extension,
                              cmap='seismic')
            plt.xlim(xmin=xsampling.min(), xmax=xsampling.max())
            plt.ylim(ymin=ysampling.min(), ymax=ysampling.max())
            plt.colorbar(orientation='vertical')
            plt.clim(vmin=-extreme, vmax=extreme)
            plt.scatter(sensors[:, 0],
                        sensors[:, 1],
                        10,
                        color='red',
                        marker='o')

            def update(i=(0, num_sensors - 1, 1)):
                i = int(i)
                line.set_data(self.lambdas[:, :, i])
                fig.canvas.draw_idle()

            interact(update)

        elif kind == 'kriging':
            self.__draw__(self.estimation,
                          xsampling,
                          ysampling,
                          label=r"$Z_{kriging}(x,y)$",
                          cmap='hot')

            if clim is None:
                pass
            else:
                plt.clim(clim[0], clim[1])

            if circles is True:
                plt.scatter(xsensors, ysensors, c='w', s=35)

        elif kind == 'error_kriging' or kind == 'error':
            self.__draw__(self.error.transpose(),
                          xsampling,
                          ysampling,
                          label=r"$error_{kriging}(x,y)$",
                          cmap='hot')

            if clim == None:
                maximum = np.nanmax(np.ravel(self.error))
                minimum = np.nanmin(np.ravel(self.error))
                plt.clim(minimum, maximum)
            else:
                plt.clim(clim[0], clim[1])

            if circles is True:
                plt.scatter(xsensors, ysensors, c='w', s=35)

    def __draw__(self, z, x, y, label='', cmap='seismic'):
        """_summary_

        Args:
            z (_type_): _description_
            x (_type_): _description_
            y (_type_): _description_
            label (str, optional): _description_. Defaults to ''.
            cmap (str, optional): _description_. Defaults to 'seismic'.
        """
        plt.figure()
        extension = [x[0], x[-1], y[0], y[-1]]
        plt.imshow(z.transpose(),
                   interpolation='bilinear',
                   origin='lower',
                   extent=extension,
                   cmap=cmap)

        plt.xlabel('x', fontsize=18)
        plt.ylabel('y', fontsize=18)
        plt.title(label, fontsize=22)
        plt.colorbar()
        plt.tight_layout()