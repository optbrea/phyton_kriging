#import scipy as sp
import matplotlib.pyplot as plt
import numpy as np
import scipy as sp

__author__ = """Luis Miguel Sanchez Brea"""
__email__ = 'optbrea@ucm.es'
__version__ = '0.1.1'

name = 'phyton_kriging'

um = 1.
mm = 1000. * um
nm = um / 1000.
degrees = np.pi / 180.
s = 1.
seconds = 1.

eps = 1e-6
num_decimals = 4