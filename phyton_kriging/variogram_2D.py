# !/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This module generates Variogram class for theoretical and experimental variograms, including fitting

"""

from . import degrees, mm, np, plt
from .config import CONF_DRAWING
from .utils_math import nearest, rotate, nearest2
from .utils_drawing import draw2D
from scipy.spatial import distance

variogram_types = [
    'constant', 'gauss', 'exponential', 'spherical', 'cubic', 'potential', 'lineal','cuadratico'
]


def print_types():
    """print variogram types
    """
    for t in variogram_types:
        print(t)

    return variogram_types


class theoretical(object):
    """Class for unidimensional 1D Variogram.

    Parameters:
        h (numpy.array[:,2]): linear array with positions hx, hy.
        variogram (numpy.array): linear array with equidistant positions.
        parameters (dict):
        type (str):

    Attributes:
        self.x (numpy.array): .
    """

    def format(self, h, make_2D=True):

        if isinstance(h, (list, tuple)):
            h = np.array(h)

        if make_2D is True:
            hx, hy = h
            Hx, Hy = np.meshgrid(hx, hy)
            H = (Hx, Hy)
        else:
            H = None

        return h, H

    def __init__(self, h=None, make_2D=True):

        h, H = self.format(h, make_2D)

        self.h = h
        self.H = H
        self.variogram = None
        self.parameters = None
        self.type = None

    def __str__(self):
        """Represents main data of the atributes."""

        hx, hy = self.h
        print("name = {}\n".format(self.type))
        print("hx: min - {}, max - {}".format(hx.min(), hx.max()))
        print("hx: min - {}, max - {}".format(hy.min(), hy.max()))
        print("variogram: min - {}, max - {}".format(self.variogram.min(),
                                                     self.variogram.max()))
        print("parameters = {}".format(self.parameters))
        return ("")

    def types(self):
        return print_types()

    # def __getattr__(self, name: str):
    #     print("in getter")
    #     return self.__dict__[f"_{name}"]

    # def __setattr__(self, name, value):
    #     print("in setter")
    #     self.__dict__[f"_{name}"] = value

    def get(self, h, type=None, parameters=None):
        """Get result, using self.parameters and name.

        Args:
            h (_type_): _description_
        """
        if type is None:
            type = self.type
        if parameters is None:
            parameters == self.parameters
            print(self.parameters)

        if type == 'constant':
            var_new = self.constant(h=h, **self.parameters)

        elif type == 'gauss':
            var_new = self.gauss(h=h, **self.parameters)

        elif type == 'exponential':
            var_new = self.exponential(h=h, **self.parameters)

        elif type == 'spherical':
            var_new = self.spherical(h=h, **self.parameters)
        
        elif type == 'lineal':
            var_new = self.lineal(h=h, **self.parameters)

        elif type == 'cuadratico':
            var_new = self.cuadratico(h=h, **self.parameters)
            
        elif type == 'cubic':
            var_new = self.cubic(h=h, **self.parameters)

        elif type == 'potential':
            var_new = self.potential(h=h, **self.parameters)

        return var_new
    


    def constant(self, sc, A=None, lc=None, angle=None, power=None, h=None):
        """variogram constante
        A y lc son inutiles, por continuidad con el resto
        """
        if h is None:
            h = self.h
            has_return = False
            self.type = 'constant'
            self.parameters = dict(sc=sc, A=A, lc=lc, angle=angle, power=power)
        else:
            has_return = True

        variogram = sc**2 * np.ones_like(h)
        if has_return is False:
            self.variogram = variogram
        else:
            return variogram
        
    def lineal(self, sc, A=None, lc=None, angle=None, power=None, h=None):
        """variogram lineal
            Realizado por alumno
        """
        if h is None:
            h = self.h

            has_return = False
            self.type = 'lineal'
            self.parameters = dict(sc=sc, A=A, lc=lc, angle=angle, power=power)
        else:
            has_return = True

        if self.H is not None:
            H = self.H
            Hx, Hy = H
            has_return = False
            self.type = 'lineal'
            self.parameters = dict(sc=sc, A=A, lc=lc, angle=angle, power=power)
        else:
            has_return = True

        hx = h[0]
        hy = h[1]

        lcx, lcy = lc
        #hxrot, hyrot = rotate(Hx, Hy, angle, (0, 0))
        hxrot, hyrot = rotate(hx, hy, angle, (0, 0))

        variogram = sc + lcx*hxrot +lcy*hyrot
        if has_return is False:
            self.variogram = variogram
        else:
            return variogram
        
    def cuadratico(self, sc, A=None, lc=None, angle=None, power=None, h=None):
        """variogram lineal
            Realizado por alumno
        """
        if h is None:
            h = self.h

            has_return = False
            self.type = 'cuadratico'
            self.parameters = dict(sc=sc, A=A, lc=lc, angle=angle, power=power)
        else:
            has_return = True

        if self.H is not None:
            H = self.H
            Hx, Hy = H
            has_return = False
            self.type = 'cuadratico'
            self.parameters = dict(sc=sc, A=A, lc=lc, angle=angle, power=power)
        else:
            has_return = True

        hx = Hx[0]
        hy = Hy[1]

        lcx, lcy = lc
        #hxrot, hyrot = rotate(Hx, Hy, angle, (0, 0))
        hxrot, hyrot = rotate(hx, hy, angle, (0, 0))

        variogram = sc + lcx*(hx**2) +lcy*(hy**2) 
        self.variogram = variogram
        if has_return is False:
            self.variogram = variogram
        else:
            return variogram

    def linear(self, sc,  lc, angle=0, power=None, h=None):
        """Linear variogram
        """
        if h is None:
            h = self.h

            has_return = False
            self.type = 'linear'
            self.parameters = dict(sc=sc, A=0, lc=lc, angle=angle, power=power)
        else:
            has_return = True

        if self.H is not None:
            H = self.H
            Hx, Hy = H
            has_return = False
            self.type = 'linear'
            self.parameters = dict(sc=sc, A=0, lc=lc, angle=angle, power=power)
        else:
            has_return = True

        hx = h[0]
        hy = h[1]

        if isinstance(lc, (float, int)):
            lc = (lc, lc)

        lcx, lcy = lc
        #hxrot, hyrot = rotate(Hx, Hy, angle, (0, 0))
        hxrot, hyrot = rotate(hx, hy, angle, (0, 0))

        variogram = sc**2 + lcx * hxrot + lcy * hyrot
        if has_return is False:
            self.variogram = variogram
        else:
            return variogram

    def gauss(self, sc, A, lc, angle=0, power=None, h=None):
            """variogram gaussiano"""
            if h is None:
                h = self.h

                has_return = False
                self.type = 'gauss'
                self.parameters = dict(sc=sc, A=A, lc=lc, angle=angle, power=power)
            else:
                has_return = True

            if self.H is not None:
                H = self.H
                Hx, Hy = H
                has_return = False
                self.type = 'gauss'
                self.parameters = dict(sc=sc, A=A, lc=lc, angle=angle, power=power)
            else:
                has_return = True

            hx = h[0]
            hy = h[1]

            if isinstance(lc, (float, int)):
                lc = (lc, lc)

            lcx, lcy = lc
            #hxrot, hyrot = rotate(Hx, Hy, angle, (0, 0))
            hxrot, hyrot = rotate(hx, hy, angle, (0, 0))

            variogram = sc**2 + A * (
                1 - np.exp(-hxrot**2 / lcx**2 - hyrot**2 / lcy**2))
            if has_return is False:
                self.variogram = variogram
            else:
                return variogram

    def exponential(self, sc, A, lc, angle=0, power=None, h=None):
        """variogram exponencial"""
        if h is None:
            h = self.h
            has_return = False
            self.type = 'exponential'
            self.parameters = dict(sc=sc, A=A, lc=lc, angle=angle, power=power)
        else:
            has_return = True

        if isinstance(lc, (float, int)):
            lc = (lc, lc)

        hx = h[0]
        hy = h[1]

        lcx, lcy = lc
        #hxrot, hyrot = rotate(Hx, Hy, angle, (0, 0))
        hxrot, hyrot = rotate(hx, hy, angle, (0, 0))

        variogram = sc**2 + A * (1 - np.exp(-hxrot / lcx - hyrot / lcy))
        if has_return is False:
            self.variogram = variogram
        else:
            return variogram

    def spherical(self, sc, A, lc, angle=0, power=None, h=None):
        """variogram spherical"""
        if h is None:
            h = self.h
            has_return = False
            self.type = 'spherical'
            self.parameters = dict(sc=sc, A=A, lc=lc, angle=angle, power=power)
        else:
            has_return = True

        if isinstance(lc, (float, int)):
            lc = (lc, lc)

        hx = h[0]
        hy = h[1]

        lcx, lcy = lc
        #hxrot, hyrot = rotate(Hx, Hy, angle, (0, 0))
        hxrot, hyrot = rotate(hx, hy, angle, (0, 0))

        variogram = sc**2 + A * (1.5 * hxrot / lcx - 0.5 * (hxrot / lcx)**3)
        if (h > lc).any():
            variogram[h > lc] = sc**2 + A
        variogram = variogram
        if has_return is False:
            self.variogram = variogram
        else:
            return variogram

    def cubic(self, sc, A, lc, angle=0, power=None, h=None):
        """variogram cúbico"""
        if h is None:
            h = self.h
            has_return = False
            self.parameters = dict(sc=sc, A=A, lc=lc, angle=angle, power=power)
            self.type = 'cubic'
        else:
            has_return = True

        if isinstance(lc, (float, int)):
            lc = (lc, lc)

        hx = h[0]
        hy = h[1]

        h_normx = hx / lc[0]
        h_normy = hx / lc[1]

        lcx, lcy = lc
        #hxrot, hyrot = rotate(Hx, Hy, angle, (0, 0))
        hxrot, hyrot = rotate(hx, hy, angle, (0, 0))

        variogram = sc**2 + A * (7 * h_normx**2 - 35 / 4 * h_normx**3 +
                                 7 / 2 * h_normx**5 - 3 / 4 * h_normx**7) * (
                                     7 * h_normy**2 - 35 / 4 * h_normy**3 +
                                     7 / 2 * h_normy**5 - 3 / 4 * h_normy**7)
        if (hx > lcx).any():
            variogram[hx > lcx] = sc**2 + A
        if (hy > lcy).any():
            variogram[hy > lcy] = sc**2 + A

        variogram = variogram
        if has_return is False:
            self.variogram = variogram
        else:
            return variogram

    def potential(self, sc, A, lc, angle=0, power=None, h=None):
        """variogram potencial"""
        if h is None:
            h = self.h
            has_return = False
            self.parameters = dict(sc=sc, A=A, lc=lc, angle=angle, power=power)
            self.type = 'potential'
        else:
            has_return = True

        if isinstance(lc, (float, int)):
            lc = (lc, lc)

        hx = h[0]
        hy = h[1]

        lcx, lcy = lc
        #hxrot, hyrot = rotate(Hx, Hy, angle, (0, 0))
        hxrot, hyrot = rotate(hx, hy, angle, (0, 0))

        variogram = sc**2 + A * ((hx / lcx)**power + (hy / lcy)**power)
        if has_return is False:
            self.variogram = variogram
        else:
            return variogram

    def draw(self,
             kind='sqrt',
             logarithm=False,
             normalize='maximum',
             title="",
             cut_value=None,
             colormap_kind='',
             reduce_matrix='standard',
             **kwargs):
        """Draws intensity  XY field.

        Parameters:
            logarithm (bool): If True, intensity is scaled in logarithm
            normalize (str):  False, 'maximum', 'area', 'intensity'
            title (str): title for the drawing
            cut_value (float): if provided, maximum value to show
        """

        hx, hy = self.h
        print(hx.shape, hy.shape)

        if colormap_kind in ['', None, []]:
            colormap_kind = CONF_DRAWING["color_variogram"]

        if kind == 'sqrt':
            drawing = np.sqrt(self.variogram)
        else:
            drawing = self.variogram

        id_fig, IDax, IDimage = draw2D(drawing,
                                       hx,
                                       hy,
                                       xlabel="$h_x$",
                                       ylabel="$h_y$",
                                       title=title,
                                       color=colormap_kind,
                                       reduce_matrix=reduce_matrix,
                                       **kwargs)
        plt.tight_layout()

        return id_fig, IDax, IDimage

    def draw2(self, kind='sqrt'):
        """Draws teh variogram

        Args:
            kind (str, optional): 'sqrt' or 'normal'. Defaults to 'normal'.
        """

        h = self.h
        hx = h[0]
        hy = h[1]

        extent = [hx.min(), hx.max(), hy.min(), hy.max()]

        plt.figure()

        if kind == 'sqrt':
            drawing = np.sqrt(self.variogram)
        else:
            drawing = self.variogram

        plt.imshow(drawing, origin='lower', extent=extent)

        plt.xlim(hx.min(), hx.max())
        plt.ylim(hy.min(), hy.max())
        plt.legend()

    def draw3(self, kind='sqrt'):

        hx, hy = self.h

        if kind == 'sqrt':
            z = np.sqrt(self.variogram)
        else:
            z = self.variogram

        fig, ax = plt.subplots()
        ax.plot(hx, hy, 'o', markersize=2, color='red')
        ax.tripcolor(hx, hy, z, cmap=CONF_DRAWING['color_variogram'])
        plt.xlim(hx.min(), hx.max())
        plt.ylim(hy.min(), hy.max())


        plt.show()

    def draw_luis(self):

        variogram=self.variogram
        num=np.sqrt(len(variogram))
        num=int(num)
        variogram=variogram.reshape((num,num))
        hx,hy=self.H
        plt.figure()
        plt.imshow(variogram,
                   origin='lower')
        
        plt.colorbar()
        plt.title('2D Variogram')
        plt.figure()
        plt.plot((variogram[0, :]), 'r', label=' 0º')
        plt.plot((np.diag(variogram)), 'g', label='45º')
        plt.plot((variogram[:, 0]), 'b', label='90º')
        plt.ylim(0)
        plt.legend()
        plt.title('Cortes en direcciones principales')
        plt.ylabel('$\gamma(h)$')
        plt.xlabel('$h$')


class experimental(object):
    """Class for unidimensional experimental 1D Variogram.

    Parameters:
        h (numpy.array): linear array with equidistant positions.
            The number of data is preferibly :math:`2^n` .

    Attributes:
        self.x (numpy.array): Linear array with equidistant positions.
            The number of data is preferibly :math:`2^n`.
    """

    def __init__(self, sensors):
        """Starts an experimental array

        Args:
            x (np.array): x position of data
            y (np.array): y position of data
            z (np.array): z value of data
        """

        self.sensors = sensors
        self.x = sensors[:, 0]
        self.y = sensors[:, 1]
        self.z = sensors[:, 2]
        self.h = None
        self.variogram = None
        self.num_h = None
        self.i_pos = None

        num_sensors = len(self.x)
        positions = np.zeros((num_sensors, 2), dtype=float)
        positions[:, 0] = sensors[:, 0]
        positions[:, 1] = sensors[:, 1]

        self.positions = positions

    def __str__(self):
        """Represents main data of the atributes."""

        print("name = {}\n".format(self.type))
        print("h: min - {}, max - {}".format(self.h.min(), self.h.max()))
        print("variogram: min - {}, max - {}".format(self.variogram.min(),
                                                     self.variogram.max()))
        print("parameters = {}".format(self.parameters))
        return ("")

    # def __getattr__(self, name: str):
    #     print("in getter")
    #     return self.__dict__[f"_{name}"]

    # def __setattr__(self, name, value):
    #     print("in setter")
    #     self.__dict__[f"_{name}"] = value

    def compute(self, hx_var, hy_var, remove_origin=True, has_draw=True):

        sensors = self.sensors
        positions = self.positions
        num_sensors = len(sensors)

        self.h = [hx_var[0:len(hx_var)-1], hy_var[0:len(hx_var)-1]]
        # positions = np.zeros((num_sensors, 2), dtype=float)
        # positions[:, 0] = sensors[:, 0]
        # positions[:, 1] = sensors[:, 1]

        ihx_var = np.arange(0, len(hx_var)-1)
        ihy_var = np.arange(0, len(hy_var)-1)
        

        variogram = np.zeros((len(ihx_var), len(ihy_var)), dtype=float)
        num_data = np.zeros((len(ihx_var), len(ihy_var)), dtype=float)

        dist_x = np.abs(positions[:, 0] - positions[:, 0, np.newaxis])
        dist_y = np.abs(positions[:, 1] - positions[:, 1, np.newaxis])

        i_distances_x, _, _ = nearest2(hx_var, dist_x)
        i_distances_y, _, _ = nearest2(hy_var, dist_y)

        #quitar los de abajo, ya que suben el valor de 0 y no cuadra
        i_distances_x = i_distances_x.astype(dtype=float)
        i_distances_y = i_distances_y.astype(dtype=float)
        i_distances_x[np.tril_indices(i_distances_x.shape[0], -1)] = np.nan
        i_distances_y[np.tril_indices(i_distances_y.shape[0], -1)] = np.nan

        for i_h, kx in enumerate(ihx_var):
            for j_h, ky in enumerate(ihy_var):
                print("{}/{}".format(i_h, len(ihx_var)), end='\r')
                has_condition_distance_x = i_distances_x == i_h
                has_condition_distance_y = i_distances_y == j_h

                x_true, y_true = np.where(has_condition_distance_x *
                                          has_condition_distance_y)

                n_data = (has_condition_distance_x *
                          has_condition_distance_y).sum()
                num_data[kx, ky] = n_data

                if n_data != 0:
                    variogram_j = ((sensors[x_true, 2] - sensors[y_true, 2])**
                                   2).sum() / (2 * n_data)
                else:
                    variogram_j = None

                variogram[kx, ky] = variogram_j

        if remove_origin:
            y2 = np.sqrt(variogram[2, 0])
            y1 = np.sqrt(variogram[1, 0])

            v0 = 2 * y1 - y2
            variogram[0, 0] = v0**2

        #variogram[-1, -1] = (variogram[-2, -1] + variogram[-1, -2]) / 2
        # num_data[-1, -1] = (num_data[-2, -1] + num_data[-1, -2]) / 2

        if has_draw:
            plt.figure()
            plt.imshow((variogram).transpose(),
                       origin='lower',
                       extent=[hx_var[0], hx_var[-1], hy_var[0], hy_var[-1]])
            plt.colorbar()
            plt.title('Variograma 2D')

            plt.figure()
            plt.plot((variogram[:, 0]), 'r', label=' 0º')
            plt.plot((np.diag(variogram)), 'g', label='45º')
            plt.plot((variogram[0, :]), 'b', label='90º')
            plt.ylim(0)
            plt.legend()
            plt.title('Cortes en direcciones principales')
            plt.ylabel('$\gamma(h)$')
            plt.xlabel('$h$')

            #plt.figure()
            #plt.imshow(num_data.transpose(),
            #           origin='lower',
            #           extent=[hx_var[0], hx_var[-1], hy_var[0], hy_var[-1]])

            #plt.colorbar()
            #plt.title('Num Data')
            #plt.clim(0)

        self.variogram=variogram

        return variogram, num_data

    def compute_radial(self, h_var, remove_origin=True, has_draw=True):
        """
        """

        sensors = self.sensors
        num_sensors = len(sensors)

        positions = self.positions

        i_var = np.arange(0, len(h_var))
        variogram = np.zeros_like(i_var, dtype=float)
        num_data = np.zeros_like(i_var, dtype=float)

        d0 = distance.cdist(positions, positions, 'euclidean')
        print(d0.max())

        i_distances, _, _ = nearest2(h_var, d0)

        # i_distances = i_distances.astype(dtype=float)
        # i_distances[np.tril_indices(i_distances.shape[0], -1)] = np.nan

        d0[np.tril_indices(d0.shape[0], -1)] = np.nan

        for i_h, k in enumerate(i_var):

            has_condition_distance = i_distances == i_h

            x_true, y_true = np.where(has_condition_distance)

            n_data = (has_condition_distance).sum()
            num_data[k] = n_data

            if n_data != 0:
                variogram_j = ((sensors[x_true, 2] - sensors[y_true, 2])**
                               2).sum() / (2 * n_data)
            else:
                variogram_j = None

            variogram[k] = variogram_j
            variogram[-1] = variogram[-2]
            num_data[-1] = num_data[-2]
        if remove_origin:
            y2 = np.sqrt(variogram[2])
            y1 = np.sqrt(variogram[1])

            v0 = 2 * y1 - y2
            variogram[0] = v0**2

        if has_draw:
            plt.figure()
            plt.plot(h_var, np.sqrt(variogram), 'ko')
            plt.xlim(0)
            plt.ylim(0)
            plt.figure()
            plt.plot(h_var, num_data, 'ko')
            plt.xlim(0)
            plt.ylim(0)

        self.variogram = variogram
        self.num_data = num_data
        return variogram, num_data

    def get_seed(self):
        """
		calcula los parámetros iniciales para el ajuste del variogram
		"""
        point_central = (self.variogram.max() + self.variogram.min()) / 2
        imenor, _, _ = nearest(self.variogram, point_central)
        param_ini = np.array(
            [self.variogram[0],
             self.variogram.max(), self.h[imenor]])

        if self.function_ajuste in [variogram_potencial]:
            param_ini = np.append(param_ini, [3])

        return param_ini

    def draw(self, kind='sqrt'):
        """Draws teh variogram

        Args:
            kind (str, optional): 'sqrt' or 'normal'. Defaults to 'normal'.
        """
        plt.figure()
        if kind == 'sqrt':
            plt.plot(self.h,
                     np.sqrt(self.variogram),
                     'k',
                     label='experimental')
            plt.ylabel('$\sqrt{\gamma(h)}$')
        else:
            plt.plot(self.h, self.variogram, 'k', label='experimental')
            plt.ylabel('$\gamma(h)$')

        plt.ylim(0)
        plt.xlim(self.h[0], self.h[-1])
        plt.legend()
        plt.xlabel('h')


class fitting(object):
    """Fits a theoretical variogram to experimental variogram.

    Parameters:
        h (numpy.array): 

    Attributes:
        self.x 
    """

    def __init__(
            self,
            var_exp,
            type,
            num_transitions=3,
            num_iters=200,
            num_particles=100,
            # sc, A, lcx, lcy, angle
            min_bound=np.array([0, 0, 0, 0, 0]),
            max_bound=np.array([2, 5, 20, 20, 2 * np.pi]),
            options={
                'c1': 0.5,
                'c2': 0.6,
                'w': 0.9
            }):

        self.var_exp = var_exp
        self.var_theo = theoretical(h=var_exp.h)
        self.var_theo.type = type
        self.type_variogram = type

        bounds = (min_bound, max_bound)

        pso_params = dict()
        pso_params['bounds'] = bounds
        pso_params['options'] = options
        pso_params['num_transitions'] = num_transitions
        pso_params['num_iters'] = num_iters
        pso_params['num_particles'] = num_particles

        self.pso_params = pso_params

        self.cost = []
        self.param_final = []
        self.optimizer = []

    def types(self):
        return print_types()

    def cost_variogram(self, params):
        # sc = params[0], A = params[1],
        # lc = (params[2], params[3]), angle = params[4]

        v2 = self.var_theo.get(self.var_theo.h,
                               type=self.type_variogram,
                               parameters=params)
        self.variogram = v2

        error = np.sqrt(((self.var_exp.variogram - v2)**2).mean())

        return error

    def opt_func(self, params, var_exp):
        n_particles = params.shape[0]
        std = [
            self.cost_variogram(params[i, :], var_exp)
            for i in range(n_particles)
        ]
        return std

    def execute(self):
        num_transitions = self.pso_params['num_transitions']
        num_iters = self.pso_params['num_iters']
        num_particles = self.pso_params['num_particles']
        options = self.pso_params['options']
        bounds = self.pso_params['bounds']

        optimizer = GlobalBestPSO(n_particles=num_particles,
                                  dimensions=num_transitions,
                                  options=options,
                                  bounds=bounds)
        # cost, param_final = optimizer.optimize(self.opt_func,
        #                                      iters=num_iters,
        #                                      var_exp=self.var_exp)

        cost, param_final = optimizer.optimize(opt_func2,
                                               iters=num_iters,
                                               self2=self,
                                               cost_function=cost_variogram2)

        self.cost = cost
        self.param_final = param_final
        if len(param_final) == 3:
            param_final = np.append(param_final, 2)

        self.params_dict = dict(sc=param_final[0],
                                A=param_final[1],
                                lc=(param_final[2], param_final[3]),
                                angle=param_final[4])

        self.var_theo.parameters = self.params_dict
        self.var_theo.type = self.type_variogram

        self.optimizer = optimizer

        return cost, param_final, optimizer
    

    def fit_lineal(self):
        var_exp=self.var_exp.variogram
        h=self.var_exp.h
        [hy,hx]=h


        lcx, sc1= np.polyfit(hx,var_exp[0,:],1)
        lcy, sc2= np.polyfit(hy,var_exp[:,0],1)
        sc=(sc1+sc2)/2

        lc=[lcx,lcy]
        
        self.params_dict = dict(sc=sc,
                                A=None,
                                lc=lc,
                                power=None)


        return lc, sc
    

    def fit_cuadratico(self):
        var_exp=self.var_exp.variogram
        h=self.var_exp.h

        [hy,hx]=h
        

        

        lcx,A1, sc1= np.polyfit(hx,var_exp[0,:],2)
        lcy,A2, sc2= np.polyfit(hy,var_exp[:,0],2)

        sc=(sc1+sc2)/2

        lc=[lcx,lcy]
        A=(A1+A2)/2
        param_final=[sc,A,lc]
        
        self.param_final = param_final

        self.params_dict = dict(sc=sc,
                                A=A,
                                lc=lc,
                                angle=None)

        self.var_theo.parameters = self.params_dict
        self.var_theo.type = self.type_variogram



        return lc, A, sc
    

    def draw_history(self):
        """Draw history
        """
        plot_cost_history(self.optimizer.cost_history)
        plt.show()

    def draw_fitting(self, kind='sqrt'):
        """Draw fitting

        Args:
            kind (str, optional): _description_. Defaults to 'sqrt'.
        """

        h = self.var_exp.h

        self.var_theo.parameters = self.params_dict

        v2 = self.var_theo.get(h,
                               parameters=self.params_dict,
                               type=self.type_variogram)
        self.variogram = v2
        self.var_theo.variogram = v2

        # var_theo.exponential(sc, A, lc)
        self.var_theo.draw(kind)
        if kind == 'normal':
            #plt.plot(h, v2, 'g')
            plt.plot(self.var_exp.h, self.var_exp.variogram)
        elif kind == 'sqrt':
            #plt.plot(h, np.sqrt(v2), 'g')
            plt.plot(self.var_exp.h, np.sqrt(self.var_exp.variogram))
        else:
            print("kind error")
        plt.show()


def opt_func2(params, self2, cost_function):
    n_particles = params.shape[0]
    std = [cost_function(params[i, :], self2) for i in range(n_particles)]
    return std


def cost_variogram2(params, self2):

    if self2.type_variogram == 'gauss':
        self2.var_theo.gauss(*params)

    elif self2.type_variogram == 'constant':
        self2.var_theo.constant(*params)

    elif self2.type_variogram == 'gauss':
        self2.var_theo.gauss(*params)

    elif self2.type_variogram == 'exponential':
        self2.var_theo.exponential(*params)
    elif self2.type_variogram == 'spherical':
        self2.var_theo.spherical(*params)

    elif self2.type_variogram == 'cubic':
        self2.var_theo.cubic(*params)

    elif self2.type_variogram == 'potential':
        self2.var_theo.potential(*params)

    elif self2.type_variogram == 'cuadratico':
        self2.var_theo.cuadratico(*params)

    elif self2.type_variogram == 'lineal':
        self2.var_theo.lineal(*params)

    std = np.sqrt(
        ((self2.var_exp.variogram - self2.var_theo.variogram)**2).mean())

    return std