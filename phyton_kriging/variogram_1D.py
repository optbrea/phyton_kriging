# !/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This module generates Variogram class for theoretical and experimental variograms, including fitting

"""

from . import np, plt
from .utils_math import nearest
from pyswarms.single.global_best import GlobalBestPSO
from pyswarms.utils.plotters import plot_cost_history

variogram_types = [
    'constant', 'gauss', 'exponential', 'spherical', 'cubic', 'lineal',
    'cuadratico'
]
#, 'potential'


def print_types():
    """print variogram types
    """
    for t in variogram_types:
        print(t)

    return variogram_types


class theoretical(object):
    """Class for unidimensional 1D Variogram.

    Parameters:
        h (numpy.array): linear array with equidistant positions.
            The number of data is preferibly :math:`2^n` .
        variogram (numpy.array): linear array with equidistant positions.
            The number of data is preferibly :math:`2^n` .
        parameters (dict):
        type (str):

    Attributes:
        self.x (numpy.array): .
    """

    def __init__(self, h=None):
        self.h = h
        self.variogram = None
        self.parameters = None
        self.type = None

    def __str__(self):
        """Represents main data of the atributes."""

        print("name = {}\n".format(self.type))
        print("h: min - {}, max - {}".format(self.h.min(), self.h.max()))
        print("variogram: min - {}, max - {}".format(self.variogram.min(),
                                                     self.variogram.max()))
        print("parameters = {}".format(self.parameters))
        return ("")

    def types(self):
        return print_types()

    # def __getattr__(self, name: str):
    #     print("in getter")
    #     return self.__dict__[f"_{name}"]

    # def __setattr__(self, name, value):
    #     print("in setter")
    #     self.__dict__[f"_{name}"] = value

    def get(self, h, type=None, parameters=None):
        """Get result, using self.parameters and name.

        Args:
            h (_type_): _description_
        """
        if type is None:
            type = self.type

        if parameters is None:
            parameters == self.parameters

        if type == 'constant':
            var_new = self.constant(h=h, **self.parameters)

        elif type == 'gauss':
            var_new = self.gauss(h=h, **self.parameters)

        elif type == 'exponential':
            var_new = self.exponential(h=h, **self.parameters)

        elif type == 'spherical':
            var_new = self.spherical(h=h, **self.parameters)

        elif type == 'cubic':
            var_new = self.cubic(h=h, **self.parameters)

        elif type == 'potential':
            var_new = self.potential(h=h, **self.parameters)

        elif type == 'cuadratico':
            var_new = self.cuadratico(h=h, **self.parameters)

        elif type == 'lineal':
            var_new = self.lineal(h=h, **self.parameters)

        return var_new

    def constant(self, sc, A=None, lc=None, power=None, h=None):
        """variogram constante
        A y lc son inutiles, por continuidad con el resto
        """
        if h is None:
            h = self.h
            has_return = False
            self.type = 'constant'
            self.parameters = dict(sc=sc, A=A, lc=lc, power=power)
        else:
            has_return = True

        variogram = sc**2 * np.ones_like(h)
        if has_return is False:
            self.variogram = variogram
        else:
            return variogram

    def cuadratico(self, sc, A=None, lc=None, power=None, h=None):
        """variogram cuadratico
        desarrollado por alumno
        """
        if h is None:
            h = self.h
            has_return = False
            self.parameters = dict(sc=sc, A=A, lc=lc, power=power)
            self.type = 'cuadratico'
        else:
            has_return = True

        variogram = sc + A * h + lc * (h**2)

        if has_return is False:
            self.variogram = variogram
        else:
            return variogram

    def lineal(self, sc, A=None, lc=None, power=None, h=None):
        """variogram lineal
        desarrollado por alumno
        """
        if h is None:
            h = self.h
            has_return = False
            self.parameters = dict(sc=sc, A=A, lc=lc, power=power)
            self.type = 'lineal'
        else:
            has_return = True

        variogram = sc + lc * h

        if has_return is False:
            self.variogram = variogram
        else:
            return variogram

    def gauss(self, sc, A, lc, power=None, h=None):
        """variogram gaussiano"""
        if h is None:
            h = self.h
            has_return = False
            self.type = 'gauss'
            self.parameters = dict(sc=sc, A=A, lc=lc, power=power)
        else:
            has_return = True

        # print(sc, A, lc, len(h))
        variogram = sc**2 + A * (1 - np.exp(-h**2 / lc**2))

        if has_return is False:
            self.variogram = variogram
        else:
            return variogram

    def exponential(self, sc, A, lc, power=None, h=None):
        """variogram exponencial"""
        if h is None:
            h = self.h
            has_return = False
            self.type = 'exponential'
            self.parameters = dict(sc=sc, A=A, lc=lc, power=power)
        else:
            has_return = True

        variogram = sc**2 + A * (1 - np.exp(-h / lc))

        if has_return is False:
            self.variogram = variogram
        else:
            return variogram

    def spherical(self, sc, A, lc, power=None, h=None):
        """variogram spherical"""
        if h is None:
            h = self.h
            has_return = False
            self.type = 'spherical'
            self.parameters = dict(sc=sc, A=A, lc=lc, power=power)
        else:
            has_return = True

        variogram = sc**2 + A * (1.5 * h / lc - 0.5 * (h / lc)**3)

        if (h > lc).any():
            variogram[h > lc] = sc**2 + A
        variogram = variogram
        if has_return is False:
            self.variogram = variogram
        else:
            return variogram

    def cubic(self, sc, A, lc, power=None, h=None):
        """variogram cúbico"""
        if h is None:
            h = self.h
            has_return = False
            self.parameters = dict(sc=sc, A=A, lc=lc, power=power)
            self.type = 'cubic'
        else:
            has_return = True
        h_norm = h / lc
        variogram = sc**2 + A * (7 * h_norm**2 - 35 / 4 * h_norm**3 +
                                 7 / 2 * h_norm**5 - 3 / 4 * h_norm**7)

        if (h > lc).any():
            variogram[h > lc] = sc**2 + A

        # variogram = variogram

        if has_return is False:
            self.variogram = variogram
        else:
            return variogram

    def potential(self, sc, A, lc, power=None, h=None):
        """variogram potencial"""
        if h is None:
            h = self.h
            has_return = False
            self.parameters = dict(sc=sc, A=A, lc=lc, power=power)
            self.type = 'potential'
        else:
            has_return = True

        variogram = sc**2 + A * (h / lc)**power

        if has_return is False:
            self.variogram = variogram
        else:
            return variogram

    def draw(self, kind='sqrt', label='theory'):
        """Draws the variogram

        Args:
            kind (str, optional): 'sqrt' or 'normal'. Defaults to 'sqrt'.
            title (str, optional). Title on plot legend. Default to 'theory.
        """
        plt.figure()
        if kind == 'sqrt':
            plt.plot(self.h, np.sqrt(self.variogram), 'k', label=label)
            plt.ylabel('$\sqrt{\gamma(h)}$')
        else:
            plt.plot(self.h, self.variogram, 'k', label=label)
            plt.ylabel('$\gamma(h)$')

        plt.ylim(0)
        plt.xlim(self.h[0], self.h[-1])
        plt.legend()
        plt.xlabel('h')


class experimental(object):
    """Class for unidimensional experimental 1D Variogram.

    Parameters:
        h (numpy.array): linear array with equidistant positions.
            The number of data is preferibly :math:`2^n` .

    Attributes:
        self.x (numpy.array): Linear array with equidistant positions.
            The number of data is preferibly :math:`2^n`.
    """

    def __init__(self, x, y):

        self.x = x
        self.y = y
        self.h = None
        self.variogram = None
        self.num_h = None
        self.i_pos = None

    def __str__(self):
        """Represents main data of the atributes."""

        print("name = {}\n".format(self.type))
        print("h: min - {}, max - {}".format(self.h.min(), self.h.max()))
        print("variogram: min - {}, max - {}".format(self.variogram.min(),
                                                     self.variogram.max()))
        print("parameters = {}".format(self.parameters))
        return ("")

    # def __getattr__(self, name: str):
    #     print("in getter")
    #     return self.__dict__[f"_{name}"]

    # def __setattr__(self, name, value):
    #     print("in setter")
    #     self.__dict__[f"_{name}"] = value

    def compute(self, ipoints):
        """compute the experimental variogram

        Args:
            ipoints (integers): compute at points where i_points = np.arange(integer, integer, integer)
        """
        variogram = np.zeros_like(ipoints, dtype=float)
        num_points = np.zeros_like(ipoints, dtype=int)

        num_data = len(self.y)
        num_h = ipoints.size

        data = self.y

        for i in np.arange(0, num_h):
            hi = ipoints[i]
            VAR = 0
            n_data = 0
            for j in np.arange(1, hi + 1):
                positions = np.arange(j, num_data, hi, dtype='int')
                values = self.y[positions]
                diffs = np.diff(values)**2
                VAR = VAR + diffs.sum()
                n_data = n_data + diffs.size

            if n_data > 0:
                variogram[i] = VAR / (2 * n_data)
            else:
                variogram[i] = -1
            num_points[i] = n_data

        # Remove points with no data
        i_remove = np.argwhere(num_points == 0)
        # data = np.delete(data, i_remove)
        variogram = np.delete(variogram, i_remove)
        num_points = np.delete(num_points, i_remove)
        ipoints = np.delete(ipoints, i_remove)

        incr_h = self.x[1] - self.x[0]
        self.h = ipoints * incr_h
        self.variogram = variogram
        self.num_h = num_points
        self.i_pos = ipoints
        # self.imax = len(self.h)

    def get_seed(self):
        """
		calcula los parámetros iniciales para el ajuste del variogram
		"""
        point_central = (self.variogram.max() + self.variogram.min()) / 2
        imenor, _, _ = nearest(self.variogram, point_central)
        param_ini = np.array(
            [self.variogram[0],
             self.variogram.max(), self.h[imenor], 2])

        # if self.function_ajuste in [variogram_types]:
        #     param_ini = np.append(param_ini, [3])

        return param_ini

    def draw(self, kind='sqrt'):
        """Draws the variogram.

        Args:
            kind (str, optional): 'sqrt' or 'normal'. Defaults to 'normal'.
        """
        plt.figure()
        if kind == 'sqrt':
            plt.plot(self.h,
                     np.sqrt(self.variogram),
                     'k',
                     label='experimental')
            plt.ylabel('$\sqrt{\gamma(h)}$')
        else:
            plt.plot(self.h, self.variogram, 'k', label='experimental')
            plt.ylabel('$\gamma(h)$')

        plt.ylim(0)
        plt.xlim(self.h[0], self.h[-1])
        plt.legend()
        plt.xlabel('h')


class fitting(object):
    """Fits a theoretical variogram to experimental variogram.

    Parameters:
        h (numpy.array): 

    Attributes:
        self.x 
    """

    def __init__(self,
                 var_exp,
                 type,
                 num_transitions=3,
                 num_iters=200,
                 num_particles=100,
                 min_bound=np.array([0, 0, 0]),
                 max_bound=np.array([2, 5, 20]),
                 options={
                     'c1': 0.5,
                     'c2': 0.6,
                     'w': 0.9
                 }):

        self.var_exp = var_exp
        self.var_theo = theoretical(h=self.var_exp.h)
        self.var_theo.type = type
        self.type_variogram = type

        bounds = (min_bound, max_bound)

        pso_params = dict()
        pso_params['bounds'] = bounds
        pso_params['options'] = options
        pso_params['num_transitions'] = num_transitions
        pso_params['num_iters'] = num_iters
        pso_params['num_particles'] = num_particles

        self.pso_params = pso_params

        self.cost = []
        self.param_final = []
        self.optimizer = []

    def types(self):
        return print_types()

    def cost_variogram(self, params):
        # sc = params[0],   A = params[1],        lc = params[2]

        v2 = self.var_theo.get(self.var_theo.h,
                               type=self.type_variogram,
                               parameters=params)
        self.variogram = v2

        error = np.sqrt(((self.var_exp.variogram - v2)**2).mean())

        return error

    def opt_func(self, params, var_exp):
        n_particles = params.shape[0]
        std = [
            self.cost_variogram(params[i, :], var_exp)
            for i in range(n_particles)
        ]
        return std

    def fit_lineal(self):
        var_exp = self.var_exp.variogram
        h = self.var_exp.h

        lc, sc = np.polyfit(h, var_exp, 1)

        self.params_dict = dict(sc=sc, A=None, lc=lc, power=None)

        return lc, sc

    def fit_cuadratico(self):
        var_exp = self.var_exp.variogram
        h = self.var_exp.h

        lc, A, sc = np.polyfit(h, var_exp, 2)

        self.params_dict = dict(sc=sc, A=A, lc=lc, power=None)

        return lc, A, sc

    def execute(self):
        num_transitions = self.pso_params['num_transitions']
        num_iters = self.pso_params['num_iters']
        num_particles = self.pso_params['num_particles']
        options = self.pso_params['options']
        bounds = self.pso_params['bounds']

        optimizer = GlobalBestPSO(n_particles=num_particles,
                                  dimensions=num_transitions,
                                  options=options,
                                  bounds=bounds)

        cost, param_final = optimizer.optimize(opt_func2,
                                               iters=num_iters,
                                               self2=self,
                                               cost_function=cost_variogram2)

        self.cost = cost
        self.param_final = param_final
        if len(param_final) == 3:
            param_final = np.append(param_final, 2)

        self.params_dict = dict(sc=param_final[0],
                                A=param_final[1],
                                lc=param_final[2],
                                power=param_final[3])

        self.var_theo.parameters = self.params_dict
        self.var_theo.type = self.type_variogram

        self.optimizer = optimizer

        return cost, param_final, optimizer

    def draw_history(self):
        """Draw history
        """
        plot_cost_history(self.optimizer.cost_history)
        plt.show()

    def draw_fitting(self, kind='sqrt'):
        """Draw fitting

        Args:
            kind (str, optional): _description_. Defaults to 'sqrt'.
        """

        h = self.var_exp.h

        self.var_theo.parameters = self.params_dict

        v2 = self.var_theo.get(h,
                               parameters=self.params_dict,
                               type=self.type_variogram)
        self.variogram = v2
        self.var_theo.variogram = v2

        # var_theo.exponential(sc, A, lc)
        self.var_theo.draw(kind, label='fitting')
        if kind == 'normal':
            #plt.plot(h, v2, 'g')
            plt.plot(self.var_exp.h,
                     self.var_exp.variogram,
                     'r',
                     label='experimental')
        elif kind == 'sqrt':
            #plt.plot(h, np.sqrt(v2), 'g')
            plt.plot(self.var_exp.h,
                     np.sqrt(self.var_exp.variogram),
                     'r',
                     label='experimental')
        else:
            print("kind error")
        plt.legend()


def opt_func2(params, self2, cost_function):
    n_particles = params.shape[0]
    std = [cost_function(params[i, :], self2) for i in range(n_particles)]
    return std


def cost_variogram2(params, self2):

    if self2.type_variogram == 'gauss':
        self2.var_theo.gauss(*params)

    elif self2.type_variogram == 'constant':
        self2.var_theo.constant(*params)

    elif self2.type_variogram == 'cuadratico':
        self2.var_theo.constant(*params)

    elif self2.type_variogram == 'gauss':
        self2.var_theo.gauss(*params)

    elif self2.type_variogram == 'exponential':
        self2.var_theo.exponential(*params)
    elif self2.type_variogram == 'spherical':
        self2.var_theo.spherical(*params)

    elif self2.type_variogram == 'cubic':
        self2.var_theo.cubic(*params)

    elif self2.type_variogram == 'potential':
        self2.var_theo.potential(*params)

    std = np.sqrt(
        ((self2.var_exp.variogram - self2.var_theo.variogram)**2).mean())

    return std