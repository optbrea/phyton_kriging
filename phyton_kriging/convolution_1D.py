# !/usr/local/bin/python3
# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------
# Name:		krigConv.py
# Purpose:	 Kriging por convolution 1D
#
# Author:	  Luis Miguel Sanchez Brea
#
# Created:	 10/12/2011

# ----------------------------------------------------------------------
"""Kriging por convolution

TODO: 
1. meter errorReal como diferencia entre estimation y signal con data según sampling
2. hacer function para hacer simplificado (utilizable en la optimización)
3. separar la estimation y solo hacerla cuando data no sea nulo
4. hacer lo mismo para el kriging estándard
"""

import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
from scipy.signal import convolve

from .variogram_1D import theoretical
from .standard_1D import kriging1D
from .utils_math import nearest, sampling2


class Convolution_1D(kriging1D):
    """
    Clase en la que se hace la convolución 1D del kriging
    """

    def __init__(self, sensors, variogram, sampling, y_ideal=None):
        """equal than Scalar_field_X"""
        super().__init__(sensors, variogram, sampling, y_ideal)

        self.sc = None
        self.DM = None
        self.Lambda = None
        self.NEQ = None
        self.error_conv = None
        self.estimation_conv = None

        # DM(x,y)
        x = self.sampling - self.sampling.mean()

        self.standard(filtering=True)
        self.compute_DM()
        self.normalize_DM()
        self.center_lambdas()

    def compute_DM(self):
        """_summary_

        Returns:
            _type_: _description_
        """

        variogr = self.variogram.get(self.sampling)
        self.sc = np.sqrt(variogr.min())
        self.DM = self.sc**2 / (2 * variogr - self.sc**2)

        return self.DM

    def normalize_DM(self):
        """_summary_

        Returns:
            _type_: _description_
        """
        self.compute_DM()
        self.DM = self.DM - self.DM.min()
        self.DM = self.DM / self.DM.max()

        return self.DM

    def center_lambdas(self):
        """Esta función toma varias functiones lambdas y las traslada colocando los máximos en el centro
        Los que falta de la función (al ser trasladada hay partes no definidas) las rellena de Nulos        

        Returns:
            _type_: _description_
        """
        m = self.sampling
        k = self.kriging
        s = self.sensors

        incr_x = m[1] - m[0]
        distance = (m[-1] - m[0]) / 2
        long_sampling = len(m)
        xmin = m[0] - 2 * distance
        xmax = m[-1] + 2 * distance
        X = np.arange(xmin, xmax, incr_x)

        # maximum_lambdas = self.lambdas.max()
        pos_max = self.lambdas.argmax(axis=0)

        lambdas_centered = np.zeros((len(X), len(s)), float)
        for i in range(len(s)):
            # pos_inicial = m[0]-m[pos_max[i]]
            # print i, k.lambdas[:,i].shape, pos_max[i],pos_max[i]+long_sampling
            # plt.plot(m.x-m[pos_max[i]],k.lambdas[:,i])
            pos_ini = round(len(X) / 2) - pos_max[i]
            # print pos_ini, len(X), pos_max[i]
            # if k.lambdas[:,i].max() > 0.5*maximum_lambdas:
            lambdas_centered[pos_ini:pos_ini + long_sampling,
                             i] = self.lambdas[:, i]

        Lambda = np.nanmean(lambdas_centered, axis=1)

        i_min, _, _ = nearest(X, m[0])
        i_max, _, _ = nearest(X, m[-1])

        Lambda = Lambda[i_min:i_max]
        X = X[i_min:i_max]
        lambdas_centered = lambdas_centered[i_min:i_max, :]

        self.Lambda = Lambda
        self.X = X
        self.lambdas_centered = lambdas_centered

        return X, Lambda, lambdas_centered

    def compute_NEQ(self):

        xComb, _ = sampling2(xy=self.sensors, sampling=self.sampling)

        # NEQ(x,y)
        self.NEQ = convolve(xComb, self.DM, 'same')
        return self.NEQ

    def compute_estimation_convolution(self, sensors=None):
        """_summary_

        Args:
            sensors (_type_): _description_
        """

        if sensors is None:
            sensors = self.sensors

        xComb, zComb = sampling2(sensors, sampling=self.sampling)

        estimation = convolve(zComb, self.Lambda, 'same')
        lambda_normalized = convolve(xComb, self.Lambda, 'same')
        self.estimation_conv = estimation / lambda_normalized
        return self.estimation_conv

    def compute_error_convolution(self):
        """Compute error

        Args:
            sensors (_type_): _description_
        """

        self.compute_NEQ()
        self.error_conv = np.sqrt(self.sensors[0, 2]**2 +
                                  self.sc**2 / self.NEQ)
        return self.error_conv

    def get(self, kind):
        """Get data from kriging interpolation

        Args:
            kind (string): _description_

        Returns:
            _type_: _description_
        """
        xm = self.sampling

        if kind == 'data':
            output = (self.sensors[:, 0], self.sensors[:, 1], self.sensors[:,
                                                                           2])
        if kind == 'estimation_kriging':
            output = (xm, self.estimation)

        elif kind == 'error_kriging':
            output = (xm, self.error)

        if kind == 'estimation_convolution':
            self.compute_estimation_convolution()
            output = (xm, self.estimation_conv)

        elif kind == 'error_convolution':
            self.compute_error_convolution()
            output = (xm, self.error_conv)

        elif kind == 'errorReal':
            output = (xm, abs(self.sampling.y - self.estimation))

        elif kind == 'lambdas':
            output = (xm, self.lambdas)

        elif kind == 'lambdas_centered':
            output = (xm, self.lambdas_centered)

        elif kind == 'Lambda':
            output = (xm, self.Lambda)

        elif kind == 'DM':
            output = (xm, self.DM)

        elif kind == 'NEQ':
            self.compute_NEQ()
            output = (xm, self.NEQ)

        else:
            print("not proper kind parameter")
            output = None

        return output

    def draw(self, kind='kriging', xlabel='x', ylabel='I(x)', num_lambda=all):
        """		Draws kriging: data, variogram, lambdas, error, kriging

        Args:
            kind (str, optional): _description_. Defaults to 'kriging'.
            xlabel (str, optional): _description_. Defaults to 'x'.
            ylabel (str, optional): _description_. Defaults to 'I'.
            num_lambda (_type_, optional): _description_. Defaults to all.
        """
        sampling = self.sampling
        sensors = self.sensors
        estimation = self.estimation
        krig1 = self

        self.compute_error_convolution()
        self.compute_estimation_convolution()
        self.compute_NEQ()

        if kind == 'data':
            plt.plot(sensors[:, 0], sensors[:, 1], 'g', ms=2, label='data')

        if kind == 'variogram':
            self.variogram.draw()
            plt.ylim(ymin=0)

        elif kind == 'kriging':
            self.draw('data')
            plt.plot(sampling, estimation, 'r', label='kriging')
            if self.y_ideal is not None:
                plt.plot(sampling, self.y_ideal, 'b--', label='ideal')

            plt.fill_between(sampling,
                             self.estimation - self.error,
                             self.estimation + self.error,
                             color='blue',
                             alpha=0.25,
                             label=' $\sigma_{krig}$')

            plt.fill_between(sampling,
                             self.estimation - 2 * self.error,
                             self.estimation + 2 * self.error,
                             color='blue',
                             alpha=0.125,
                             label='$2\sigma_{krig}$')
            plt.xlim(sampling[0], sampling[-1])
            plt.grid('on')
            plt.xlabel(xlabel)
            plt.ylabel(ylabel)
            plt.legend()

        elif kind == 'error_kriging':

            sampling = self.sampling
            krig1 = self
            sensors = self.sensors

            plt.plot(sensors[:, 0],
                     np.zeros_like(sensors[:, 0]),
                     'ko',
                     ms=2,
                     label='sensors')

            if self.y_ideal is not None:
                diferences = self.estimation - self.y_ideal
                plt.plot(sampling, diferences, 'r', label='error')

            plt.fill_between(sampling,
                             -krig1.error,
                             krig1.error,
                             color='blue',
                             alpha=0.25,
                             label=' $\sigma_{krig}$')

            plt.fill_between(sampling,
                             -2 * krig1.error,
                             2 * krig1.error,
                             color='blue',
                             alpha=0.125,
                             label='$2\sigma_{krig}$')

            plt.xlim(sampling[0], sampling[-1])
            plt.grid('on')
            plt.legend()
            plt.xlabel(xlabel)
            plt.ylabel(ylabel)

        elif kind == 'convolution':
            self.get('estimation_convolution')
            self.get('error_convolution')

            x = self.sampling
            y = self.estimation_conv
            error = self.error_conv
            plt.figure()

            self.draw('data')
            plt.plot(sampling, self.estimation, 'r', label='kriging')

            plt.plot(x, y, 'g', label='convolution')
            if self.y_ideal is not None:
                plt.plot(sampling, self.y_ideal, 'b--', label='ideal')
            plt.fill_between(sampling,
                             y - error,
                             y + error,
                             color='blue',
                             alpha=0.25,
                             label=' $\sigma_{conv}$')

            plt.fill_between(sampling,
                             y - 2 * error,
                             y + 2 * error,
                             color='blue',
                             alpha=0.125,
                             label='$2\sigma_{conv}$')

            plt.xlim(x.min(), x.max())
            plt.ylim((y - 2 * error).min(), (y + 2 * error).max())
            plt.grid('on')

            plt.legend()

        elif kind == 'error_convolution':
            self.compute_error_convolution()
            self.draw('error_kriging')
            plt.plot(self.sampling, self.error_conv, 'b', label='conv')
            plt.plot(self.sampling, -self.error_conv, 'b')

            plt.plot(self.sampling,
                     2 * self.error_conv,
                     'b',
                     alpha=0.5,
                     label='2 conv')
            plt.plot(self.sampling, -2 * self.error_conv, 'b', alpha=0.5)
            plt.legend()

        elif kind == 'lambdas':
            plt.plot(sampling, self.lambdas)
            plt.plot(self.X, self.lambdas_centered)
            plt.plot(self.X, self.Lambda, 'k', lw=2)
            plt.xlim(sampling[0], sampling[-1])

        elif kind == 'lambdas_centered':
            self.center_lambdas()
            plt.plot(self.X, self.lambdas_centered)
            plt.plot(self.X, self.Lambda, 'k', lw=2)
            plt.xlim(sampling[0], sampling[-1])

        elif kind == 'Lambda':
            self.center_lambdas()
            plt.plot(self.X, self.Lambda, 'k', lw=2)
            plt.xlim(sampling[0], sampling[-1])

        # elif kind == 'dm':
        #     plt.plot(self.sampling, self.DM, 'k', label='dm')
        #     plt.xlim(self.sampling[0], self.sampling[-1])
        #     plt.ylim(ymin=0)

        elif kind == 'DM':
            x, y = self.get('DM')
            self.__draw__(x, y, label='DM(x)')
            plt.ylim(0)

        elif kind == 'NEQ':
            print("here")
            x, y = self.get('NEQ')
            self.__draw__(x, y, label='NEQ(x)')
            plt.plot(self.sensors[:, 0],
                     sp.zeros_like(self.sensors[:, 0]),
                     'ko',
                     lw=4)
            plt.ylim(ymin=0)

        elif kind == 'lambda':
            x, y = self.get('lambda')
            self.__draw__(x, y, label='$\lambda$(x)')

    def __draw__(self, x, y, label=''):
        """_summary_

        Args:
            x (_type_): _description_
            y (_type_): _description_
        """
        plt.figure()
        plt.plot(x, y, 'k', label=label)
        plt.xlim(x.min(), x.max())
        plt.ylabel(label)
        plt.xlabel('x')