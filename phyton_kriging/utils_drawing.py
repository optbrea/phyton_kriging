#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Common functions to classes """

import matplotlib.pyplot as plt

from . import mm


def draw2D(
        image,
        x,
        y,
        xlabel="$x$",
        ylabel="$y$",
        title="",
        color="seismic",  # YlGnBu  seismic
        interpolation='nearest',  # 'bilinear', 'nearest'
        scale='scaled',
        reduce_matrix='standard',
        range_scale='um',
        verbose=False):
    """makes a drawing of XY

    Parameters:
        image (numpy.array): image to draw
        x (numpy.array): positions x
        y (numpy.array): positions y
        xlabel (str): label for x
        ytlabel (str): label for y
        title (str): title
        color (str): color
        interpolation (str): 'bilinear', 'nearest'
        scale (str): kind of axis (None, 'equal', 'scaled', etc.)
        verbose (bool): if True prints information

    Returns:
        id_fig: handle of figure
        IDax: handle of axis
        IDimage: handle of image
    """

    print(image.shape)
    if reduce_matrix == 'standard' or reduce_matrix in (None, '', []):
        num_x = len(x)
        num_y = len(y)
        reduction_x = int(num_x / 500)
        reduction_y = int(num_y / 500)

        if reduction_x == 0:
            reduction_x = 1
        if reduction_y == 0:
            reduction_y = 1

        image = image[::reduction_x, ::reduction_y]
    else:
        image = image[::reduce_matrix[0], ::reduce_matrix[1]]

    if verbose is True:
        print(("image size {}".format(image.shape)))

    id_fig = plt.figure()
    IDax = id_fig.add_subplot(111)

    extension = (x[0], x[-1], y[0], y[-1])

    IDimage = plt.imshow(image,
                         interpolation=interpolation,
                         aspect='auto',
                         origin='lower',
                         extent=extension)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.suptitle(title)
    plt.axis(extension)
    if scale not in ('', None, []):
        plt.axis(scale)
    IDimage.set_cmap(color)
    plt.tight_layout()
    return id_fig, IDax, IDimage
