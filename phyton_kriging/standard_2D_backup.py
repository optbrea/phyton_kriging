#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import scipy as sp
import matplotlib.pyplot as plt

from .variogram_1D import theoretical
from .variogram_2D import theoretical
import ipywidgets as widgets

um = 1
mm = 1000 * um
degrees = np.pi / 180


class kriging2D(object):
    """Class for unidimensional 2D Kriging.

    Parameters:
        sensors (numpy.array): (x, y, fxy) array with positions and values of sensors
        variogram (numpy.array): Theoretical variogram.
        sampling (dict): 2D (x,y) array sampling positions
    Attributes:
        self.sensors 
        self.variogram
        self.sampling
        self.kriging
    """

    def __init__(self, sensors, variogram, sampling, z_ideal=None):

        self.sensors = sensors
        self.variogram = variogram
        self.sampling = sampling
        self.z_ideal = z_ideal

        self.kriging = None

        self.lambdas = None

    def get(self, kind):
        """Get data from kriging interpolation

        Args:
            kind (string): _description_

        Returns:
            _type_: _description_
        """
        xm = self.sampling
        if kind == 'kriging':
            output = (xm, self.estimation)

        elif kind == 'error':
            output = (xm, self.error)

        elif kind == 'lambda':
            output = (xm - xm.mean().self.lambdas)

        elif kind == 'errorReal':
            output = (xm, abs(self.sampling.y - self.estimation))

        else:
            print("not proper kind parameter")
            output = None

        return output

    def standard(self, filtering=True):
        """Standard kriging

        Args:
            * filtering: Si true se aplica la operacion LM para poder filtrar (tesis y articulo de AO)
                        Si False segun libros standard de kriging

        Outputs:
            * estimation
            * error
            * lambdas
        """

        xsensors = self.sensors[:, 0]
        ysensors = self.sensors[:, 1]
        data = self.sensors[:, 2]
        I = self.sensors[:, 3]

        Xsampling, Ysampling = self.sampling
        Xsampling_size = len(Xsampling)
        Ysampling_size = len(Ysampling)

        xsampling = Xsampling.flatten()
        ysampling = Ysampling.flatten()

        XS1, XS2 = np.meshgrid(xsensors, xsensors)
        XS, XM = np.meshgrid(xsampling, xsensors)

        YS1, YS2 = np.meshgrid(ysensors, ysensors)
        YS, YM = np.meshgrid(ysampling, ysensors)

        # distance entre sensors
        distSensores = np.sqrt((XS2 - XS1)**2 + (YS2 - YS1)**2)
        # distances entre sensors y points de sampling
        distSensoresMuestreo = np.sqrt((XS - XM)**2 + (YS - YM)**2)

        # matrices principales
        Gamma = np.mat(self.variogram.get(distSensores))
        gamma = np.mat(self.variogram.get(distSensoresMuestreo))

        # Modificaciones LM según artículo AO
        if filtering == True:
            s0 = self.variogram.get(0.)
            I1, I2 = np.meshgrid(I, I)

            gamma = gamma - s0 / 2
            Gamma = Gamma - I1 * I2
            for i in range(len(xsensors)):
                Gamma[i, i] = -I[i]**2
        else:
            for i in range(len(xsensors)):
                Gamma[i, i] = 0

        # definición de matrices
        U = np.mat(np.ones((len(xsensors), 1), dtype='float'))
        u = np.mat(np.ones((len(xsampling), 1), dtype='float'))
        Ut = np.transpose(U)
        ut = np.transpose(u)
        invGamma = Gamma.I

        g = (ut - Ut * invGamma * gamma)
        h = (Ut * invGamma * U).I
        lambdas = np.mat(np.transpose(gamma + U * h * g) * invGamma)

        error2 = np.squeeze(
            np.diag(
                np.transpose(gamma) * invGamma * gamma -
                np.transpose(g) * h * g))
        self.error = np.array(np.sqrt(error2))
        self.estimation = np.squeeze(
            np.array(lambdas * np.transpose(np.mat(data))))

        self.error = np.transpose(
            self.error.reshape(Xsampling_size, Ysampling_size))
        self.estimation = np.transpose(
            self.estimation.reshape(Xsampling_size, Ysampling_size))

        lambdas = np.array(lambdas)

        self.lambdas = lambdas.reshape(
            (Xsampling_size, Ysampling_size, len(xsensors)))

    def krigStandard2D_no_uniforme(self, filtering=True):
        # este no sigue al noise, pero va a toda pastilla...
        """Standard kriging. Los datosnotienen por que estar equipuestreados.

		Args:
            * filtering (bool): Si true se aplica la operacion LM para poder filtrar (tesis y articulo de AO)
                        Si False segun libros standard de kriging

		Outputs:
            * lambdas
            * interpolation
            * error
		"""

        # 1. Detecta dimensión de los data
        # 2. Calcula los values del kriging estándar
        # 3. Guarda los data en data1D o data2D

        xsensors = self.sensors[:, 0]
        ysensors = self.sensors[:, 1]
        data = self.sensors[:, 2]
        I = self.sensors[:, 3]

        Xsampling, Ysampling = self.sampling
        xsampling = Xsampling.flatten()
        ysampling = Ysampling.flatten()

        XS1, XS2 = np.meshgrid(xsensors, xsensors)
        XS, XM = np.meshgrid(xsampling, xsensors)

        YS1, YS2 = np.meshgrid(ysensors, ysensors)
        YS, YM = np.meshgrid(ysampling, ysensors)

        # distance entre sensors
        distSensores = np.sqrt((XS2 - XS1)**2 + (YS2 - YS1)**2)
        # distances entre sensors y points de sampling
        distSensoresMuestreo = np.sqrt((XS - XM)**2 + (YS - YM)**2)

        # matrices principales
        Gamma = np.mat(self.variogram.get(distSensores))
        gamma = np.mat(self.variogram.get(distSensoresMuestreo))

        # Modificaciones LM según artículo AO
        if filtering is True:
            s0 = self.variogram.get(0.)
            I1, I2 = np.meshgrid(I, I)

            gamma = gamma - s0 / 2
            Gamma = Gamma - I1 * I2
            for i in range(len(xsensors)):
                Gamma[i, i] = -I[i]**2
        else:
            for i in range(len(xsensors)):
                Gamma[i, i] = 0

        # definición de matrices
        U = np.mat(np.ones((len(xsensors), 1), dtype='float'))
        u = np.mat(np.ones((len(xsampling), 1), dtype='float'))
        Ut = np.transpose(U)
        ut = np.transpose(u)
        invGamma = Gamma.I

        g = (ut - Ut * invGamma * gamma)
        h = (Ut * invGamma * U).I
        lambdas = np.mat(np.transpose(gamma + U * h * g) * invGamma)

        error2 = np.squeeze(
            np.diag(
                np.transpose(gamma) * invGamma * gamma -
                np.transpose(g) * h * g))
        self.error = np.array(np.sqrt(error2))
        self.interpolation = np.squeeze(
            np.array(lambdas * np.transpose(np.mat(data))))

        self.error = np.transpose(self.error)
        self.interpolation = np.transpose(self.interpolation)

        lambdas = np.array(lambdas)
        self.lambdas = lambdas

    def draw(self, kind='kriging', circles=True, clim=None):
        """
                Dibuja las diferentes señales del kriging: señal, variogram, lambdas, error, kriging
                """
        from ipywidgets import interact

        sensors = self.sensors

        xsensors = self.sensors[:, 0]
        ysensors = self.sensors[:, 1]
        data = self.sensors[:, 2]
        I = self.sensors[:, 3]
        num_sensors = len(xsensors)

        Xsampling, Ysampling = self.sampling
        xsampling = Xsampling.flatten()
        ysampling = Ysampling.flatten()

        extension = [
            xsampling.min(),
            xsampling.max(),
            ysampling.min(),
            ysampling.max()
        ]

        if kind == 'data':
            sensors = self.sensors
            plt.scatter(sensors[:, 0],
                        sensors[:, 1],
                        100 * sensors[:, 2] / sensors[:, 2].max(),
                        marker='o')
            plt.colorbar()
        elif kind == 'ideal':
            plt.figure(figsize=(8, 8))
            plt.imshow(self.ideal,
                       interpolation='bilinear',
                       aspect='equal',
                       origin='lower',
                       extent=extension)
            plt.xlim(xmin=xsampling.min(), xmax=xsampling.max())
            plt.ylim(ymin=ysampling.min(), ymax=ysampling.max())
            if clim is None:
                maximum = max(np.ravel(self.ideal))
                minimum = min(np.ravel(self.ideal))
                plt.clim(maximum, minimum)
            else:
                plt.clim(clim[0], clim[1])

            plt.title(r"$Z_{ideal}(x,y)$", fontsize=26)
            plt.colorbar(cax=None, orientation='horizontal')
            if circles is True:
                plt.scatter(xsensors, ysensors, c='w',
                            s=35)  # ,c=colors,s=length

        elif kind == 'variogram':
            self.variogram.draw()

        elif kind == 'lambdas':
            global LAMBDAS, ax1, sS

            LAMBDAS = self.lambdas

            maximum = max(np.ravel(LAMBDAS))
            minimum = min(np.ravel(LAMBDAS))

            fig = plt.figure()
            ax1 = plt.subplot(111)
            plt.subplots_adjust(left=0.15, bottom=0.25)
            plt.title('$\lambda(x)$')

            line = plt.imshow(np.squeeze(LAMBDAS[:, :, 0]),
                              interpolation='bilinear',
                              aspect='equal',
                              origin='lower',
                              extent=extension)
            plt.xlim(xmin=xsampling.min(), xmax=xsampling.max())
            plt.ylim(ymin=ysampling.min(), ymax=ysampling.max())
            plt.set_cmap('seismic')
            plt.colorbar(orientation='vertical')
            plt.clim(vmin=minimum, vmax=maximum)
            plt.scatter(sensors[:, 0],
                        sensors[:, 1],
                        10,
                        color='red',
                        marker='o')

            def update(i=(0, num_sensors - 1, 1)):
                i = int(i)
                line.set_data(LAMBDAS[:, :, i])
                fig.canvas.draw_idle()

            interact(update)

        elif kind == 'error':
            plt.figure(figsize=(8, 8))
            plt.imshow(self.error,
                       interpolation='bilinear',
                       aspect='equal',
                       origin='lower',
                       extent=extension)
            plt.xlim(xmin=xsampling.min(), xmax=xsampling.max())
            plt.ylim(ymin=ysampling.min(), ymax=ysampling.max())

            if clim == None:
                maximum = np.nanmax(np.ravel(self.error))
                minimum = np.nanmin(np.ravel(self.error))
                plt.clim(minimum, maximum)
            else:
                plt.clim(clim[0], clim[1])

            plt.title(r"error kriging(x,y)", fontsize=26)
            plt.colorbar(cax=None, orientation='horizontal')
            if circles is True:
                plt.scatter(xsensors, ysensors, c='w',
                            s=35)  # ,c=colors,s=length

        elif kind == 'kriging':
            plt.figure(figsize=(8, 8))
            plt.imshow(self.estimation,
                       interpolation='bilinear',
                       aspect='equal',
                       origin='lower',
                       extent=extension)
            plt.xlim(xmin=xsampling.min(), xmax=xsampling.max())
            plt.ylim(ymin=ysampling.min(), ymax=ysampling.max())

            if clim is None:
                pass
            else:
                plt.clim(clim[0], clim[1])

            plt.title(r"$Z_{kriging}(x,y)$", fontsize=26)
            plt.colorbar(cax=None, orientation='horizontal')
            if circles is True:
                plt.scatter(xsensors, ysensors, c='w',
                            s=35)  # ,c=colors,s=length
