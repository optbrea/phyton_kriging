#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

import numpy as np
import scipy as sp
import matplotlib.pyplot as plt

from .variogram_1D import theoretical

um = 1
mm = 1000 * um
degrees = np.pi / 180


class kriging1D(object):
    """Class for unidimensional 1D Kriging.

    Parameters:
        sensors (numpy.array): (x,fx) array with positions and values of sensors
        variogram (numpy.array): Theoretical variogram.
        sampling (dict): x array sampling positions
    Attributes:
        self.sensors 
        self.variogram
        self.sampling
        self.kriging
        self.DM
        self.lambdas
        self.y_ideal
    """

    def __init__(self, sensors, variogram, sampling, y_ideal=None):
        """
        sensors (numpy.array): (x,fx, Ix) array with positions and values of sensors
        variogram (numpy.array): Theoretical variogram.
        sampling (dict): x array sampling positions
		Aqui solamente se verifica que las entradas son correctas
		"""
        self.sensors = sensors
        self.variogram = variogram
        self.sampling = sampling
        self.y_ideal = y_ideal

        self.kriging = None

        self.lambdas = None

    def get(self, kind):
        """Get data from kriging interpolation

        Args:
            kind (string): _description_

        Returns:
            _type_: _description_
        """
        xm = self.sampling
        if kind == 'kriging':
            output = (xm, self.estimation)

        elif kind == 'error':
            output = (xm, self.error)

        elif kind == 'lambda':
            output = (xm - xm.mean().self.lambdas)

        elif kind == 'errorReal':
            output = (xm, abs(self.sampling.y - self.estimation))

        else:
            print("not proper kind parameter")
            output = None

        return output

    def standard(self, filtering=True):
        """Standard kriging

		Args:
            * filtering: Si true se aplica la operacion LM para poder filtrar (tesis y articulo de AO)
                         Si False segun libros standard de kriging

		Outputs:
            * lambdas
            * interpolation
            * error
		"""

        xsensors = self.sensors[:, 0]
        data = self.sensors[:, 1]
        I = self.sensors[:, 2]
        xsampling = self.sampling

        XS1, XS2 = np.meshgrid(xsensors, xsensors)
        XS, XM = np.meshgrid(xsampling, xsensors)

        # distance entre sensors
        distSensores = abs(XS2 - XS1)
        # distances entre sensors y points de sampling
        distSensoresMuestreo = abs(XS - XM)

        # matrices principales
        Gamma = np.mat(self.variogram.get(distSensores))
        gamma = np.mat(self.variogram.get(distSensoresMuestreo))

        # Modificaciones LM según artículo AO
        if filtering is True:
            s0 = self.variogram.get(0.)
            I1, I2 = np.meshgrid(I, I)

            gamma = gamma - s0 / 2
            Gamma = Gamma - I1 * I2
            num_sensors = len(self.sensors)
            for i in range(num_sensors):
                Gamma[i, i] = -I[i]**2

        else:
            for i in range(len(xsensors)):
                Gamma[i, i] = 0

        # definición de matrices
        U = np.mat(np.ones((len(xsensors), 1), dtype='float'))
        u = np.mat(np.ones((len(xsampling), 1), dtype='float'))
        Ut = np.transpose(U)
        ut = np.transpose(u)
        invGamma = Gamma.I

        g = (ut - Ut * invGamma * gamma)
        h = (Ut * invGamma * U).I
        self.lambdas = np.array(np.transpose(gamma + U * h * g) * invGamma)
        error2 = np.squeeze(
            np.diag(
                np.transpose(gamma) * invGamma * gamma -
                np.transpose(g) * h * g))
        self.error = np.array(np.sqrt(error2))
        self.estimation = np.squeeze(
            np.array(self.lambdas * np.transpose(np.mat(data))))

    def draw(self, kind='kriging', xlabel='x', ylabel='I(x)', num_lambda=all):
        """		Draws kriging: data, variogram, lambdas, error, kriging

        Args:
            kind (str, optional): _description_. Defaults to 'kriging'.
            xlabel (str, optional): _description_. Defaults to 'x'.
            ylabel (str, optional): _description_. Defaults to 'I'.
            num_lambda (_type_, optional): _description_. Defaults to all.
        """
        sampling = self.sampling
        sensors = self.sensors
        estimation = self.estimation
        krig1 = self

        if kind == 'data':
            plt.plot(sensors[:, 0], sensors[:, 1], 'k', label='data')

        if kind == 'variogram':
            self.variogram.draw()
            plt.ylim(ymin=0)

        if kind == 'kriging':
            plt.plot(sensors[:, 0], sensors[:, 1], 'k', label='data')
            plt.plot(sampling, estimation, 'r', label='estimation')
            if self.y_ideal is not None:
                plt.plot(sampling, self.y_ideal, 'b--', label='ideal')
            plt.fill_between(sampling,
                             self.estimation - 2 * self.error,
                             self.estimation + 2 * self.error,
                             color='blue',
                             alpha=0.125,
                             label='$2\sigma$')
            plt.fill_between(sampling,
                             self.estimation - self.error,
                             self.estimation + self.error,
                             color='blue',
                             alpha=0.25,
                             label='$\sigma$')
            plt.xlim(sampling[0], sampling[-1])
            plt.grid('on')
            plt.xlabel(xlabel)
            plt.ylabel(ylabel)
            plt.legend()

        if kind == 'error':
            output = self.get('error')

            sampling = self.sampling
            krig1 = self
            sensors = self.sensors

            if self.y_ideal is not None:
                diferences = self.estimation - self.y_ideal
                plt.plot(sampling, diferences, 'r', label='error')

            plt.plot(sensors[:, 0],
                     np.zeros_like(sensors[:, 0]),
                     'ko',
                     label='sensors')

            plt.fill_between(sampling,
                             -2 * krig1.error,
                             2 * krig1.error,
                             color='blue',
                             alpha=0.125,
                             label='$2\sigma$')
            plt.fill_between(sampling,
                             -krig1.error,
                             krig1.error,
                             color='blue',
                             alpha=0.25,
                             label='$\sigma$')
            plt.xlim(sampling[0], sampling[-1])
            plt.grid('on')
            plt.legend()
            plt.xlabel(xlabel)
            plt.ylabel(ylabel)

        if kind == 'lambdas':
            plt.plot(sampling, self.lambdas)
