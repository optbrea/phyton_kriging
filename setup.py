#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""The setup script."""

from setuptools import find_packages, setup

with open('README.rst', encoding='utf8') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst', encoding='utf8') as history_file:
    history = history_file.read()

requirements = ['numpy']

setup_requirements = [
    'pytest-runner',
]

test_requirements = [
    'pytest',
]

setup(
    name='phyton_kriging',
    author='Luis Miguel Sanchez Brea',
    author_email='optbrea@ucm.es',
    version='0.0.1',
    description='kriging at AOCG/UCM',
    long_description=readme,
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.10',
        'Topic :: data :: scientific',
    ],
    keywords='kriging, spatial statistics',
    url='http://github.com/luismiguel/data',
    license="MIT license",
    packages=[
        'phyton_kriging',
    ],
    include_package_data=True,
    zip_safe=False,
    install_requires=requirements,
)
"""test_suite='nose.collector',
tests_require=['nose', 'nose-cover3'],
entry_points={
    'console_scripts': ['data=data.command_line:main'],
},"""
