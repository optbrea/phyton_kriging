Welcome to phython_kriging: Python phython_krigingn and interference's  documentation!
===================================================================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:
   :numbered:
   :glob:

   readme
   installation
   usage
   modules
   contributing
   authors
   history

Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
