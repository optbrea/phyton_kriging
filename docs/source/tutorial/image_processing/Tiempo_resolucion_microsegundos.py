# -*- coding: utf-8 -*-
"""
Created on Tue Jan 24 13:10:38 2023

@author: luisb
"""

import time 
import os 
from datetime import datetime
import glob
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


fileDir=r"C:\Escribano\trackeo\prueba_fps"

fileExt= r".bmp"

a =[_ for _ in os.listdir(fileDir) if _.endswith(fileExt)]
x=[]
export=[]
for i in range(len(a)):
    x.append(i)
    file=fileDir+'\\'+a[i]
    date= os.path.getmtime(file)
    fecha= datetime.fromtimestamp(date)
    
    export.append([a[i],fecha.year,fecha.month,fecha.day,fecha.hour,fecha.minute,fecha.second,fecha.microsecond])
    
df=pd.DataFrame(export)
df.columns=['archivo','año','mes','dia','hora','minuto','segundo','microsegundo']

#df.to_csv(fileDir+'\\'+'result2.txt')

#Esto es para analizar los fps. Para ello variamos el programa que tenemos

segundos=df.segundo-df.segundo[0]
micro=df.microsegundo
tiempo=micro+segundos*10e5
sortedseries = tiempo.sort_values()

plt.plot(x,sortedseries)
plt.show()
