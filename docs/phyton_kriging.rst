phyton\_kriging package
=======================

Submodules
----------

phyton\_kriging.calculo\_periodo module
---------------------------------------

.. automodule:: phyton_kriging.calculo_periodo
   :members:
   :undoc-members:
   :show-inheritance:

phyton\_kriging.convolucion1D module
------------------------------------

.. automodule:: phyton_kriging.convolucion1D
   :members:
   :undoc-members:
   :show-inheritance:

phyton\_kriging.convolucion2D module
------------------------------------

.. automodule:: phyton_kriging.convolucion2D
   :members:
   :undoc-members:
   :show-inheritance:

phyton\_kriging.general module
------------------------------

.. automodule:: phyton_kriging.general
   :members:
   :undoc-members:
   :show-inheritance:

phyton\_kriging.krigConv\_comparacion module
--------------------------------------------

.. automodule:: phyton_kriging.krigConv_comparacion
   :members:
   :undoc-members:
   :show-inheritance:

phyton\_kriging.optimizacion module
-----------------------------------

.. automodule:: phyton_kriging.optimizacion
   :members:
   :undoc-members:
   :show-inheritance:

phyton\_kriging.optimizacion\_clase module
------------------------------------------

.. automodule:: phyton_kriging.optimizacion_clase
   :members:
   :undoc-members:
   :show-inheritance:

phyton\_kriging.standard1D module
---------------------------------

.. automodule:: phyton_kriging.standard1D
   :members:
   :undoc-members:
   :show-inheritance:

phyton\_kriging.standard2D module
---------------------------------

.. automodule:: phyton_kriging.standard2D
   :members:
   :undoc-members:
   :show-inheritance:

phyton\_kriging.variogram1D module
----------------------------------

.. automodule:: phyton_kriging.variogram1D
   :members:
   :undoc-members:
   :show-inheritance:

phyton\_kriging.variogram2D module
----------------------------------

.. automodule:: phyton_kriging.variogram2D
   :members:
   :undoc-members:
   :show-inheritance:

phyton\_kriging.variogram2D\_radial module
------------------------------------------

.. automodule:: phyton_kriging.variogram2D_radial
   :members:
   :undoc-members:
   :show-inheritance:

phyton\_kriging.variograma2D module
-----------------------------------

.. automodule:: phyton_kriging.variograma2D
   :members:
   :undoc-members:
   :show-inheritance:

phyton\_kriging.variograma2D\_radial module
-------------------------------------------

.. automodule:: phyton_kriging.variograma2D_radial
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: phyton_kriging
   :members:
   :undoc-members:
   :show-inheritance:
