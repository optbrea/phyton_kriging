.. highlight:: shell

============
Installation
============


Stable release
--------------

To install Phyton_kriging, run this command in your terminal:

.. code-block:: console

	# Linux:
	$ pip3 install phyton_kriging

	# Windows:
	$ pip install phyton_kriging


This is the preferred method to install phyton_kriging, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


Additional packages
------------------------

Numpy, Scipy, Matplootlib

They should previously be installed before phyton_kriging module.


From sources
------------

The sources for Python phyton_krigingn and interference can be downloaded from the `Bitbucket repo`_.

You can either clone the public repository:

.. code-block:: console

	$ git clone git@bitbucket.org:optbrea/phyton_kriging.git
	$ git clone https://optbrea@bitbucket.org/optbrea/phyton_kriging.git



Once you have a copy of the source, you can install it with:

.. code-block:: console

	# Linux:
	$ python3 setup.py install

	# Windows:
	$ python setup.py install



.. _Bitbucket repo: https://bitbucket.org/optbrea/phyton_kriging/src/master/
