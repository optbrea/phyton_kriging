# Todo

## Bugs

- 23/04/01 En el variograma1D hay un problema de raiz consistente en que hay que llamar de una forma más efectiva a las funciones con el tipo
- 23/04/01 También hay un problema en la llamada a las funcion potentia, ya que tiene 4 parámetros. Funciona en teoría, pero no en el fitting

## In process

- f

## To Do

- 23/07/12 kriging 2D con variograma 2D.
- 23/07/12 kriging con posiciones de sampling irregulares.

## Documentation

- A
