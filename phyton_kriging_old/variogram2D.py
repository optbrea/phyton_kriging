#!/usr/bin/env python
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------
# Name:		 variogram.py
# Purpose:	 clase variogram 2D
#
# Author:	  Luis Miguel Sanchez Brea
#
# Created:	 2015/09/15
# RCS-ID:
# Copyright:
# Licence:	 GPL
#----------------------------------------------------------------------
"""
clase variogram 2D
"""

import scipy as sp
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons

from general.funcs_varias import cercano
from data.data1D import data1D
from data.data2D import data2D

um = 1
mm = 1000 * um
pi = sp.pi
degrees = pi / 180


class variogram2D(object):

    def __init__(self,
                 data=data2D(),
                 f_texto="s0**2+c0**2*(1-sp.exp(-(x/l0)**2))",
                 xtext=r"$ h(mm) $",
                 ytext=r"$ \gamma(h) (mm) $",
                 title=r"$variogram$"):
        """Se inicia un new e1emento"""
        self.data = data  #data experimentales que dan el variogram

        self.kind = None  #elijo variogram teorico o experiemntal

        self.f_texto = f_texto  #texto que representa la ecuación teórica

        self.varExp = data1D(title=r"experimental")  #variogram experimental
        self.varTeo = data1D(title=r"teorico")  #variogram teorico

        self.isTeo = False
        self.isExp = False

        self.pT = [-1, -1, -1]  #parámetros para el variogram teórico

        self.xtext = xtext
        self.ytext = ytext
        self.title = title

    def compute_experimental(self,
                             ipoints=sp.arange(1, 100),
                             recortarmaximum=False,
                             xcorte=None):
        data = self.data.y
        variogram = sp.zeros(len(ipoints), dtype='float')
        numPuntos = sp.zeros(len(ipoints), dtype='int')

        num_data = data.size
        npoints = ipoints.size

        for i in sp.arange(0, npoints):
            hi = ipoints[i]
            VAR = 0
            num_data = 0
            for j in sp.arange(1, hi + 1):
                posiciones = sp.arange(j, num_data, hi, dtype='int')
                values = data[posiciones]
                diferencias = sp.diff(values)**2
                VAR = VAR + diferencias.sum()
                num_data = num_data + diferencias.size

            variogram[i] = VAR / (2 * num_data)
            numPuntos[i] = num_data

        incremento = self.data.x[1] - self.data.x[0]

        if recortarmaximum == True:
            if not xcorte == None:
                (posmaximum, value,
                 distance) = cercano(ipoints * incremento, xcorte)
                print(posmaximum, value, distance)
                valuemaximum = variogram[posmaximum]
                variogram[posmaximum:-1] = 0.99 * valuemaximum

            else:
                "todo lo que esté después del máximo lo ponemos al value del máximo"
                posmaximum = variogram.argmax()
                #print variogram
                print "posmaximum", posmaximum
                valuemaximum = variogram.max()
                variogram[
                    posmaximum:
                    -1] = 0.99 * valuemaximum  #para que luego busque bien el value

        #variogram[-1] = variogram[-2]
        #numPuntos[-1] = numPuntos[-2]
        #variogram[0] = variogram[1]
        #numPuntos[0] = variogram[1]

        self.varExp.x = ipoints * incremento
        self.varExp.y = variogram
        self.varExp.num_data = numPuntos
        self.isExp = True

    def establecer_parametros(self, x=None, s0=0, c0=1, l0=1, f_texto=None):

        if not f_texto == None:
            self.f_texto = f_texto

        self.pT[0] = s0
        self.pT[1] = c0
        self.pT[2] = l0

        if not x == None:
            self.varTeo.x = x
        else:
            self.varTeo.x = self.varExp.x
            x = self.varExp.x

        self.varTeo.y = eval(self.f_texto)
        self.isTeo = True

    def compute_parametros(self, x=None):
        s0 = (self.varExp.y[1:4]).mean()**0.5
        c0 = (max(self.varExp.y) - self.varExp.y[1])**0.5

        #podria hacer una convolución
        posmaximum = self.varExp.y.argmax()
        l0 = self.varExp.x[posmaximum] / 2

        self.isTeo = True

        self.pT[0] = s0
        self.pT[1] = c0
        self.pT[2] = l0

        if not x == None:
            self.varTeo.x = x
        else:
            self.varTeo.x = self.varExp.x
            x = self.varExp.x

        self.varTeo.y = eval(self.f_texto)
        self.isTeo = True

    def ajuste_manual(self, title='ajuste manual'):

        global l2b, sS, sL, sH, h
        global paramV

        paramV = sp.zeros(3, dtype=float)

        id_fig = plt.figure()
        ax1 = plt.subplot(121)
        l1, = plt.plot(self.data.x, self.data.y, lw=2, color='k')

        ax2 = plt.subplot(122)
        plt.subplots_adjust(left=0.15, bottom=0.25)
        l2a, = plt.plot(self.varExp.x, self.varExp.y, lw=2, color='k')
        plt.title(title)

        sc0 = sp.sqrt(self.varExp.y[0])
        hc0 = self.varExp.y.max() - sc0**2
        lc0 = self.varExp.x[-1] / 2

        h = self.varExp.x

        varTeoricoAjuste = sc0**2 + hc0**2 * (1 - sp.exp(-(h / lc0)**2))
        l2b, = plt.plot(h, varTeoricoAjuste, lw=2, color='red')
        plt.axis([0, self.varExp.x[-1], 0, 1.1 * self.varExp.y.max()])

        axcolor = 'lightgoldenrodyellow'
        axS = plt.axes([0.15, 0.15, 0.75, 0.03], axisbg=axcolor)
        axH = plt.axes([0.15, 0.10, 0.75, 0.03], axisbg=axcolor)
        axL = plt.axes([0.15, 0.05, 0.75, 0.03], axisbg=axcolor)

        sS = plt.Slider(axS,
                        'sc',
                        0,
                        sp.sqrt(self.varExp.y.max()),
                        valinit=sc0)
        sH = plt.Slider(axH, 'hc', 0, self.varExp.y.max(), valinit=hc0)
        sL = plt.Slider(axL,
                        'lc',
                        self.varExp.x[-1] / 1000,
                        self.varExp.x[-1],
                        valinit=lc0)

        sS.on_changed(update)
        sL.on_changed(update)
        sH.on_changed(update)

        #	resetax = plt.axes([0.01, 0.05, 0.1, 0.04])
        #	buttonReset = plt.Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')
        #	buttonReset.on_clicked(reset)

        aceptarax = plt.axes([0.01, 0.1, 0.1, 0.04])
        buttonAceptar = plt.Button(aceptarax,
                                   'aceptar',
                                   color=axcolor,
                                   hovercolor='0.975')
        buttonAceptar.on_clicked(aceptar)

        #		self.set('s0',paramV[0] )
        #		self.set('c0',paramV[1] )
        #		self.set('l0',paramV[2] )
        #
        #		self.pT[0]=paramV[0]
        #		self.pT[1]=paramV[1]
        #		self.pT[2]=paramV[2]
        #
        #		s0=paramV[0]
        #		c0=paramV[1]
        #		l0=paramV[2]
        #
        #		self.varTeo.x=self.varExp.x
        #		x=self.varExp.x
        #
        #		self.varTeo.y=eval(self.f_texto)
        #		self.isTeo=True

        #		#self.establecer_parametros(x=self.varExp.x, s0=paramV[0], c0=paramV[1], l0=paramV[2], f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")
        #
        #		plt.show()
        #		return paramV

        #elf.establecer_parametros(x=self.varExp.x, s0=paramV[0], c0=paramV[1], l0=paramV[2], f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")

        plt.show()
        return paramV

    def f(self, x=None, kind='teorico', almacen=True):
        """Si x==None entonces cogemos los data de var.varTeo.x o var.varExp.x
		   kind='teorico' o 'experimental'
		   si almacen==True entonces guardamos los data en varTeo.x, y o varExp.x,y
		"""

        if kind == 'teorico':
            s0 = self.pT[0]
            c0 = self.pT[1]
            l0 = self.pT[2]

            if x == None:
                x = self.varTeo.x

            y = eval(self.f_texto)
            if almacen == True:
                self.varTeo.x = x
                self.varTeo.y = y

        if kind == 'experimental':
            if x == None:
                x = self.varExp.x

            #interpolar data
            y = sp.interp(x, self.varExp.x, self.varExp.y)
            if almacen == True:
                self.varExp.x = x
                self.varExp.y = y

        return y

    def set(self, variable, value):
        if variable == 's0':
            self.pT[0] = value
        if variable == 'c0':
            self.pT[1] = value
        if variable == 'l0':
            self.pT[2] = value
        if variable == 'f_texto':
            self.f_texto = value
        if variable == 'kind':
            if value == 'teorico':
                self.kind = 'teorico'
                self.x = self.varTeo.x
                self.y = self.varTeo.y
            if value == 'experimental':
                self.kind = 'experimental'
                self.x = self.varExp.x
                self.y = self.varExp.y

    def draw(self, kind='teorico'):
        plt.figure(figsize=(8, 8), facecolor='w', edgecolor='k')

        if kind == 'teorico':
            self.__drawVariogramaTeorico()
        if kind == 'experimental':
            self.__drawVariogramaExperimental()
        if kind == 'ambos':
            self.__drawVariogramaCompleto()
        if kind == 'todo':
            self.__drawVariogramaTodo()

    def __drawVariogramaTeorico(self):
        if self.isTeo == True:
            plt.plot(self.varTeo.x,
                     sp.sqrt(self.varTeo.y),
                     'k',
                     label=r"$ \sqrt{\gamma_{Teo}(h)}  $",
                     linewidth=2)
            plt.xlabel(r"$ h $", fontsize=22)
            plt.ylabel(r"$ \sqrt{\gamma_{Teo}(h)}  $", fontsize=22)
            plt.title('variogram teorico')
            plt.legend(shadow=False)

    def __drawVariogramaExperimental(self):
        if self.isExp == True:
            plt.plot(self.varExp.x,
                     sp.sqrt(self.varExp.y),
                     'k',
                     label=r"$ \sqrt{\gamma_{Exp}(h)}  $",
                     linewidth=2)
            plt.xlabel(r"$ h $", fontsize=22)
            plt.ylabel(r"$ \sqrt{\gamma_{Exp}(h)}  $", fontsize=22)
            plt.title(self.title, fontsize=26)
            plt.title('variogram experimental')
            plt.legend(shadow=False)

    def __drawVariogramaCompleto(self):
        "dibuja la function"
        if self.isTeo == True:
            plt.plot(self.varTeo.x,
                     sp.sqrt(self.varTeo.y),
                     'k',
                     label=r"$ \sqrt{\gamma_{Teo}(h)}  $",
                     linewidth=2)
        if self.isExp == True:
            plt.plot(self.varExp.x,
                     sp.sqrt(self.varExp.y),
                     'k--',
                     label=r"$ \sqrt{\gamma_{Exp}(h)}  $",
                     linewidth=2)

        plt.xlabel(r"$ h $", fontsize=20)
        plt.ylabel(r"$ \sqrt{\gamma(h)}  $", fontsize=20)
        plt.title(self.title, fontsize=26)

        plt.legend(shadow=False)

    def __drawVariogramaTodo(self):
        "dibuja la function"
        #function
        plt.subplot(211)
        plt.plot(self.data.x, self.data.y, 'k', label='function', linewidth=2)
        #sp.plot(x,y_data,'ko')
        plt.xlabel(r"$ x $", fontsize=20)
        plt.ylabel(r"$ f(x) $", fontsize=20)
        plt.title(self.title, fontsize=26)

        #variogram
        plt.subplot(212)
        if self.isTeo == True:
            plt.plot(self.varTeo.x,
                     sp.sqrt(self.varTeo.y),
                     'k',
                     label=r"$ \sqrt{\gamma_{Teo}(h)}  $",
                     linewidth=2)

        if self.isExp == True:
            plt.plot(self.varExp.x,
                     sp.sqrt(self.varExp.y),
                     'k--',
                     label=r"$ \sqrt{\gamma_{Exp}(h)}  $",
                     linewidth=2)

        plt.xlabel(r"$ h $", fontsize=20)
        plt.ylabel(r"$ \sqrt{\gamma(h)}  $", fontsize=20)
        plt.legend(shadow=False)


if __name__ == '__main__':
    from variogram_tests import tests_variogram

    checkTiempo = 0  #falta modulo pstats
    if checkTiempo == 1:
        from cProfile import run
        run('tests_variogram()', sort=True)
    else:
        tests_variogram()

    plt.show()
