#!/usr/local/bin/python
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------
# Name:		convolution1D.py
# Purpose:	 Kriging por convolucion 1D
#
# Author:	  Luis Miguel Sanchez Brea
#
# Created:	 10/02/2023
#----------------------------------------------------------------------
"""Kriging por convolucion
"""
"""TODO: 
1. meter errorReal como diferencia entre estimacion y señal con data según sampling
2. hacer function para hacer simplificado (utilizable en la optimización)
3. separar la estimacion y solo hacerla cuando data no sea nulo
4. hacer lo mismo para el kriging estándard
"""

import scipy as sp
import numpy as np
import matplotlib.pyplot as plt

from scipy.signal import convolve

from phyton_kriging.variogram import variogram

um = 1
mm = 1000 * um
degrees = sp.pi / 180


class krigingConvolucion1D(object):
    """
	Clase en la que se hace la convolución 1D del kriging
	"""

    def __init__(self, sensors, variogram, sampling):
        """Se inicia un new elemento"""

        self.sensors = sensors
        self.variogram = variogram
        self.sampling = sampling

        #DM(x,y)
        x = self.sampling.x - self.sampling.x.mean()
        var = variogram.f(x, kind='teorico')
        self.sc = variogram.pT[0]
        self.DM = self.sc**2 / (2 * var - self.sc**2)
        self.DM_normalize()

        #		elif self.kind == 'gauss':
        #			#de momento no vale porque la longitud de correlacion depende de muchos parámetros
        #			x = self.sampling.x - self.sampling.x.mean()
        #			lc = 0.333 * self.variogram.pT[2]
        #			self.DM = sp.exp(-0.5 * (x / (0.9 * lc)) ** 2)

        self.computeError(sensors)
        self.estimar(sensors)
        self.x = sampling.x

    def computeError(self, sensors):
        xComb, ZComb = sensors.sampling2(xMuestreo=self.sampling.x)

        #NEQ(x,y)
        if self.kind == 'antiguo':
            self.NEQ = convolve(xComb, self.DM, 'same')
        elif self.kind == 'moderno':
            self.NEQ = convolve(
                xComb, self.DM,
                'same') + (self.sc)**2 / self.variogram.f(10000)

        #error(x,y)
        #de momento no se como colocar cuando hay distintas precisiones
        self.error = sp.sqrt(self.sensors.I[0]**2 + self.sc**2 / self.NEQ)

    def estimar(self, sensors):
        numsensors = len(self.sensors.x)
        period_aproximado = (self.sampling.x.max() -
                             self.sampling.x.min()) / numsensors

        self.Lambda = sp.sinc(.25 *
                              (self.sampling.x - self.sampling.x.mean()) /
                              period_aproximado) * self.DM**.5

        #estimacion(x,y)
        self.sensors = sensors

        xComb, zComb = sensors.sampling2(xMuestreo=self.sampling.x)

        estimacion = convolve(zComb, self.Lambda, 'same')
        normalizeLambda = convolve(xComb, self.Lambda, 'same')
        self.estimacion = estimacion / normalizeLambda

        #print normalizeLambda

    def DM_normalize(self):
        DM = self.DM
        self.DM = (DM - DM.min()) / (1 - DM.min())

    def get(self, kind):
        xm = self.sampling.x

        title = '$convolucion$'

        if kind == 'DM':
            output = (xm - xm.mean(), self.DM)

        if kind == 'NEQ':
            output = data1D(x=xm,
                            y=self.NEQ,
                            xtext="$x(mm)$",
                            ytext="$NEQ(x)$",
                            title=title)

        if kind == 'error':
            output = data1D(x=xm,
                            y=self.error,
                            xtext="$x(mm)$",
                            ytext="$error(x)$",
                            title=title)

        if kind == 'lambda':
            output = data1D(x=xm - xm.mean(),
                            y=self.Lambda,
                            xtext="$x(mm)$",
                            ytext="$\Lambda(x)$",
                            title=title)

        if kind == 'estimacion':
            output = data1D(x=xm,
                            y=self.estimacion,
                            xtext="$x(mm)$",
                            ytext="$estimacion(x)$",
                            title=title)

        return output

    def draw(self, kind='DM'):

        if kind == 'comparar':
            linewidth = 2
            plt.figure()
            plt.plot(self.sensors.x,
                     self.sensors.y,
                     'ro',
                     label="señal",
                     linewidth=linewidth)
            plt.plot(self.sensors.x,
                     self.sensors.yideal,
                     'b',
                     label=r"ideal",
                     linewidth=linewidth)
            plt.plot(self.sampling.x,
                     self.estimacion,
                     'k',
                     label=r"conv",
                     linewidth=linewidth)
            plt.plot(self.sampling.x,
                     self.estimacion + self.error,
                     'k--',
                     label=r"error",
                     linewidth=linewidth)
            plt.plot(self.sampling.x,
                     self.estimacion - self.error,
                     'k--',
                     linewidth=linewidth)
            plt.title(r'kriging por convolucion', fontsize=30)
            plt.xlabel("$x (mm)$", fontsize=22)
            plt.ylabel("$I$", fontsize=22)
            plt.legend()

            plt.figure()
            plt.plot(self.sensors.x,
                     abs(self.sensors.noise),
                     'ro',
                     label=r"noise",
                     linewidth=linewidth)
            plt.plot(self.sampling.x,
                     self.error,
                     'k',
                     label=r"error convolucion",
                     linewidth=linewidth)
            errorCometido = abs(self.sampling.y - self.estimacion)
            plt.plot(self.sampling.x,
                     errorCometido,
                     'b',
                     label=r"error real",
                     linewidth=linewidth)

            plt.title(r'kriging por convolucion', fontsize=30)
            plt.xlabel("$x (mm)$", fontsize=22)
            plt.ylabel("$error vs. noise$", fontsize=22)
            plt.legend()

        if kind == 'data':
            self.sensors.draw()

        if kind == 'var':
            self.variogram.draw()
            print self.variogram

        if kind == 'DM':
            output = self.get('DM')
            output.draw()

        if kind == 'NEQ':
            output = self.get('NEQ')
            output.draw()
            plt.plot(self.sensors.x, sp.zeros_like(self.sensors.x), 'ko', lw=8)
            plt.ylim(ymin=0)

        if kind == 'error':
            output = self.get('error')
            output.draw(label='error estimado')
            plt.plot(self.sensors.x,
                     sp.zeros_like(self.sensors.x) + self.sensors.I[0],
                     'ko',
                     lw=8,
                     label='sensors')
            plt.ylim(ymin=0)
            plt.xlim(xmin=self.sampling.x[0], xmax=self.sampling.x[-1])
            plt.legend()

        if kind == 'lambda':
            output = self.get('lambda')
            output.draw()

        if kind == 'estimacion':
            output = self.get('estimacion')
            output.draw()

        if kind == 'all':
            self.draw('comparar')
            self.draw('data')
            self.draw('var')
            self.draw('DM')
            self.draw('NEQ')
            self.draw('error')
            self.draw('lambda')
            self.draw('estimacion')


if __name__ == '__main__':

    from krigConv_tests import test_krigConv

    checkTiempo = 0
    if checkTiempo == 1:
        from cProfile import run
        run('test_krigConv()', sort=True)
    else:
        test_krigConv()
    plt.show()
