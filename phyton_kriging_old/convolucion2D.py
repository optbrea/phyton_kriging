#!/usr/local/bin/python
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------
# Name:		convolution2D.py
# Purpose:	 Kriging por convolucion 2D
#
# Author:	  Luis Miguel Sanchez Brea
#
# Created:	 05/04/2023

#----------------------------------------------------------------------
"""Kriging por convolucion
"""

import scipy as sp
import numpy as np
from scipy.signal import convolve
import matplotlib.pyplot as plt

from phyton_kriging.variogram import variogram

um = 1
mm = 1000 * um
degrees = sp.pi / 180


class krigingConvolucion2D(object):
    """
	Clase en la que se hace la convolución 1D del kriging
	"""

    def __init__(self, sensors, variogram, sampling, kind='moderno'):
        """Se inicia un new elemento"""

        self.sensors = sensors
        self.variogram = variogram
        self.sampling = sampling
        self.kind = kind  #antiguo significa la convolución primera, #moderno lo que estoy haciendo ahora

        #DM(x,y)
        x = self.sampling.X - (self.sampling.x.max() +
                               self.sampling.x.min()) / 2
        y = self.sampling.Y - (self.sampling.y.max() +
                               self.sampling.y.min()) / 2

        var = variogram.f(sp.sqrt(x**2 + y**2), kind='teorico')
        self.sc = variogram.pT[0]
        self.DM = self.sc**2 / (2 * var - self.sc**2)

        self.DM_normalize()
        #		elif self.kind == 'gauss':
        #			#de momento no vale porque la longitud de correlacion depende de muchos parámetros
        #			x = self.sampling.x - self.sampling.x.mean()
        #			lc = 0.333 * self.variogram.pT[2]
        #			self.DM = sp.exp(-0.5 * (x / (0.9 * lc)) ** 2)

        #self.computeError(sensors)
        #self.estimar(sensors)
        self.x = sampling.x

    def computeError(self, sensors):
        self.sensors = sensors
        XYComb, ZComb = sensors.sampling2(xMuestreo=self.sampling.x,
                                          yMuestreo=self.sampling.y)

        #NEQ(x,y)
        if self.kind == 'antiguo':
            self.NEQ = convolve(XYComb, self.DM, 'same')
        elif self.kind == 'moderno':
            self.NEQ = convolve(
                XYComb, self.DM,
                'same') + (self.sc)**2 / self.variogram.f(10000)

        #error(x,y)
        #de momento no se como colocar cuando hay distintas precisiones
        self.NEQ = sp.transpose(self.NEQ)
        self.error = sp.sqrt(self.sensors.I[0]**2 + self.sc**2 / self.NEQ)
        """
		XYComb, ZComb = sensors.sampling2(xMuestreo=self.sampling.x, yMuestreo=self.sampling.y)

		#NEQ(x,y)
		NEQ = convolve(XYComb, self.DM, 'same') + 1 * (self.sc / self.variogram.f(10000)) ** 2
		"""

    def estimar(self, sensors):
        #lambda(x,y)
        #esto es experimental
        self.Lambda = sp.sqrt(self.DM)

        #estimacion(x,y)
        self.sensors = sensors

        XYComb, ZComb = sensors.sampling2(xMuestreo=self.sampling.x,
                                          yMuestreo=self.sampling.y)

        estimacion = convolve(ZComb, self.Lambda, 'same')
        normalizeLambda = convolve(XYComb, self.Lambda, 'same')
        self.estimacion = estimacion / normalizeLambda

        return self.estimacion

    def DM_normalize(self):

        DM = self.DM
        self.DM = (DM - DM.min()) / (1 - DM.min())
        """ remove cuando se verifique lo que está
		DM = self.DM
		DM = DM - kmin * DM.min()
		DM = DM / DM.max()
		self.DM = DM
		"""

    def get(self, kind):
        xm = self.sampling.x
        ym = self.sampling.y

        xtext = r"$x (mm)$"
        ytext = r"$y (mm)$"
        title = '$convolucion$'

        if kind == 'senalReal':
            output = dataMatrix(x=xm - xm.mean(),
                                y=ym - ym.mean(),
                                matrix=self.sampling.Z,
                                xtext=xtext,
                                ytext=ytext,
                                title=r"$senal real(x,y)$")

        if kind == 'DM':
            output = dataMatrix(x=xm - xm.mean(),
                                y=ym - ym.mean(),
                                matrix=self.DM,
                                xtext=xtext,
                                ytext=ytext,
                                title=r"$DM(x,y)$")

        if kind == 'NEQ':
            output = dataMatrix(x=xm,
                                y=ym,
                                matrix=self.NEQ,
                                xtext=xtext,
                                ytext=ytext,
                                title=r"$NEQ(x,y)$")

        if kind == 'error':
            output = dataMatrix(x=xm,
                                y=ym,
                                matrix=self.error,
                                xtext=xtext,
                                ytext=ytext,
                                title=r"$error(x,y)$")

        if kind == 'errorReal':
            output = dataMatrix(x=xm,
                                y=ym,
                                matrix=abs(self.estimacion - self.sampling.Z),
                                xtext=xtext,
                                ytext=ytext,
                                title=r"error_Real(x,y)$")

        if kind == 'lambda':
            output = dataMatrix(x=xm,
                                y=ym,
                                matrix=self.Lambda,
                                xtext=xtext,
                                ytext=ytext,
                                title=r"$\Lambda(x,y)$")

        if kind == 'estimacion':
            output = dataMatrix(x=xm,
                                y=ym,
                                matrix=self.estimacion,
                                xtext=xtext,
                                ytext=ytext,
                                title=r"$estimacion(x,y)$")

        return output

    def draw(self, kind='DM', circles=True, clim=None):

        if kind == 'senalReal':
            output = self.get('senalReal')
            output.draw(kind='señal')
            plt.gcf()
            plt.gca()
            if circles == True:
                plt.scatter(self.sensors.x, self.sensors.y, c='w',
                            s=35)  #,c=colors,s=length

        if kind == 'data':
            self.sensors.draw()

        if kind == 'var':
            self.variogram.draw()

        if kind == 'DM':
            output = self.get('DM')
            output.draw(kind='señal')

        if kind == 'NEQ':
            output = self.get('NEQ')
            output.draw(kind='señal')
            plt.gcf()
            plt.gca()
            if circles == True:
                plt.scatter(self.sensors.x, self.sensors.y, c='w',
                            s=35)  #,c=colors,s=length

        if kind == 'error':
            plt.figure()
            plt.imshow(self.error,
                       interpolation='bilinear',
                       aspect='equal',
                       origin='lower',
                       extent=[
                           self.sampling.x.min(),
                           self.sampling.x.max(),
                           self.sampling.y.min(),
                           self.sampling.y.max()
                       ])
            plt.xlim(xmin=self.sampling.x.min(), xmax=self.sampling.x.max())
            plt.ylim(ymin=self.sampling.y.min(), ymax=self.sampling.y.max())

            plt.xlabel(self.sampling.xtext, fontsize=20)
            plt.ylabel(self.sampling.ytext, fontsize=20)

            plt.title(r"error convolucion(x,y)", fontsize=26)
            plt.colorbar(cax=None, orientation='vertical')
            if circles == True:
                plt.scatter(self.sensors.x, self.sensors.y, c='w',
                            s=35)  #,c=colors,s=length
            if clim == None:
                pass
            else:
                plt.clim(clim[0], clim[1])


#			output = self.get('error convolucion')
#			output.draw(kind='señal')
#			plt.gcf()
#			plt.gca()
#			plt.scatter(self.sensors.x, self.sensors.y, c='w', s=35)  #,c=colors,s=length

        if kind == 'errorReal':
            output = self.get('errorReal')
            output.draw(kind='señal')
            plt.gcf()
            plt.gca()
            if circles == True:
                plt.scatter(self.sensors.x, self.sensors.y, c='w',
                            s=35)  #,c=colors,s=length

        if kind == 'lambda':
            output = self.get('lambda')
            output.draw(kind='señal')

        if kind == 'estimacion':
            output = self.get('estimacion')
            output.draw(kind='señal')
            plt.gcf()
            plt.gca()
            if circles == True:
                plt.scatter(self.sensors.x, self.sensors.y, c='w',
                            s=35)  #,c=colors,s=length

        if kind == 'all':
            self.draw('senalReal')
            self.draw('data')
            self.draw('var')
            self.draw('DM')
            self.draw('NEQ')
            self.draw('error')
            self.draw('errorReal')
            self.draw('lambda')
            self.draw('estimacion')

if __name__ == '__main__':

    from krigConv2_tests import test_krigConv2

    checkTiempo = 0
    if checkTiempo == 1:
        from cProfile import run
        run('test_krigConv2()', sort=True)
    else:
        test_krigConv2()
    plt.show()
