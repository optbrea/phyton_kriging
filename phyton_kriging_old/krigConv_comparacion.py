#!/usr/bin/env python
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------
# Name:		articuloKrigingIrregulares.py
# Purpose:	 Funciones y drawings del artículo de kriging por convolucion
#			  con posiciones irregulares
#
# Author:	  Luis Miguel Sanchez Brea
#
# Created:	 12/08/10
# Licence:	 GPL
#----------------------------------------------------------------------
"""
Funciones y drawings del artículo de kriging convolucion con medidas irregulares
"""

import scipy as sp
import matplotlib.pyplot as plt
#from functionesDibujar import guardarDibujo

from phyton_kriging.variogram import variogram
from data_generator.data1D import data1D
from phyton_kriging.standard1D import kriging1D
from phyton_kriging.convolucion import krigingConvolucion1D

from scipy.signal import convolve

um = 1
mm = 1000 * um
degrees = sp.pi / 180


def variogramEtc():

    #	v = variogram(data=s_ideal)
    #	v.establecer_parametros(x=s_ideal.x, s0=0.0001, c0=1, l0=.25, f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")
    #	v.draw(kind='ambos')
    #	guardarDibujo('Fig1.eps')

    #Señal
    func1 = 'sp.sin(2*sp.pi*self.x)'
    rangoX = (0, 4)
    num_data = 250

    #generar señal muestra para la comparacion
    s_ideal = data1D()
    s_ideal.regular_signal(xlim=rangoX, num_data=num_data, f=func1)

    sc = .25
    I0 = 0.01
    nSensores = 4 * 5 + 1

    s_real = data1D()
    s_real.regular_signal(xlim=rangoX, num_data=num_data, f=func1)
    s_real.add_noise(sc=(sc, ), I=(I0, ))

    v = variogram(data=s_real)
    ix = sp.arange(1, round(len(s_real.x)))
    v.compute_experimental(ipoints=ix, recortarmaximum=True)
    v.establecer_parametros(x=s_real.x,
                            s0=sc,
                            c0=1,
                            l0=.25,
                            f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")
    v.draw(kind='ambos')
    plt.title('(a)', fontsize=26)

    calculoTeorico = False
    if calculoTeorico == True:
        paramV = v.ajuste_manual(title='ajuste manual')
        v.establecer_parametros(x=s_real.x,
                                s0=paramV[0],
                                c0=paramV[1],
                                l0=paramV[2],
                                f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")
        v.draw(kind='ambos')
        print 'parametros calculados:', paramV

    #points de sampling
    xsampling = sp.linspace(rangoX[0], rangoX[1], 1000)
    y0 = sp.zeros_like(xsampling)
    m = data1D(x=xsampling, y=y0)

    #kriging convolucion
    kconv = krigingConvolucion1D(sensors=s_real,
                                 variogram=v,
                                 sampling=m,
                                 kind='moderno')
    kconv.computeError(sensors=s_real)
    kconv.draw('DM')
    plt.xlim(xmin=-0.6, xmax=0.6)
    plt.ylim(ymin=0, ymax=1.0)
    plt.title('(b)', fontsize=26)


def medidasRegularmenteDistribuidas():
    """
	data irregularente distribuidos
	"""

    #Señal
    func1 = 'sp.sin(2*sp.pi*self.x)'
    rangoX = (0, 4)
    num_data = 100

    #generar señal muestra para la comparacion
    s_ideal = data1D()
    s_ideal.regular_signal(xlim=rangoX, num_data=num_data, f=func1)

    sc = .125
    I0 = 0.01
    nSensores = 4 * 11 + 1

    s_real = data1D()
    s_real.regular_signal(xlim=rangoX, num_data=num_data, f=func1)
    s_real.add_noise(sc=(sc, ), I=(I0, ))

    v = variogram(data=s_real)
    ix = sp.arange(1, round(len(s_real.x)))
    v.compute_experimental(ipoints=ix, recortarmaximum=True)
    v.establecer_parametros(x=s_real.x,
                            s0=sc,
                            c0=1,
                            l0=.25,
                            f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")
    v.draw(kind='ambos')

    calculoTeorico = False
    if calculoTeorico == True:
        paramV = v.ajuste_manual(title='ajuste manual')
        v.establecer_parametros(x=s_real.x,
                                s0=paramV[0],
                                c0=paramV[1],
                                l0=paramV[2],
                                f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")
        v.draw(kind='ambos')
        print 'parametros calculados:', paramV

    #generar señal a utilizar
    s_irreg = data1D()
    s_irreg.regular_signal(xlim=rangoX, num_data=nSensores, f=func1)
    s_irreg.senal_irregular(xlim=rangoX,
                            num_data=nSensores,
                            f=func1,
                            edges=True)

    s_irreg.add_noise(sc=(sc, ), I=(I0, ))

    #points de sampling
    xsampling = sp.linspace(rangoX[0], rangoX[1], 250)
    y0 = sp.zeros_like(xsampling)
    m = data1D(x=xsampling, y=y0)

    #kriging estandard
    krig = kriging1D(sensors=s_irreg, variogram=v, sampling=m)
    krig.krigingEstandard(filtrado=True)

    #kriging convolucion
    kconv = krigingConvolucion1D(sensors=s_irreg,
                                 variogram=v,
                                 sampling=m,
                                 kind='moderno')
    kconv.computeError(sensors=s_irreg)

    kconv.draw('var')
    kconv.draw('DM')
    plt.figure()

    drawEstimacion(s_ideal, s_irreg, kconv, krig, numFig=2)

    drawErrorNEQ_esteArticulo(s_ideal, s_irreg, kconv, krig, numFig=2)

    #kconv2 = krigingConvolucion1D(sensors=s_irreg, variogram=v, sampling=m, normalizeDM=False)
    #kconv2.computeError(sensors=s_irreg)
    #drawErrorNEQ_esteArticulo(s_ideal, s_irreg, kconv2, krig, numFig=3)


def medidasIrregularmenteDistribuidas():
    """
	data irregularente distribuidos
	"""

    #Señal
    func1 = 'sp.sin(2*sp.pi*self.x)'
    rangoX = (0, 4)
    num_data = 100

    #generar señal muestra para la comparacion
    s_ideal = data1D()
    s_ideal.regular_signal(xlim=rangoX, num_data=num_data, f=func1)

    sc = .25
    I0 = 0.01
    nSensores = 4 * 5 + 1

    s_real = data1D()
    s_real.regular_signal(xlim=rangoX, num_data=num_data, f=func1)
    s_real.add_noise(sc=(sc, ), I=(I0, ))

    v = variogram(data=s_real)
    ix = sp.arange(1, round(len(s_real.x)))
    v.compute_experimental(ipoints=ix, recortarmaximum=True)
    v.establecer_parametros(x=s_real.x,
                            s0=sc,
                            c0=1,
                            l0=.25,
                            f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")
    v.draw(kind='ambos')

    calculoTeorico = False
    if calculoTeorico == True:
        paramV = v.ajuste_manual(title='ajuste manual')
        v.establecer_parametros(x=s_real.x,
                                s0=paramV[0],
                                c0=paramV[1],
                                l0=paramV[2],
                                f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")
        v.draw(kind='ambos')
        print 'parametros calculados:', paramV

    #generar señal a utilizar
    s_irreg = data1D()
    s_irreg.senal_irregular(xlim=rangoX,
                            num_data=nSensores,
                            f=func1,
                            edges=True)
    s_irreg.add_noise(sc=(sc, ), I=(I0, ))

    #points de sampling
    xsampling = sp.linspace(rangoX[0], rangoX[1], 250)
    y0 = sp.zeros_like(xsampling)
    m = data1D(x=xsampling, y=y0)

    #kriging estandard
    krig = kriging1D(sensors=s_irreg, variogram=v, sampling=m)
    krig.krigingEstandard(filtrado=True)

    #kriging convolucion
    kconv = krigingConvolucion1D(sensors=s_irreg,
                                 variogram=v,
                                 sampling=m,
                                 kind='moderno')
    kconv.computeError(sensors=s_irreg)

    kconv.draw('var')
    kconv.draw('DM')

    drawErrorNEQ_esteArticulo(s_ideal, s_irreg, kconv, krig, numFig=2)

    #kconv2 = krigingConvolucion1D(sensors=s_irreg, variogram=v, sampling=m, normalizeDM=False)
    #kconv2.computeError(sensors=s_irreg)
    #drawErrorNEQ_esteArticulo(s_ideal, s_irreg, kconv2, krig, numFig=3)


def variasMediasUnaPosicion():
    """
	data regularmente distribuidos y varios data en una posición
	"""
    #Señal
    func1 = 'sp.sin(2*sp.pi*self.x)'
    rangoX = (0, 4)
    num_data = 100

    #generar señal muestra para la comparacion
    s_ideal = data1D()
    s_ideal.regular_signal(xlim=rangoX, num_data=num_data, f=func1)

    sc = .5
    I0 = 0.01
    nSensores = 4 * 10 + 1

    s_real = data1D()
    s_real.regular_signal(xlim=rangoX, num_data=num_data, f=func1)
    s_real.add_noise(sc=(sc, ), I=(I0, ))

    v = variogram(data=s_real)
    ix = sp.arange(1, round(len(s_real.x)))
    v.compute_experimental(ipoints=ix, recortarmaximum=True)
    v.establecer_parametros(x=s_real.x,
                            s0=sc,
                            c0=1,
                            l0=.25,
                            f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")
    v.draw(kind='ambos')

    calculoTeorico = False
    if calculoTeorico == True:
        paramV = v.ajuste_manual(title='ajuste manual')
        v.establecer_parametros(x=s_real.x,
                                s0=paramV[0],
                                c0=paramV[1],
                                l0=paramV[2],
                                f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")
        v.draw(kind='ambos')
        print 'parametros calculados:', paramV

    s_irreg = data1D(title="data")
    s_irreg.regular_signal(xlim=rangoX, num_data=nSensores, f=func1)
    s_irreg.add_data(x=[2.],
                     y=[0.],
                     nveces=10,
                     yideal=(0., ),
                     sc=(0., ),
                     I=(0., ),
                     ordenar=True)
    s_irreg.add_noise(sc=(sc, ), I=(I0, ))
    s_irreg.draw(kind='ko')

    #variogram
    ix = sp.arange(1, round(len(s_ideal.x)))
    v = variogram(data=s_irreg)
    v.compute_experimental(ipoints=ix, recortarmaximum=True)
    v.establecer_parametros(x=s_ideal.x,
                            s0=sc,
                            c0=1,
                            l0=.25,
                            f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")
    v.draw(kind='ambos')

    #points de sampling
    xsampling = sp.linspace(rangoX[0], rangoX[1], 1000)
    y0 = sp.zeros_like(xsampling)
    m = data1D(x=xsampling, y=y0)

    #kriging estandard
    krig = kriging1D(sensors=s_irreg, variogram=v, sampling=m)
    krig.krigingEstandard(filtrado=True)

    #kriging convolucion
    kconv = krigingConvolucion1D(sensors=s_irreg,
                                 variogram=v,
                                 sampling=m,
                                 kind='moderno')
    kconv.computeError(sensors=s_irreg)

    kconv.draw('var')
    kconv.draw('DM')

    drawErrorNEQ_esteArticulo(s_ideal, s_irreg, kconv, krig, numFig=3)


def removeMedida():
    """
	data irregularente distribuidos
	"""

    #Señal
    func1 = 'sp.sin(2*sp.pi*self.x)'
    rangoX = (-2, 2)
    num_data = 100

    #generar señal muestra para la comparacion
    s_ideal = data1D()
    s_ideal.regular_signal(xlim=rangoX, num_data=num_data, f=func1)

    sc = .5
    I0 = 0.01
    nSensores = 4 * 10 + 1

    s_real = data1D()
    s_real.regular_signal(xlim=rangoX, num_data=num_data, f=func1)
    s_real.add_noise(sc=(sc, ), I=(I0, ))

    v = variogram(data=s_real)
    ix = sp.arange(1, round(len(s_real.x)))
    v.compute_experimental(ipoints=ix, recortarmaximum=True)
    v.establecer_parametros(x=s_real.x,
                            s0=sc,
                            c0=1,
                            l0=.25,
                            f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")
    v.draw(kind='ambos')

    calculoTeorico = False
    if calculoTeorico == True:
        paramV = v.ajuste_manual(title='ajuste manual')
        v.establecer_parametros(x=s_real.x,
                                s0=paramV[0],
                                c0=paramV[1],
                                l0=paramV[2],
                                f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")
        v.draw(kind='ambos')
        print 'parametros calculados:', paramV

    #generar señal a utilizar
    s_irreg = data1D()
    s_irreg.regular_signal(xlim=rangoX, num_data=nSensores, f=func1)
    #s_irreg.senal_irregular(xlim=rangoX, num_data=nSensores, func=func1, edges=True)
    s_irreg.remove_dato(xremove=0.)
    s_irreg.add_noise(sc=(sc, ), I=(I0, ))

    #points de sampling
    xsampling = sp.linspace(rangoX[0], rangoX[1], 250)
    y0 = sp.zeros_like(xsampling)
    m = data1D(x=xsampling, y=y0)

    #kriging estandard
    krig = kriging1D(sensors=s_irreg, variogram=v, sampling=m)
    krig.krigingEstandard(filtrado=True)

    #kriging convolucion
    kconv = krigingConvolucion1D(sensors=s_irreg,
                                 variogram=v,
                                 sampling=m,
                                 kind='moderno')
    kconv.computeError(sensors=s_irreg)

    kconv.draw('var')
    kconv.draw('DM')

    drawErrorNEQ_esteArticulo(s_ideal, s_irreg, kconv, krig, numFig=4)


def interpolation():
    """
	data irregularente distribuidos
	"""

    #Señal
    func1 = 'sp.sin(2*sp.pi*self.x)'
    rangoX = (-2, 2)
    num_data = 100

    #generar señal muestra para la comparacion
    s_ideal = data1D()
    s_ideal.regular_signal(xlim=rangoX, num_data=num_data, f=func1)

    sc = .5
    I0 = 0.01
    nSensores = 4 * 10 + 1

    s_real = data1D()
    s_real.regular_signal(xlim=rangoX, num_data=num_data, f=func1)
    s_real.add_noise(sc=(sc, ), I=(I0, ))

    v = variogram(data=s_real)
    ix = sp.arange(1, round(len(s_real.x)))
    v.compute_experimental(ipoints=ix, recortarmaximum=True)
    v.establecer_parametros(x=s_real.x,
                            s0=sc,
                            c0=1,
                            l0=.25,
                            f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")
    v.draw(kind='ambos')

    calculoTeorico = False
    if calculoTeorico == True:
        paramV = v.ajuste_manual(title='ajuste manual')
        v.establecer_parametros(x=s_real.x,
                                s0=paramV[0],
                                c0=paramV[1],
                                l0=paramV[2],
                                f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")
        v.draw(kind='ambos')
        print 'parametros calculados:', paramV

    #generar señal a utilizar
    s_irreg = data1D()
    s_irreg.regular_signal(xlim=(rangoX[0] + 0.5, rangoX[1] - 0.5),
                           num_data=nSensores,
                           f=func1)
    s_irreg.add_noise(sc=(sc, ), I=(I0, ))

    #points de sampling
    xsampling = sp.linspace(rangoX[0], rangoX[1], 250)
    y0 = sp.zeros_like(xsampling)
    m = data1D(x=xsampling, y=y0)

    #kriging estandard
    krig = kriging1D(sensors=s_irreg, variogram=v, sampling=m)
    krig.krigingEstandard(filtrado=True)

    #kriging convolucion
    kconv = krigingConvolucion1D(sensors=s_irreg,
                                 variogram=v,
                                 sampling=m,
                                 kind='moderno')
    kconv.computeError(sensors=s_irreg)

    kconv.draw('var')
    kconv.draw('DM')

    drawErrorNEQ_esteArticulo(s_ideal, s_irreg, kconv, krig, numFig=5)


def drawEstimacion(s_ideal, s_irreg, kconv, krig, numFig=0):
    """Generar las figuras """

    plt.figure(figsize=(8, 8), num=None, facecolor='w',
               edgecolor='k')  #figsize=(4, 3), dpi=100
    plt.title('(0)', fontsize=26)
    plt.xlabel('$x$', fontsize=26)
    plt.ylabel('$y$', fontsize=26)

    plt.plot(s_ideal.x, s_ideal.y, 'r', label='ideal', lw=2)  #'k-o'
    plt.plot(s_irreg.x, s_irreg.y, 'ko', label='irregular', lw=2)

    plt.plot(krig.kriging.x, krig.kriging.y, 'k', label='kriging',
             lw=2)  #'k-o'
    plt.plot(krig.kriging.x,
             krig.kriging.y + krig.kriging.error,
             'k--',
             label='error bands')
    plt.plot(krig.kriging.x, krig.kriging.y - krig.kriging.error, 'k--')
    plt.plot(kconv.x, kconv.estimacion, 'b', lw=2, label='conv_mala')

    #Lambda 1
    length = krig.lambdas.shape
    lambda1 = krig.lambdas[:, sp.round_(length[1] / 2)]
    xComb, zComb = s_irreg.sampling2(xMuestreo=kconv.sampling.x)

    estimacion = convolve(zComb, lambda1, 'same')
    normalizeLambda = convolve(xComb, lambda1, 'same')
    estimacion = estimacion / normalizeLambda

    plt.plot(kconv.sampling.x, estimacion, 'g', lw=2, label='conv_buena')
    plt.legend()

    #Lambda
    plt.figure(figsize=(8, 8), num=None, facecolor='w',
               edgecolor='k')  #figsize=(4, 3), dpi=100
    plt.title('(a)', fontsize=26)
    plt.xlabel('$x$', fontsize=26)
    plt.ylabel('$\Lambda(x)$', fontsize=26)
    plt.plot(kconv.sampling.x,
             kconv.Lambda,
             'k',
             lw=2,
             label='$\lambda_{conv}$')
    plt.plot(kconv.sampling.x, lambda1, 'r', lw=2, label='$\lambda_{krig}$')

    plt.legend()


def drawErrorNEQ_esteArticulo(s_ideal, s_irreg, kconv, krig, numFig=0):
    """Generar las figuras """

    #NEQ
    plt.figure(figsize=(8, 8), num=None, facecolor='w',
               edgecolor='k')  #figsize=(4, 3), dpi=100
    plt.title('(a)', fontsize=26)
    plt.xlabel('$x$', fontsize=26)
    plt.ylabel('$N_{EQ}(x)$', fontsize=26)

    plt.plot(krig.kriging.x, krig.kriging.NEQ, 'k', lw=2, label='kriging')
    plt.plot(kconv.sampling.x, kconv.NEQ, 'k--', lw=2, label='convolution')
    plt.plot(s_irreg.x, sp.zeros_like(s_irreg.x) + .01, 'ko', lw=8)
    plt.legend()

    #Errores
    plt.figure(figsize=(8, 8), num=None, facecolor='w',
               edgecolor='k')  #figsize=(4, 3), dpi=100
    plt.title('(b)', fontsize=26)
    plt.xlabel('$x$', fontsize=26)
    plt.ylabel('$\sigma(x)$', fontsize=26)

    plt.plot(krig.kriging.x, krig.kriging.error, 'k', lw=2, label='kriging')
    plt.plot(kconv.sampling.x, kconv.error, 'k--', lw=2, label='convolution')
    plt.plot(s_irreg.x, sp.zeros_like(s_irreg.x) + .01, 'ko', lw=8)
    plt.legend()


def ejecutar():

    tests = {
        'variogramEtc': 0,
        'medidasRegularmenteDistribuidas': 0,
        'medidasIrregularmenteDistribuidas': 1,
        'variasMediasUnaPosicion': 0,
        'removeMedida': 0,
        'interpolation': 0,
    }

    if tests['variogramEtc'] == 1:
        variogramEtc()

    if tests['medidasRegularmenteDistribuidas'] == 1:
        medidasRegularmenteDistribuidas()

    if tests['medidasIrregularmenteDistribuidas'] == 1:
        medidasIrregularmenteDistribuidas()

    if tests['variasMediasUnaPosicion'] == 1:
        variasMediasUnaPosicion()

    if tests['removeMedida'] == 1:
        removeMedida()

    if tests['interpolation'] == 1:
        interpolation()


if __name__ == '__main__':
    checkTiempo = 0
    if checkTiempo == 1:
        from cProfile import run
        run('ejecutar()', sort=True)
    else:
        ejecutar()

    plt.show()
    raw_input("pulse enter para terminar")
