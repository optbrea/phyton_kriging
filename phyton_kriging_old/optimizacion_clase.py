#!/usr/bin/env python
# -*- coding: utf-8 -*-
#------------------------------------
# Author:	Luis Miguel Sanchez Brea
# Fecha		2013/10/24 (version 1.0)
# License:	GPL
# Purpose: optimizacion PSO y kriging convolucional
#-------------------------------------

import time
import scipy as sp
import matplotlib.pyplot as plt

from data_generator.data1D import data1D
from data_generator.data2D import data2D
from data_generator.dataMatrix import dataMatrix

from general.funcs_varias import cercano

from scipy.signal import convolve
from phyton_kriging.variogram import variogram

from phyton_kriging.convolucion2D import krigingConvolucion2D
import numpy.random as mtrand

import os

m = 1.
mm = m / 1000.
degrees = sp.pi / 180


def J_errormaximum(error, i_zona_optimizacion):
    return error[i_zona_optimizacion].max()


def J_errorMedio(error, i_zona_optimizacion):
    return sp.mean(error[i_zona_optimizacion])


def J_uniformidad(error, i_zona_optimizacion):
    return sp.std(
        error[i_zona_optimizacion])  #- sp.mean(error[i_zona_optimizacion])


class optimizacion(object):
    """
	Clase en la que se hace la convolución 1D del kriging
	"""

    def __init__(self, zona_optimizacion, variogram, sensors, param_optim,
                 param_visualizacion, error_umbral, I0):
        """Se inicia un new elemento"""

        self.zona_optimizacion = zona_optimizacion
        self.variogram = variogram

        sampling = dataMatrix()
        sampling.generarF(
            x=self.zona_optimizacion.x, y=self.zona_optimizacion.y, f="x+y"
        )  #y en esta función es para compute el error cometido entre la estimación y esto

        self.sampling = sampling
        self.sensors = sensors
        self.param_optim = param_optim
        self.kconv2 = krigingConvolucion2D(sensors=sensors,
                                           variogram=variogram,
                                           sampling=sampling,
                                           kind='moderno')
        self.kconv2.computeError(sensors=sensors)

        self.error_umbral = error_umbral
        self.I0 = I0

        self.param_visualizacion = param_visualizacion

    def __cargar__(self):
        self.i_zona_optimizacion = self.zona_optimizacion.Z > 0.5
        self.items = sp.array(range(self.param_optim['numPasos']))
        self.files = []
        self.i_zona_optimizacion = self.zona_optimizacion.Z > 0.5
        self.items = sp.array(range(self.param_optim['numPasos']))
        self.errormaximum = sp.zeros_like(self.items, dtype=float)
        self.errorMedio = sp.zeros_like(self.items, dtype=float)
        self.uniformidad = sp.zeros_like(self.items, dtype=float)
        self.factor_cobertura = sp.zeros_like(self.items, dtype=float)

    def optimizar(self):
        self.__cargar__()
        param_optim = self.param_optim
        sensors = self.sensors
        num_sensors = len(self.sensors.x)
        zona_optimizacion = self.zona_optimizacion
        Xsampling, Ysampling = sp.meshgrid(self.sampling.x, self.sampling.y)

        #error inicial
        self.kconv2.error = self.kconv2.error * zona_optimizacion.Z

        print zona_optimizacion.Z.shape
        "FUNCIÓN DE OPTIMIZACIÓN"
        num_pasos = param_optim['numPasos']

        diagonal = sp.sqrt(
            (self.zona_optimizacion.x[0] - self.zona_optimizacion.x[-1])**2 +
            (self.zona_optimizacion.y[0] - self.zona_optimizacion.y[-1])**2)

        #bucle de optimización
        for i in sp.arange(num_pasos):

            #muevo los sensors de forma aleatoria
            orden = mtrand.permutation(num_sensors)

            #cada vuelta tiene un value que he definido antes.
            #Cada vez se hace menos aleatorio y los saltos son menores
            energia = param_optim["energia"][i] / diagonal
            aleat = param_optim["aleat"][i] / diagonal
            separation = param_optim["separation"][i] / diagonal

            #=======================================================
            # distances entre sensors
            #=======================================================
            pos_x = sp.mat(sensors.x)
            pos_y = sp.mat(sensors.y)

            d_x = sp.array(pos_x - pos_x.transpose())
            d_y = sp.array(pos_y - pos_y.transpose())

            distances_sensors = sp.sqrt(d_x**2 + d_y**2)
            i_distance_mayor = distances_sensors.argmax()
            idm, jdm = sp.unravel_index(i_distance_mayor,
                                        distances_sensors.shape)
            distance_mayor = distances_sensors[idm, jdm]

            for l in range(num_sensors
                           ):  #cambio las distances de 1 sensor consigo mismo
                distances_sensors[l, l] = 1.1e10

            #=======================================================
            # Bucle de optimización
            #=======================================================
            for j in range(num_sensors):  #muevo los sensors en este bucle
                k = orden[j]  #muevo el sensor k, según el orden anterior

                #el sensor busca hacia donde ir

                #distances entre sensor y posiciones de sampling
                distances = (sensors.x[k] - Xsampling)**2 + (
                    sensors.y[k] -
                    Ysampling)**2  #he quitado sqrt para acelerar
                distances = distances / distances.max()
                #distances = distances / diagonal #mejora para computar una distance fija, como es la diagonal

                #===============================================================
                # 1er elemento de pesado: ir hacia el error máximo
                #===============================================================
                errorpesado = self.kconv2.error * zona_optimizacion.Z * (
                    1 - distances)**param_optim['kappa']
                # errorpesado=errorpesado.*(param.maskOptimizacion-.75*param.edge)

                #incluye edges
                i_max = errorpesado.argmax()
                ierror, jerror = sp.unravel_index(i_max, errorpesado.shape)

                #posicion de mayor error pesado
                x1 = self.sampling.x[jerror]
                y1 = self.sampling.x[ierror]

                #===============================================================
                # 2er elemento de pesado: separation entre sensors
                #===============================================================
                distances_sensor = distances_sensors[:, k]
                imin = distances_sensor.argmin()
                vector = (sensors.x[k] - sensors.x[imin],
                          sensors.y[k] - sensors.y[imin]
                          )  #vector de separación
                vector = vector / (sp.sqrt(
                    (sensors.x[k] - sensors.x[imin])**2 +
                    (sensors.y[k] - sensors.y[imin])**2) + 4 * distance_mayor)

                #posicion actual
                x0 = sensors.x[k]
                y0 = sensors.y[k]

                #nueva posición
                xnew = x0 + energia * (x1 - x0) + aleat * sp.rand(1) * (
                    x1 - x0) + separation * vector[0]
                ynew = y0 + energia * (y1 - y0) + aleat * sp.rand(1) * (
                    y1 - y0) + separation * vector[1]

                #si estoy dentro de la zona de optimización no me puedo salir de la zona, si estoy fuera sí.
                i_x1, tmp, tmp = cercano(self.sampling.x, x0)
                i_y1, tmp, tmp = cercano(self.sampling.y, y0)

                i_x2, tmp, tmp = cercano(self.sampling.x, xnew)
                i_y2, tmp, tmp = cercano(self.sampling.y, ynew)

                if zona_optimizacion.Z[i_x1, i_y1] > 0 and zona_optimizacion.Z[
                        i_x2, i_y2] == 0:
                    sensors.x[k] = x0
                    sensors.y[k] = y0
                else:
                    sensors.x[k] = xnew
                    sensors.y[k] = ynew

            if self.param_optim['anadir_sensor'][i] == True:
                x0, y0 = self.buscar_posicion_inicial_sensor(
                    kind='maximum_error')
                sensors.add_data(x=(x0, ),
                                 y=(y0, ),
                                 z=(0., ),
                                 nveces=1,
                                 zideal=(0., ),
                                 sc=(0, ),
                                 I=(0, ),
                                 ordenar=False)
                num_sensors = len(sensors.x)
                print "añado new sensor"
                #print "factor_cobertura: ", self.factor_cobertura()

            self.kconv2.computeError(
                sensors)  #calcula el error según la convolución
            self.kconv2.error = self.kconv2.error * zona_optimizacion.Z

            self.errormaximum[i] = J_errormaximum(self.kconv2.error,
                                                  self.i_zona_optimizacion)
            self.errorMedio[i] = J_errorMedio(self.kconv2.error,
                                              self.i_zona_optimizacion)
            self.uniformidad[i] = J_uniformidad(self.kconv2.error,
                                                self.i_zona_optimizacion)
            #self.factor_cobertura[i] = self.J_factor_cobertura()

            print i, self.errormaximum[i]
            "FUNCIÓN DE DIBUJO"
            if self.param_visualizacion['guardar_drawing'][i] == True:
                print "drawing ahora"
                #kconv2.error=kconv2.error*zona_optimizacion.Z
                nombre = self.param_visualizacion[
                    "nombre_drawings"] + "%2.2s.png" % (str(i))
                self.draw_error(nombre=nombre, kind='vacio')

            #meter en un wx y será más rápido
            #if i / self.param_visualizacion['draw_cada'] == round(i / self.param_visualizacion['draw_cada']):
            #	self.draw_error()

            if self.param_visualizacion['generar_imagees'] == True:
                self.draw_error(kind='vacio')
                "FUNCIÓN DE VIDEO"
                fname = '_tmp%03d.png' % i
                print 'Saving frame', fname
                plt.savefig(fname)
                self.files.append(fname)

            plt.close()  #probar a ver si así no salen todos
            plt.close()

        self.sensors = sensors

    def optimizar_BACK(self):
        print "entro en optimizar"
        self.__cargar__()
        param_optim = self.param_optim
        sensors = self.sensors
        num_sensors = len(self.sensors.x)
        zona_optimizacion = self.zona_optimizacion
        Xsampling, Ysampling = sp.meshgrid(self.sampling.x, self.sampling.y)

        #error inicial
        self.kconv2.error = self.kconv2.error * zona_optimizacion.Z
        "FUNCIÓN DE OPTIMIZACIÓN"
        num_pasos = param_optim['numPasos']

        diagonal = sp.sqrt(
            (self.zona_optimizacion.x[0] - self.zona_optimizacion.x[-1])**2 +
            (self.zona_optimizacion.y[0] - self.zona_optimizacion.y[-1])**2)

        #bucle de optimización
        for i in sp.arange(num_pasos):

            #muevo los sensors de forma aleatoria
            orden = mtrand.permutation(num_sensors)

            #cada vuelta tiene un value que he definido antes.
            #Cada vez se hace menos aleatorio y los saltos son menores
            energia = param_optim["energia_"][i]
            aleat = param_optim["aleat"][i]

            for j in range(num_sensors):  #muevo los sensors en este bucle
                k = orden[j]  #muevo el sensor k, según el orden anterior
                print k

                #el sensor busca hacia donde ir
                distances = (sensors.x[k] - Xsampling)**2 + (
                    sensors.y[k] -
                    Ysampling)**2  #he quitado sqrt para acelerar
                distances = distances / distances.max()
                #distances = distances / diagonal #mejora para computar una distance fija, como es la diagonal

                errorpesado = self.kconv2.error * zona_optimizacion.Z * (
                    1 - distances)**param_optim['kappa']
                # errorpesado=errorpesado.*(param.maskOptimizacion-.75*param.edge)

                #algun parametro de expulsión de los mínimos de error????????

                #incluye edges
                i_max = errorpesado.argmax()
                ierror, jerror = sp.unravel_index(i_max, errorpesado.shape)

                #posicion de mayor error pesado
                x1 = self.sampling.x[jerror]
                y1 = self.sampling.x[ierror]

                #posicion actual
                x0 = sensors.x[k]
                y0 = sensors.y[k]

                #nueva posición
                xnew = x0 + energia * (x1 - x0) + aleat * sp.rand(1) * (x1 -
                                                                        x0)
                ynew = y0 + energia * (y1 - y0) + aleat * sp.rand(1) * (y1 -
                                                                        y0)

                sensors.x[k] = xnew
                sensors.y[k] = ynew

            if self.param_optim['anadir_sensor'][i] == True:
                x0, y0 = self.buscar_posicion_inicial_sensor(
                    kind='maximum_error')
                sensors.add_data(x=(x0, ),
                                 y=(y0, ),
                                 z=(0., ),
                                 nveces=1,
                                 zideal=(0., ),
                                 sc=(0, ),
                                 I=(0, ),
                                 ordenar=False)
                num_sensors = len(sensors.x)
                print "añado new sensor"
                #print "factor_cobertura: ", self.factor_cobertura()

            self.kconv2.computeError(
                sensors)  #calcula el error según la convolución
            self.kconv2.error = self.kconv2.error * zona_optimizacion.Z

            self.errormaximum[i] = J_errormaximum(self.kconv2.error,
                                                  self.i_zona_optimizacion)
            self.errorMedio[i] = J_errorMedio(self.kconv2.error,
                                              self.i_zona_optimizacion)
            self.uniformidad[i] = J_uniformidad(self.kconv2.error,
                                                self.i_zona_optimizacion)
            #self.factor_cobertura[i] = self.J_factor_cobertura()

            print i, self.errormaximum[i]
            "FUNCIÓN DE DIBUJO"
            if self.param_visualizacion['guardar_drawing'][i] == True:
                print "drawing ahora"
                #kconv2.error=kconv2.error*zona_optimizacion.Z
                nombre = self.param_visualizacion[
                    "nombre_drawings"] + "%2.2s.png" % (str(i))
                self.draw_error(nombre=nombre, kind='vacio')

            #meter en un wx y será más rápido
            #if i / self.param_visualizacion['draw_cada'] == round(i / self.param_visualizacion['draw_cada']):
            #	self.draw_error(kind='vacio')

            if self.param_visualizacion['generar_imagees'] == True:
                self.draw_error(kind='vacio')
                "FUNCIÓN DE VIDEO"
                fname = '_tmp%03d.png' % i
                print 'Saving frame', fname
                plt.savefig(fname)
                self.files.append(fname)

            plt.close()  #probar a ver si así no salen todos
            plt.close()

        self.sensors = sensors

    def distances_sensors(self):
        x1 = sp.array(self.sensors.x)
        x2 = sp.array(self.sensors.x)
        y1 = sp.array(self.sensors.y)
        y2 = sp.array(self.sensors.y)

        x2.resize(len(x2), 1)
        y2.resize(len(y2), 1)

        distance = (x2 - x1)**2 + (y2 - y1)**2
        print distance

    def video(self, filename_1="optimizacion1.avi"):
        print "Generando el video: ", filename_1, ", esto tarda poco. Numero de frames:", str(
            len(self.files))
        texto = "mencoder 'mf://_tmp*.png' -mf kind=png:fps=10 -ovc lavc -lavcopts vcodec=wmv2 -oac copy -o " + filename_1
        #texto = "mencoder 'mf://home/_tmp*.png' -mf kind=png:fps=10 -ovc lavc -lavcopts vcodec=wmv2 -oac copy -o " + filename_1
        os.system(texto)
        #os.system("convert _tmp*.png animation2.gif")  #este sale muy grande
        #esto podria hacer mas pequeño convert -geometry 400 -loop 5  animation2.gif animation3.gif
        # cleanup
        for fname in self.files:
            os.remove(fname)

    def guardar_posiciones(self, filename_1):
        self.sensors.guardarDatos(filename_1)

    def draw_error(self, nombre=None, kind='completo'):
        """
		nombre:	nombre para draw
		kind: 	'completo' - se dibuja todo
				'vacio' - solamente lo de dentro
		"""
        if kind == 'completo':
            xlim = (min(self.sampling.x), max(self.sampling.x))
            ylim = (min(self.sampling.y), max(self.sampling.y))
            self.kconv2.draw(kind='error', circles=True)
            plt.plot(self.sensors.x, self.sensors.y, 'wo', markersize=10)
            plt.axis('equal')
            plt.xlim(xlim)
            plt.ylim(ylim)
            plt.xlabel(u"x (metros)")
            plt.ylabel(u"y (metros)")
            plt.title("")

        elif kind == 'vacio':
            m = self.kconv2.sampling

            plt.figure(figsize=(8, 8))
            plt.axes([0.00, 0.00, .975, .975])
            xlim = (min(self.sampling.x), max(self.sampling.x))
            ylim = (min(self.sampling.y), max(self.sampling.y))
            plt.axis('equal')
            plt.axis('off')
            plt.xlim(xlim)
            plt.ylim(ylim)
            plt.imshow(self.kconv2.error,
                       interpolation='bilinear',
                       aspect='equal',
                       origin='lower',
                       extent=[m.x.min(),
                               m.x.max(),
                               m.y.min(),
                               m.y.max()])
            plt.plot(self.sensors.x, self.sensors.y, 'wo', markersize=18)

        if not nombre == None:
            plt.savefig(nombre, pad_inches=0.1, dpi=300,
                        bbox_inches='tight')  #
            plt.close()

    def draw_quality(self):
        eMax = data1D(x=self.items,
                      y=self.errormaximum,
                      xtext=r"num. iteracion",
                      ytext=r"error maximum (%)",
                      title="")
        eMax.draw()
        plt.ylim(0, max(eMax.y))

        eMean = data1D(x=self.items,
                       y=self.errorMedio,
                       xtext=r"num. iteracion",
                       ytext=r"error Medio (%)",
                       title="")
        eMean.draw()
        plt.ylim(0, max(eMean.y))

        eUnif = data1D(x=self.items,
                       y=self.uniformidad,
                       xtext=r"num. iteracion",
                       ytext=r"uniformidad (%)",
                       title="")
        eUnif.draw()
        plt.ylim(0, max(eUnif.y))

        eCobertura = data1D(x=self.items,
                            y=self.factor_cobertura,
                            xtext=r"num. iteracion",
                            ytext=r"factor_cobertura (%)",
                            title="")
        eCobertura.draw()
        plt.ylim(0, max(eCobertura.y))

    def compute_numero_sensors(self):
        #determinación del número de sensors
        s = self.variogram.pT[0]
        area = sum(sp.ravel(self.zona_optimizacion.Z))  #en uds de pixeles
        area_cobertura_1sensor = (self.error_umbral**2 - self.I0**2) * sum(
            sp.ravel(self.kconv2.DM)) / s**2
        num_sensors = 2 * int(
            area / area_cobertura_1sensor)  #el 2 es porque yo lo valgo
        return num_sensors

    def J_factor_cobertura(self):
        #calcula el porcentaje de pixeles que están por debajo del error umbral
        todos_pixeles = self.kconv2.error.size
        #		print "todos_pixeles:", todos_pixeles

        pixeles_debajo = sum(sp.ravel(self.kconv2.error < self.error_umbral))
        #		print "pixeles_debajo:", pixeles_debajo

        return 100. * (pixeles_debajo / todos_pixeles)

    def buscar_posicion_inicial_sensor(self, kind='aleatorio', pos=(0., 0.)):
        """
		Si se van metiendo sensors uno a uno, hay que decir dónde se pone cada uno de los sensors.
		kind= 'centro' 		- se ubica en 0,0
		      'aleatorio' 	- 
		"""

        if kind == 'aleatorio':
            x0 = self.zona_optimizacion.x.min() + (
                self.zona_optimizacion.x.max() -
                self.zona_optimizacion.x.min()) * sp.rand()
            y0 = self.zona_optimizacion.y.min() + (
                self.zona_optimizacion.y.max() -
                self.zona_optimizacion.y.min()) * sp.rand()

        elif kind == 'maximum_error':
            imax = self.kconv2.error.argmax()
            imin, jmin = sp.unravel_index(imax, self.kconv2.error.shape)
            x0 = self.zona_optimizacion.x[jmin]
            y0 = self.zona_optimizacion.y[imin]

        elif kind == 'posicion_fija':  #no he comprobado
            x0, y0 = pos

        return x0, y0

    def posiciones_sensors(self, title=''):
        """
		function que simplemente devuelve la posición final de los sensors
		"""
        self.sensors.title = title
        return self.sensors


if __name__ == '__main__':

    import sys
    sys.path.append('../')

    from optimizacion_tests import tests_optimizacion

    tests_optimizacion()

    plt.show()
