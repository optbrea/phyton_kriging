#!/usr/bin/env python
# -*- coding: utf-8 -*-
#------------------------------------
# Author:	Luis Miguel Sanchez Brea
# Fecha		2013/10/24 (version 1.0)
# License:	GPL
# Purpose: Optimización de la ubicación de sensors a través del kriging por convolución
#-------------------------------------

import time
import scipy as sp
import matplotlib.pyplot as plt

from data_generator.data1D import data1D
from data_generator.data2D import data2D
from data_generator.dataMatrix import dataMatrix

from scipy.signal import convolve
from phyton_kriging.variogram import variogram

from phyton_kriging.convolucion2D import krigingConvolucion2D
import numpy.random as mtrand

import os

mm = 1.
m = 1000 * mm
degrees = 180 / sp.pi
"INICIAR OPTIMIZACIÓN"

#Generación de la población inicial
s = 0.25
I0 = 0.01
xlim = (-5 * mm, 5 * mm)
ylim = (-5 * mm, 5 * mm)

image = 'circle.bmp'
angle_image = 0 * degrees

error_umbral = .25

#sampling
n_sampling = 32
xsampling = sp.linspace(xlim[0], xlim[1], n_sampling)
ysampling = sp.linspace(ylim[0], ylim[1], n_sampling)
Xsampling, Ysampling = sp.meshgrid(xsampling, ysampling)
"""
#zona donde se pueden ubicar los sensors
zona_sensors=dataMatrix(x=xsampling, y=ysampling, matrix=[], xtext=r"x(mm)", ytext=r"y(mm)", title=r"zona sensors")
zona_sensors.image(filename_1=image, normalize=True, canal=0, lengthImage=False, inverse=False, angle=angle_image)
zona_sensors.draw()
"""

#zona donde se deben optimizar la función
zona_optimizacion = dataMatrix(x=xsampling,
                               y=ysampling,
                               matrix=[],
                               xtext=r"x(mm)",
                               ytext=r"y(mm)",
                               title=r"zona optimizacion")
zona_optimizacion.image(filename_1=image,
                        normalize=True,
                        canal=0,
                        lengthImage=False,
                        inverse=False,
                        angle=angle_image)
zona_optimizacion.draw()

i_zona_optimizacion = zona_optimizacion.Z > 0.5

#sampling
m = dataMatrix(x=xsampling,
               y=ysampling,
               matrix=[],
               xtext=r"x(mm)",
               ytext=r"y(mm)",
               title=r"sampling")
m.image(filename_1=image,
        normalize=True,
        canal=0,
        lengthImage=False,
        inverse=False,
        angle=angle_image)  #no hace falta, pero por si acaso

#calculo del variogram
v = variogram()
v.establecer_parametros(x=sp.linspace(0, 5, 100),
                        s0=s,
                        c0=1,
                        l0=2.5,
                        f_texto="s0**2+c0**2*(1-sp.exp(-(x/l0)**2))")
#v.draw()

#generar DM y otras functiones
t1 = time.time()
kconv2 = krigingConvolucion2D(sensors=None,
                              variogram=v,
                              sampling=m,
                              kind='moderno')
t2 = time.time()

#determinación del número de sensors
area = sum(sp.ravel(zona_optimizacion.Z))  #en uds de pixeles
print "Area: ", area
print "num pixeles: ", len(zona_optimizacion.x) * len(zona_optimizacion.y)
area_porcentaje = area / (len(zona_optimizacion.x) * len(zona_optimizacion.y))
print "area_porcentaje: ", area_porcentaje

area_cobertura_1sensor = (error_umbral**2 - I0**2) * sum(sp.ravel(
    kconv2.DM)) / s**2
print "area_cobertura_1sensor: ", area_cobertura_1sensor

num_sensors = 4 * int(
    area / area_cobertura_1sensor)  #el 4 es porque yo lo valgo
print "num_sensors: ", num_sensors
num_sensors = 8

#Ubicación inicial de los sensors
sensors = data2D(xtext="x(mm)",
                 ytext="y(mm)",
                 ztext="z(mm)",
                 title="posicion inicial")
sensors.senal_aleatoria_image(xlim=xlim,
                              ylim=ylim,
                              image=image,
                              angle=angle_image,
                              num_data=num_sensors,
                              invert=False,
                              f='x')
sensors.add_noise(sc=(s, ), I=(I0, ))
sensors.draw()

#calculo inicial del error
t3 = time.time()
kconv2.computeError(sensors=sensors)
t4 = time.time()

print "tiempos: ", t2 - t1, " error: ", t4 - t3
kconv2.error = kconv2.error
kconv2.draw(kind='error', circles=True, clim=None)
kconv2.error = kconv2.error * zona_optimizacion.Z
kconv2.draw(kind='error', circles=True, clim=None)


def J_errormaximum(error, i_zona_optimizacion):
    return error[i_zona_optimizacion].max()


def J_errorMedio(error, i_zona_optimizacion):
    return sp.mean(error[i_zona_optimizacion])


def J_uniformidad(error, i_zona_optimizacion):
    return sp.std(
        error[i_zona_optimizacion])  #- sp.mean(error[i_zona_optimizacion])ł


plt.show()
