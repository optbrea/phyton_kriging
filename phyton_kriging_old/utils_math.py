#!/usr/local/bin/python
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------
# Name:		general.funcs_varias.py
# Purpose:	 functiones de proposito general para LM
#
# Author:	  Luis Miguel Sanchez Brea
#
# Created:	 2011
# RCS-ID:
# Copyright:
# Licence:	 GPL
#----------------------------------------------------------------------
""" Funciones de proposito general """

#__________________________________________________________________________

from math import factorial

from numpy import array, concatenate, nonzero, ones_like, zeros
from phyton_optics import degrees, mm, nm, np, plt, sp, um


def seq(start, stop, step=1):
    n = int(round((stop - start) / float(step)))
    if n > 1:
        return ([start + step * i for i in range(n + 1)])
    else:
        return ([])


def recortar(x, y, longitud):
    """
        tenemos una function y(x) que tiene ciertos values
        solamente dejamos fuera de 0 aquellos que esten dentro de longitud
        """
    x_central = (x[0] + x[-1]) / 2
    incr = longitud / 2
    xmin = x_central - incr
    xmax = x_central + incr

    i_min, _, _ = cercano(x, xmin)
    i_max, _, _ = cercano(x, xmax)

    y[0:i_min] = 0
    y[i_max::] = 0
    y[-1] = y[-2]

    return y


def muestrear(xsampling, data):
    """devuelve un array de points, ysampling, donde vale 0 si no es cercano y 1 si lo es.
        Esta function es valida para luego hacer las convoluciones

        inputs:
        * xsampling : array de points que son las posiciones del sampling
        * xdata	: array  de points de las posiciones de los 'sensors'

        outputs:
        * ysampling : array con el mismo length de ysampling con values 0 o 1

        mejoras:
        * he metido un bucle for que creo que habria que remove
        * incluir n dimensiones
        """

    ysampling = sp.zeros(xsampling.shape, dtype=float)

    imenores, values, distances = cercano2(xsampling, data)  # @UnusedVariable
    for i in imenores:
        ysampling[int(i)] = 1.
    return ysampling


# def sampling(self, x0=0, x1=1, num_data=1000):
#	"""
#	Esta function pone 1 donde hay un dato y 0 en el resto
#	tambien manda una function identica para los values de x
#	"""
#
#	xMuestreo = sp.linspace(x0, x1, num_data)  #array con el value de x
#	xComb = sp.zeros(len(xMuestreo))	  #array con 1 en posiciones
#	yComb = sp.zeros(len(xMuestreo))	  #array con y(i) en posiciones
#
# for idato in range(len(self.x)):
##			imenor, value, distance = cercano(xMuestreo, self.x[idato])
##			xComb[imenor] = 1
##			yComb[imenor] = self.y[idato]
#
#	imenores, values, distances = cercano2(xMuestreo, self.x)
#	xComb[imenores] = sp.ones(imenores.shape)
#	yComb[imenores] = self.y
#
#	return xMuestreo, xComb, yComb


def distance(x1, x2):
    """
        calcula la distance entre 2 vectores
        """
    x1 = array(x1)
    x2 = array(x2)
    print x1.ndim

    dist2 = 0
    for i in range(x1.ndim):
        dist2 = dist2 + (x1[i] - x2[i])**2

    return sp.sqrt(dist2)


def cercano(vector, numero=0.):
    """calcula la posicion de numero en el vector
        actualmente esta implementado para 1-D, pero habria que hacerlo para n-D
        inputs:
        *vector es un array donde estan los points
        *numero es un dato de los points que se quieren compute

        outputs:
        *imenor - orden del elemento
        *value  - value del elemento
        *distance - diferencia entre el value elegido y el incluido
        """
    assert (sp.isnan(vector).any(), False)  # verifico que no haya ningun nan
    assert (sp.isnan(numero).any(), False)  # verifico que no haya ningun nan
    vector[sp.isnan(vector)] = 1e6  # si hay alguno lo quito

    dist = abs(vector - numero)
    lista = nonzero(dist == min(dist))
    # importante, esto evita que cuando haya dos numeros casque. si solo hay
    # uno no deja lista[0]
    lista = concatenate(lista, 0)
    imenor = int(lista[0])
    value = vector[imenor]
    distance = dist[imenor]

    return imenor, value, distance


def cercano2(vector, numero=array([0.])):
    """calcula la posocion de numero en el vector
        actualmente esta implementado para 1-D, pero habria que hacerlo para n-D

        inputs:
        *vector es un array donde estan los points
        *numero es un array de los points que se quieren compute

        outputs:
        *imenor - orden del elemento
        *value  - value del elemento
        *distance - diferencia entre el value elegido y el incluido

        mejoras:
        *incluir n-dimensiones
        """
    imenores = sp.zeros(numero.shape, dtype=int)
    values = sp.zeros(numero.shape, dtype=float)
    distances = sp.zeros(numero.shape, dtype=float)

    ixs = range(0, len(numero))
    for ix in ixs:
        x = numero[ix]
        dist = abs(vector - x)
        lista = nonzero(dist == min(dist))
        # importante, esto evita que cuando haya dos numeros casque. si solo
        # hay uno no deja lista[0]
        lista = concatenate(lista, 0)
        imenor = int(lista[0])
        imenores[ix] = int(imenor)
        values[ix] = vector[imenor]
        distances[ix] = dist[imenor]

    return imenores, values, distances


def get_image():
    kinds = ('png', 'jpg', 'gif', 'tif', 'xpm')
    nombre = seleccionarArchivo(kinds, nombreGenerico='imagees')
    return nombre


def view_image(nombre):
    if not nombre == '' and not nombre == None:
        im = mpimg.imread(nombre)
        im.show()


def laguerre_polynomial_nk(x, n=4, k=5):
    """functionAuxiliar
        function y = LaguerreGen(varargin)
        LaguerreGen calculates the generalized Laguerre polynomial L{n, alpha}
        This function computes the generalized Laguerre polynomial L{n,alpha}.
        If no alpha is supplied, alpha is set to zero and this function
        calculates the "normal" Laguerre polynomial.

        Input:
        - n = nonnegative integer as degree level
        - alpha >= -1 real number (input is optional)

        The output is formated as a polynomial vector of degree (n+1)
        corresponding to MatLab norms (that is the highest coefficient is the first element).

        Possible usage:
        - polyval(LaguerreGen(n, alpha), x) evaluates L{n, alpha}(x)
        - roots(LaguerreGen(n, alpha)) calculates roots of L{n, alpha}

        Calculation is done recursively using matrix operations for very fast
        execution time. The formula is taken from Szeg?: Orthogonal Polynomials,
        1958, formula (5.1.10)

        Author: Matthias.Trampisch@rub.de
        Date: 16.08.2007
        Version 1.2"""

    f = factorial

    suma = sp.zeros_like(x, dtype=float)

    for m in range(n + 1):
        suma = suma + (-1)**m * f(n + k) / (f(n - m) * f(k + m) * f(m)) * x**m

    return suma


def ndgrid(*args, **kwargs):
    """n-dimensional gridding like Matlab's NDGRID

        The input *args are an arbitrary number of numerical sequences,
        e.g. lists, arrays, or tuples.
        The i-th dimension of the i-th output argument
        has copies of the i-th input argument.

        Optional keyword argument:
        same_dtype : If False (default), the result is an ndarray.
                                 If True, the result is a lists of ndarrays, possibly with
                                 different dtype. This can save space if some *args
                                 have a smaller dtype than others.

        Typical usage:
        >>> x, y, z = [0, 1], [2, 3, 4], [5, 6, 7, 8]
        >>> X, Y, Z = ndgrid(x, y, z) # unpacking the returned ndarray into X, Y, Z

        Each of X, Y, Z has shape [len(v) for v in x, y, z].
        >>> X.shape == Y.shape == Z.shape == (2, 3, 4)
        True
        >>> X
        array([[[0, 0, 0, 0],
                        [0, 0, 0, 0],
                        [0, 0, 0, 0]],
                   [[1, 1, 1, 1],
                        [1, 1, 1, 1],
                        [1, 1, 1, 1]]])
        >>> Y
        array([[[2, 2, 2, 2],
                        [3, 3, 3, 3],
                        [4, 4, 4, 4]],
                   [[2, 2, 2, 2],
                        [3, 3, 3, 3],
                        [4, 4, 4, 4]]])
        >>> Z
        array([[[5, 6, 7, 8],
                        [5, 6, 7, 8],
                        [5, 6, 7, 8]],
                   [[5, 6, 7, 8],
                        [5, 6, 7, 8],
                        [5, 6, 7, 8]]])

        With an unpacked argument list:
        >>> V = [[0, 1], [2, 3, 4]]
        >>> ndgrid(*V) # an array of two arrays with shape (2, 3)
        array([[[0, 0, 0],
                        [1, 1, 1]],
                   [[2, 3, 4],
                        [2, 3, 4]]])

        For input vectors of different data kinds, same_dtype=False makes ndgrid()
        return a list of arrays with the respective dtype.
        >>> ndgrid([0, 1], [1.0, 1.1, 1.2], same_dtype=False)
        [array([[0, 0, 0], [1, 1, 1]]),
         array([[ 1. ,  1.1,  1.2], [ 1. ,  1.1,  1.2]])]

        Default is to return a single array.
        >>> ndgrid([0, 1], [1.0, 1.1, 1.2])
        array([[[ 0. ,  0. ,  0. ], [ 1. ,  1. ,  1. ]],
                   [[ 1. ,  1.1,  1.2], [ 1. ,  1.1,  1.2]]])
        """
    same_dtype = kwargs.get("same_dtype", True)
    V = [array(v) for v in args]  # ensure all input vectors are arrays
    shape = [len(v) for v in args]  # common shape of the outputs
    result = []
    for i, v in enumerate(V):
        # reshape v so it can broadcast to the common shape
        # http://docs.scipy.org/doc/numpy/user/basics.broadcasting.html
        zero = zeros(shape, dtype=v.dtype)
        thisshape = ones_like(shape)
        thisshape[i] = shape[i]
        result.append(zero + v.reshape(thisshape))
    if same_dtype:
        return array(result)  # converts to a common dtype
    else:
        return result  # keeps separate dtype for each output


def meshgrid2(*arrs):
    arrs = tuple(reversed(arrs))  # edit
    lens = map(len, arrs)
    dim = len(arrs)

    sz = 1
    for s in lens:
        sz *= s

    ans = []
    for i, arr in enumerate(arrs):
        slc = [1] * dim
        slc[i] = lens[i]
        arr2 = sp.asarray(arr).reshape(slc)
        for j, sz in enumerate(lens):
            if j != i:
                arr2 = arr2.repeat(sz, axis=j)
        ans.append(arr2)

    return tuple(ans)


def find_files(directorio="/home/lmucm/Dropbox/python/python/imagees/bmps/",
               kinds=("*.tif", "*.bmp")):
    import glob
    import os

    os.chdir(directorio)
    ficheros = []
    for kind in kinds:
        ficheros = ficheros + glob.glob(kind)

    return ficheros


def seleccionarArchivo(kinds, nombreGenerico=None, todoslosarchivos=False):
    import gtk
    import pygtk
    pygtk.require('2.0')

    # Prueba para version de pygtk: PyGtk 2.4
    if gtk.pygtk_version < (2, 3, 90):
        print "Se requiere PyGtk 2.3.90 o posterior"
        raise SystemExit

    dialog = gtk.FileChooserDialog("Open..", None,
                                   gtk.FILE_CHOOSER_ACTION_OPEN,
                                   (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                                    gtk.STOCK_OPEN, gtk.RESPONSE_OK))

    dialog.set_default_response(gtk.RESPONSE_OK)
    filtro = gtk.FileFilter()

    if todoslosarchivos == True:
        filtro.set_name("Todos los archivos")
        filtro.add_pattern("*")

    dialog.add_filter(filtro)

    filtro = gtk.FileFilter()
    filtro.set_name(nombreGenerico)
    for kind in kinds:
        cadena = '*.' + kind
        filtro.add_pattern(cadena)

    dialog.add_filter(filtro)
    response = dialog.run()
    nombre = dialog.get_filename()
    dialog.destroy()

    if response == gtk.RESPONSE_OK:
        return nombre
    else:
        return ''


def guardarMat(filename, nombresVariables, variables):
    """guarda un archivo MAT con los data"""
    import scipy.io

    mdict = {}

    for nombre, variable in zip(nombresVariables, variables):
        mdict[nombre] = variable

    scipy.io.savemat(filename, mdict)


def cargarMat(filename):
    import scipy.io
    data = scipy.io.loadmat(filename)

    #	for (nombre,value) in data.items():
    #		print nombre
    #		print value
    #		exec(nombre+'='+str(value))

    return data
