#!/usr/bin/env python
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------
# Name:		 variogram.py
# Purpose:	 clase variogram 2D
#
# Author:	  Luis Miguel Sanchez Brea
#
# Created:	 2015/09/15
# RCS-ID:
# Copyright:
# Licence:	 GPL
#----------------------------------------------------------------------
"""
clase variogram 2D cuando tiene una estructura radial
"""

import scipy as sp
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons

from general.funcs_varias import cercano
from data_generator.data1D import data1D
from data_generator.data2D import data2D

from scipy.interpolate import UnivariateSpline

um = 1
mm = 1000 * um
pi = sp.pi
degrees = pi / 180

from scipy.optimize import curve_fit


def variogram_gauss(x, sc, A, lc):
    return sc + A * (1 - sp.exp(-sp.array(x)**2 / lc**2))


def variogram_exponencial(x, sc, A, lc):
    return sc + A * (1 - sp.exp(-sp.array(x) / lc))


def variogram_esferico(x, sc, A, lc):
    return sc + A * (1 - sp.exp(-sp.array(x) / lc))


class variogram2D_radial(object):

    def __init__(self, data, function_ajuste):
        """Se inicia un new e1emento
		data = = data2D()
		function_ajuste: variogram_gauss, variogram_exponencial, variogram_esferico

		"""
        self.data = data  #data experimentales que dan el variogram
        self.function_ajuste = function_ajuste

        self.h = []
        self.variogram_exp = []
        self.num_h = []
        self.i_pos = []
        self.imax = []

        self.popt = []
        self.pcov = []
        self.imax = []

        self.diferencias = []
        self.quality = []

    def experimental(self, incr_h=0.1):
        """
		incr_h: es la distance entre los points del variogram
		fill_nugget: para compute el nugget: 'none', 'nearest', 'linear'
		"""

        #aconditionamiento de señales para compute bien
        posiciones1_x = sp.array(self.data.x[sp.newaxis, :])
        posiciones2_x = sp.array(self.data.x[:, sp.newaxis])
        posiciones1_y = sp.array(self.data.y[sp.newaxis, :])
        posiciones2_y = sp.array(self.data.y[:, sp.newaxis])

        intensity1 = sp.array(self.data.z[sp.newaxis, :])
        intensity2 = sp.array(self.data.z[:, sp.newaxis])

        diferencias_intensity = intensity2 - intensity1

        #medida de la distance mediante un broadcasting
        distances = sp.sqrt((posiciones2_x - posiciones1_x)**2 +
                            (posiciones2_y - posiciones1_y)**2)

        #calculo de las posiciones y de las redondeadas
        i_pos = sp.around(distances / incr_h)

        #paso a una dimension
        i_pos = sp.ravel(i_pos).askind(int)  #se pasa a enteros
        posiciones_redondeadas = i_pos * incr_h
        distances = sp.ravel(distances)
        diferencias_intensity = sp.ravel(diferencias_intensity)

        i_ordenar = sp.argsort(i_pos)
        distances = distances[i_ordenar]
        diferencias_intensity = diferencias_intensity[i_ordenar]
        i_pos = i_pos[i_ordenar]
        posiciones_redondeadas = posiciones_redondeadas[i_ordenar]

        ih = range(max(i_pos))
        variogram_exp = sp.zeros_like(ih, dtype=float)
        num_points = sp.zeros_like(ih, dtype=float)

        for i in ih:
            posiciones = (i_pos == i)
            num_points[i] = posiciones.sum().askind(int)
            variogram_exp[i] = (diferencias_intensity[posiciones]**
                                2).sum() / num_points[i]

        #para remove aquellos points que no tienen nada. Los dejo a 0
        i_remove = sp.argwhere(num_points == 0)
        variogram_exp = sp.delete(variogram_exp, i_remove)
        num_points = sp.delete(num_points, i_remove)
        ih = sp.delete(ih, i_remove)

        self.h = sp.array(ih) * incr_h
        self.variogram_exp = variogram_exp
        self.num_h = num_points
        self.i_pos = i_pos
        self.imax = len(self.h)  #por si no se hace la de ajuste

    def nugget(self, fill_nugget='none', longitud=8):
        """
		cambia el nugget effect
		fill_nugget='none''nearest' 'linear'
		longitud es el número de points tomados para el spline
		"""
        if fill_nugget == 'none':
            self.variogram_exp[0] = self.variogram_exp[0]
        elif fill_nugget == 'nearest':
            self.variogram_exp[0] = self.variogram_exp[1]
        elif fill_nugget == 'linear':
            spl = UnivariateSpline(self.h[1:longitud],
                                   self.variogram_exp[1:longitud])
            self.variogram_exp[0] = spl(0)

    def compute_param_ini(self):
        """
		calcula los parámetros iniciales para el ajuste del variogram
		"""
        point_central = (self.variogram_exp.max() +
                         self.variogram_exp.min()) / 2
        imenor, value, distance = cercano(self.variogram_exp, point_central)
        param_ini = [
            self.variogram_exp[0],
            self.variogram_exp.max(), self.h[imenor]
        ]

        return param_ini

    def ajuste(self, param_ini, corte=.5):
        """
		ajuste, pero solo a la parte mayor
		param_ini: son los parametros iniciales para el ajuste
		corte es la posicion de corte para buscar el máximo
		"""

        h = self.h
        variogram_exp = self.variogram_exp
        function_ajuste = self.function_ajuste

        imax = (variogram_exp[0:round(len(variogram_exp) *
                                      corte)]).argmax() + 1

        variogr = sp.zeros_like(variogram_exp)
        variogr[0:imax] = variogram_exp[0:imax]
        variogr[imax:-1] = variogram_exp[imax]

        popt, pcov = curve_fit(function_ajuste, h, variogr, p0=param_ini)
        popt[0] = param_ini[0]

        diferencias = variogr[0:imax] - function_ajuste(h[0:imax], *popt)
        quality = sp.sqrt((diferencias**2).sum()) / len(diferencias)

        self.popt = popt
        self.pcov = pcov
        self.imax = imax

        self.diferencias = diferencias
        self.quality = quality
        #print "parametros de ajuste para gaussiano: ", self.popt
        #print "quality: ", quality
        return quality, diferencias

    def f(self, x):
        """
		devuelve ajuste teórico a la function utilizada
		necesitamos los parámetros de ajuste
		"""
        #verificar que functiona
        return self.function_ajuste(x, *self.popt)

    def draw(self, kind):
        if kind == 'data' or kind == 'all_':
            self.data.draw()

        if kind == 'variogram' or kind == 'all_':
            plt.figure()
            plt.subplot(2, 1, 1)
            plt.xlabel('h', fontsize=22)
            plt.ylabel(r'$\gamma (h)$', fontsize=22)
            plt.plot(self.h, self.variogram_exp, 'k', lw=2)
            plt.subplot(2, 1, 2)
            plt.xlabel('h', fontsize=22)
            plt.ylabel(r'$ num \, data$', fontsize=22)
            plt.plot(self.h, self.num_h, 'k', lw=2)

        if kind == 'ajuste' or kind == 'all_':
            imax = self.imax
            h = self.h
            popt = self.popt
            variogram_exp = sp.sqrt(self.variogram_exp)

            plt.figure()
            plt.plot(h, variogram_exp, 'k', lw=2)
            plt.plot(h[0:imax], variogram_exp[0:imax], lw=3)
            plt.plot(h, sp.sqrt(self.function_ajuste(h, *popt)), 'r', lw=2)
            plt.title('ajuste', fontsize=24)

        if kind == 'diferencias' or kind == 'all_':
            plt.figure()
            plt.xlabel('h', fontsize=22)
            plt.ylabel(r'$diferencias (h)$', fontsize=22)
            plt.plot(self.h[0:self.imax], self.diferencias, 'k', lw=2)


if __name__ == '__main__':
    from variogram2D_radial_tests import tests_variogram

    checkTiempo = 0  #falta modulo pstats
    if checkTiempo == 1:
        from cProfile import run
        run('tests_variogram()', sort=True)
    else:
        tests_variogram()

    plt.show()
