#!/usr/bin/env python
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------
# Name:			 variogram.py
# Purpose:		 clase variogram 1D
#
# Author:		  Luis Miguel Sanchez Brea
#
# Created:		 2011
# RCS-ID:
# Copyright:
# Licence:		 GPL
#----------------------------------------------------------------------
"""
clase variogram 1D
"""

from phyton_kriging import plt, sp, data1D
from matplotlib.widgets import Slider, Button, RadioButtons
from scipy import pi, exp, zeros, arange, diff, sqrt, array, zeros_like
from scipy.optimize import curve_fit
from scipy.interpolate import UnivariateSpline
from phyton_kriging.utils_math import cercano


def variogram_constante(h, sc, A, lc):
    """variogram constante
	A y lc son inutiles, por continuidad con el resto
	"""
    return sc**2 * sp.ones_like(h)


def variogram_gauss(h, sc, A, lc):
    """variogram gaussiano"""
    return sc**2 + A * (1 - exp(-h**2 / lc**2))


def variogram_exponencial(h, sc, A, lc):
    """variogram exponencial"""
    return sc**2 + A * (1 - exp(-h / lc))


def variogram_esferico(h, sc, A, lc):
    """variogram esférico"""
    variogram = sc**2 + A * (1.5 * h / lc - 0.5 * (h / lc)**3)
    if (h > lc).any(): variogram[h > lc] = sc**2 + A
    return variogram


def variogram_cubico(h, sc, A, lc):
    """variogram cúbico"""
    h_norm = h / lc
    variogram = sc**2 + A * (7 * h_norm**2 - 35 / 4 * h_norm**3 +
                             7 / 2 * h_norm**5 - 3 / 4 * h_norm**7)
    if (h > lc).any(): variogram[h > lc] = sc**2 + A
    return variogram


def variogram_potencial(h, sc, A, lc, potencia):
    """variogram potencial"""
    return sc**2 + A * (h / lc)**potencia


Tipos_variograms = [
    variogram_constante, variogram_gauss, variogram_exponencial,
    variogram_esferico, variogram_cubico, variogram_potencial
]

um = 1
mm = 1000 * um
degrees = pi / 180

## functiones para el ajuste manual


def update(val):
    """para el ajuste manual"""
    sc = sS.val
    lc = sL.val
    hc = sH.val
    l2b.set_ydata(sc**2 + hc * (1 - exp(-(h / lc)**2)))
    plt.draw()


def aceptar(event):
    print sS.val, sL.val, sH.val
    global paramV

    self.popt[0] = sS.val
    self.popt[1] = sH.val
    self.popt[2] = sL.val

    print "en aceptar: ", str(self.popt)
    plt.close()


class variogram(object):
    """
	clase variogram teorico y experimental para data unidimensionales
	"""

    def __init__(self, data, function_ajuste):
        """Se inicia un new elemento
		data = data1D()"""
        self.data = data  #data experimentales que dan el variogram
        self.function_ajuste = function_ajuste

        self.h = []
        self.variogram_exp = []
        self.num_h = []
        self.i_pos = []
        self.imax = []

        self.popt = [0, 0, 0]
        self.pcov = []
        self.imax = []

        self.diferencias = []
        self.quality = []

    def compute_experimental(self, ipoints):
        data = self.data.y
        variogram_exp = zeros_like(ipoints, dtype=float)
        num_points = zeros_like(ipoints, dtype=int)

        num_data = data.size
        npoints = ipoints.size

        for i in arange(0, npoints):
            hi = ipoints[i]
            VAR = 0
            num_data = 0
            for j in arange(1, hi + 1):
                posiciones = arange(j, num_data, hi, dtype='int')
                values = data[posiciones]
                diferencias = diff(values)**2
                VAR = VAR + diferencias.sum()
                num_data = num_data + diferencias.size

            variogram_exp[i] = VAR / (2 * num_data)
            num_points[i] = num_data

        incr_h = self.data.x[1] - self.data.x[0]

        #para remove aquellos points que no tienen nada. Los dejo a 0
        i_remove = sp.argwhere(num_points == 0)
        data = sp.delete(data, i_remove)
        variogram_exp = sp.delete(variogram_exp, i_remove)
        num_points = sp.delete(num_points, i_remove)
        ipoints = sp.delete(ipoints, i_remove)

        self.h = ipoints * incr_h
        self.variogram_exp = variogram_exp
        self.num_h = num_points
        self.i_pos = ipoints
        self.imax = len(self.h)  #por si no se hace la de ajuste

    def eliminar_fluctuaciones(self, porcentaje=.5):
        """a partir de un value pone todo al level máximo"""
        v = self.variogram_exp
        imax = (v[0:round(len(v) * porcentaje)]).argmax() + 1
        v_max = v[imax]

        #print variogram
        print "i_corte", imax
        self.variogram_exp[imax:] = v_max  #para que luego busque bien el value

    def establecer_parametros(self, *parametros):
        """
		se establecen los parámetros teóricos del variogram
		"""

        self.popt = sp.array(parametros)

    def nugget(self, fill_nugget='none', longitud=8):
        """
		cambia el nugget effect
		fill_nugget='none''nearest' 'linear'
		longitud es el número de points tomados para el spline
		"""
        if fill_nugget == 'none':
            self.variogram_exp[0] = self.variogram_exp[0]
        elif fill_nugget == 'nearest':
            self.variogram_exp[0] = self.variogram_exp[1]
        elif fill_nugget == 'linear':
            spl = UnivariateSpline(self.h[1:longitud],
                                   self.variogram_exp[1:longitud])
            self.variogram_exp[0] = spl(0)

    def compute_param_ini(self):
        """
		calcula los parámetros iniciales para el ajuste del variogram
		"""
        point_central = (self.variogram_exp.max() +
                         self.variogram_exp.min()) / 2
        imenor, _, _ = cercano(self.variogram_exp, point_central)
        param_ini = sp.array(
            [self.variogram_exp[0],
             self.variogram_exp.max(), self.h[imenor]])

        if self.function_ajuste in [variogram_potencial]:
            param_ini = sp.append(param_ini, [3])

        return param_ini

    def ajuste(self, param_ini, corte=1.):
        """
		ajuste, pero solo a la parte mayor
		param_ini: son los parametros iniciales para el ajuste
		corte es la posicion de corte para buscar el máximo
		"""

        h = self.h
        variogram_exp = self.variogram_exp
        function_ajuste = self.function_ajuste

        xx = variogram_exp[0:round(len(variogram_exp) * corte)]
        imax = xx.argmax()
        if corte < 1.:
            variogr = zeros_like(variogram_exp)
            variogr[0:imax] = variogram_exp[0:imax]
            variogr[imax:-1] = variogram_exp[imax]
        else:
            variogr = variogram_exp

        popt, pcov = curve_fit(function_ajuste, h, variogr, p0=param_ini)
        #if popt[0] < 0: #el nugget-effect no puede ser menor de 0
        #	popt[0] = 0
        popt[0] = param_ini[0]  #temporal, para que no salga nulo

        self.popt = popt
        self.pcov = pcov
        self.imax = imax
        print "ajuste: ", self.popt

        diferencias = variogr[0:imax] - function_ajuste(h[0:imax], *popt)
        quality = sp.sqrt((diferencias**2).sum()) / len(diferencias)

        self.diferencias = diferencias
        self.quality = quality
        print "parametros de ajuste para ", self.function_ajuste.__name__, " = ", self.popt
        print "quality: ", quality
        return quality, diferencias

    def f(self, x):
        """Si x==None entonces cogemos los data de var.varTeo.x o var.varExp.x
		   kind='teorico' o 'experimental'
		   si almacen==True entonces guardamos los data en varTeo.x, y o varExp.x,y
		"""

        return self.function_ajuste(x, *self.popt)

    def ajuste_manual(self, title='ajuste manual'):
        """
		se ajusta de forma manual el variogram, con drawings
		"""
        global l2b, sS, sL, sH, h
        global paramV

        paramV = zeros(3, dtype=float)

        id_fig = plt.figure()
        ax1 = plt.subplot(121)
        l1, = plt.plot(self.data.x, self.data.y, lw=2, color='k')

        ax2 = plt.subplot(122)
        plt.subplots_adjust(left=0.15, bottom=0.25)
        l2a, = plt.plot(self.h, self.variogram_exp, lw=2, color='k')
        plt.title(title)

        sc0, hc0, lc0 = self.popt

        h = self.h

        varTeoricoAjuste = self.f(h)
        l2b, = plt.plot(h, varTeoricoAjuste, lw=2, color='red')
        plt.axis([0, self.h[-1], 0, 1.1 * self.variogram_exp.max()])

        axcolor = 'lightgoldenrodyellow'
        axS = plt.axes([0.15, 0.15, 0.75, 0.03], axisbg=axcolor)
        axH = plt.axes([0.15, 0.10, 0.75, 0.03], axisbg=axcolor)
        axL = plt.axes([0.15, 0.05, 0.75, 0.03], axisbg=axcolor)

        sS = plt.Slider(axS,
                        'sc',
                        0,
                        sqrt(self.variogram_exp.max()),
                        valinit=sc0)
        sH = plt.Slider(axH,
                        'hc',
                        0,
                        1.5 * self.variogram_exp.max(),
                        valinit=hc0)
        sL = plt.Slider(axL, 'lc', self.h[-1] / 1000, self.h[-1], valinit=lc0)

        sS.on_changed(update)
        sL.on_changed(update)
        sH.on_changed(update)

        #	resetax = plt.axes([0.01, 0.05, 0.1, 0.04])
        #	buttonReset = plt.Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')
        #	buttonReset.on_clicked(reset)

        aceptarax = plt.axes([0.01, 0.1, 0.1, 0.04])
        buttonAceptar = plt.Button(aceptarax,
                                   'aceptar',
                                   color=axcolor,
                                   hovercolor='0.975')
        buttonAceptar.on_clicked(aceptar)

        self.popt = paramV
        plt.show()
        return self.popt

    def draw(self, kind='teorico', x=0.):
        plt.figure(figsize=(8, 8), facecolor='w', edgecolor='k')

        if kind == 'teorico':
            self.__drawVariogramaTeorico(x)
        if kind == 'experimental':
            self.__drawVariogramaExperimental()
        if kind == 'ambos':
            self.__drawVariogramaCompleto()
        if kind == 'ajuste':
            self.__draw_ajuste()
        if kind == 'all':
            self.__drawVariogramaTodo()

    def __drawVariogramaTeorico(self, h):

        plt.plot(h,
                 self.f(h),
                 'k',
                 label=r"$ \sqrt{\gamma_{Teo}(h)}  $",
                 linewidth=2)
        plt.xlabel(r"$ h $", fontsize=22)
        plt.ylabel(r"$ \sqrt{\gamma_{Teo}(h)}  $", fontsize=22)
        plt.title('variogram teorico')
        plt.legend(shadow=False)

    def __drawVariogramaExperimental(self):
        #plt.plot(self.h, sqrt(self.variogram_exp), 'k', label=r"$ \sqrt{\gamma_{Exp}(h)}  $", linewidth=2)
        plt.plot(self.h,
                 self.variogram_exp,
                 'k',
                 label=r"$ \gamma_{Exp}(h) $",
                 linewidth=2)
        plt.xlabel(r"$ h $", fontsize=22)
        plt.ylabel(r"$ \gamma_{Exp}(h)  $", fontsize=22)
        plt.title('variogram experimental')
        plt.legend(shadow=False)

    def __drawVariogramaCompleto(self):
        "dibuja la function"
        plt.plot(self.h,
                 sqrt(self.f(self.h)),
                 'k',
                 label=r"$ \sqrt{\gamma_{Teo}(h)}  $",
                 linewidth=2)
        plt.plot(self.h,
                 sqrt(self.variogram_exp),
                 'k--',
                 label=r"$ \sqrt{\gamma_{Exp}(h)}  $",
                 linewidth=2)

        plt.xlabel(r"$ h $", fontsize=20)
        plt.ylabel(r"$ \sqrt{\gamma(h)}  $", fontsize=20)
        plt.title("variogram", fontsize=26)

        plt.legend(shadow=False)

    def __draw_ajuste(self):

        imax = self.imax
        h = self.h
        popt = self.popt
        variogram_exp = sp.sqrt(self.variogram_exp)
        plt.plot(h, variogram_exp, 'k', lw=2, label='completo')
        plt.plot(h[0:imax], variogram_exp[0:imax], lw=3, label='cortado')
        plt.plot(h,
                 sp.sqrt(self.function_ajuste(h, *popt)),
                 'r',
                 lw=2,
                 label='ajuste')
        plt.title('ajuste', fontsize=24)
        plt.xlabel(r"$ h $", fontsize=22)
        plt.ylabel(r"$ {\gamma(h)}$", fontsize=22)
        plt.legend(shadow=False)

    def __drawVariogramaTodo(self):
        self.draw("teorico", self.h)
        self.draw("experimental", self.h)
        self.draw("ambos", self.h)
        self.draw("ajuste", self.h)
