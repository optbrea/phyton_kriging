# !/usr/bin/env python
# -*- coding: utf-8 -*-
#
#
#  Copyright 2014 Luis Miguel <luismiguel@luismiguel-desktop>
""" use colimador_period para el paper de optics letters que nos tiraron
aquí intento simplificar las cosas y hacerlo más claro y rápido
He llegado a no entender el código
"""

tmp_draw = False

import scipy.io
import scipy as sp
from scipy import squeeze, arange, polyfit, polyval, array, zeros_like
import matplotlib.pyplot as plt

from data_generator.data1D import data1D
from phyton_kriging.variogram1D import *

from scipy.optimize import curve_fit
from scipy import stats


def cargar_data(filename_1,
                nombrex='x',
                nombrez='deltaz',
                nombreI='IF_delta',
                rotate=False):
    """coge una image y extrae los data"""

    data = scipy.io.loadmat(filename_1)
    z = squeeze(data[nombrez])  # Pos
    I = sp.real(data[nombreI])  # profile
    x = squeeze(data[nombrex])
    if rotate is True:
        I = sp.transpose(I)
    return x, z, I


def visualizar_variogram(x,
                         z,
                         I,
                         zi,
                         ix=arange(1, 300),
                         todos_variograms=False,
                         draw=True):
    """visualiza el variogram elegido
    zi es la distance a visualizar. se busca con cercano
    ix es el rango del variogram a visualizar en pixeles
    si todos_variograms is True se visualizan en la misma figura
     todos los de la image
    ¿por que es más lento el rango (1000, 1100) que el (1, 100)?
    """
    from mpl_toolkits.axes_grid1 import host_subplot
    import matplotlib.transforms as mtransforms

    print z

    num_z = len(z)

    plt.figure()

    ax = host_subplot(111)

    if todos_variograms is False:
        i_z, tmp, tmp = cercano(z, zi)
        i_z = (i_z, )
    else:
        i_z = range(num_z)

    for i in i_z:
        print i
        profile = squeeze(I[i, :])
        points = data1D(x=x, y=profile)
        var = variogram(data=points, function_ajuste=variogram_gauss)

        var.compute_experimental(ipoints=ix)
        # var.draw(kind  =  'experimental')
        h = var.h
        if draw == True:
            v = var.variogram_exp
            ax.plot(h, v)
            aux_trans = mtransforms.Affine2D().scale(h[1] - h[0], 1.)
            ax2 = ax.twin(
                aux_trans
            )  # ax2 is responsible for "top" axis and "right" axis
            ax2.axis["right"].major_ticklabels.set_visible(False)


def ajuste_minimum(x, y):
    # calcula la posicion de minimum mediante un ajuste cuadratico
    coefs = sp.polyfit(x, y, 2)
    pos = -coefs[1] / (2 * coefs[0])
    y2 = sp.polyval(coefs, x)
    # plt.plot(x, y2, lw = 4)
    return pos


def period_senal(h, v, ix, num_minimum):
    '''
    calcula el period de la señal,  debería haber solo un mínimo
    h, v es el variogram (cuidado antes estaba x,  profile pero lo he sacado)
    ix es el número de points del profile
    orden_minimum es el orden del mínimo para dividir entre el y
        compute directamente    el minimum
    '''

    # calcula el mínimo directamente a partir del pixel mínimo
    iminimum = v[ix].argmin() + ix[0]
    period = h[iminimum]  # *1000
    # print iminimum

    # if tmp_draw==True:
    #     plt.plot(h, v)

    # realiza un ajuste cuadrático
    period_interpolado = ajuste_minimum(h[ix], v[ix])
    return period, period_interpolado, iminimum


def calculo_period(h, v, anchura_minimum=10):
    # x, z,  I = cargar_data(archivos[0])

    i_minimums_burda = posicion_minimums_burda(h, v)
    num_minimums = len(i_minimums_burda)
    # print i_minimums_burda

    Periodo = sp.zeros_like(i_minimums_burda, dtype=float)
    Periodo_interpolado = sp.zeros_like(i_minimums_burda, dtype=float)
    Iminimum = sp.zeros_like(i_minimums_burda, dtype=int)
    I = sp.zeros_like(i_minimums_burda, dtype=int)

    for i in range(num_minimums):
        if tmp_draw == True:
            plt.figure()
        # ix = ix+period_inicial+1 # cambio el rango
        ix = arange(i_minimums_burda[i] - anchura_minimum,
                    i_minimums_burda[i] + anchura_minimum)
        period, period_interpolado, iminimum = period_senal(h,
                                                            v,
                                                            ix=ix,
                                                            num_minimum=i + 1)
        Periodo[i] = period
        Periodo_interpolado[i] = period_interpolado
        Iminimum[i] = iminimum
        I[i] = i
        if tmp_draw == True:
            plt.plot(h[ix], v[ix], 'ko')

    Periodo = sp.append(0, Periodo)
    Periodo_interpolado = sp.append(0, Periodo_interpolado)

    period = sp.diff(Periodo)
    period_interpolado = sp.diff(Periodo_interpolado)
    iminimum = array(Iminimum)
    i = array(I)
    # print i
    # print "calculo period:", period,  period_interpolado,  iminimum

    return period, period_interpolado, iminimum, i


"""
def period_senal(h, v,  ix,  num_minimum):
    '''
    calcula el period de la señal,  debería haber solo un mínimo
    h, v es el variogram (cuidado antes estaba x,  profile pero lo he sacado)
    ix es el número de points del profile
    orden_minimum es el orden del mínimo para dividir entre el y
        compute directamente    el minimum
    '''

    # calcula el mínimo directamente a partir del pixel mínimo
    iminimum =  v[ix].argmin()+ix[0]
    period = h[iminimum]/num_minimum # *1000
    # print iminimum

    # if tmp_draw==True:
    #     plt.plot(h, v)

    # realiza un ajuste cuadrático
    period_interpolado = ajuste_minimum(h[ix], v[ix])/num_minimum # *1000

    return period,  period_interpolado,  iminimum



def calculo_period(h, v,  anchura_minimum = 10):
    # x, z,  I = cargar_data(archivos[0])

    i_minimums_burda = posicion_minimums_burda(h,v)
    num_minimums=len(i_minimums_burda)
    print i_minimums_burda

    Periodo=sp.zeros_like(i_minimums_burda,dtype=float)
    Periodo_interpolado=sp.zeros_like(i_minimums_burda,dtype=float)
    Iminimum=sp.zeros_like(i_minimums_burda,dtype=int)
    I=sp.zeros_like(i_minimums_burda,dtype=int)

    for i in range(num_minimums):
        if tmp_draw==True:
            plt.figure()
        # ix = ix+period_inicial+1 # cambio el rango
        ix = arange(i_minimums_burda[i]-anchura_minimum, i_minimums_burda[i]+anchura_minimum)
        period,  period_interpolado,  iminimum  = period_senal(h, v,  ix = ix,  num_minimum = i+1)
        print "calculo period:", period,  period_interpolado,  iminimum
        Periodo[i] = period
        Periodo_interpolado[i] = period_interpolado
        Iminimum[i] = iminimum
        I[i] = i
        if tmp_draw==True:
            plt.plot(h[ix], v[ix], 'ko')

    period = array(Periodo)
    period_interpolado = array(Periodo_interpolado)
    iminimum = array(Iminimum)
    i = array(I)
    # print i

    return period,  period_interpolado,  iminimum,  i
"""
"""
seha quitado y parece que la actual functiona mejor.
de todas formas no functiona todavía de forma optica
def calculo_period(h, v,  anchura_minimum = 10,  period_inicial = 42):
    # x, z,  I = cargar_data(archivos[0])

    ix = arange(period_inicial-anchura_minimum, period_inicial+anchura_minimum)
    period,  period_interpolado,  iminimum  = period_senal(h, v,  ix = ix,  num_minimum = 1)
    Periodo = [period]
    Periodo_interpolado = [period_interpolado]
    Iminimum = [iminimum]
    # I = [int(h[0]/(period_inicial*(h[1]-h[0]))+1)]
    I = [1]

    num_minimums = int(round(len(h)/period_inicial))
    print "numero de minimums: {}".format(num_minimums)

    print period,  period_interpolado,  iminimum
    iminimum = Iminimum

    i_minimums_burda = posicion_minimums_burda(h,v)
    print i_minimums_burda

    for i in range(num_minimums):
        if tmp_draw==True:
            plt.figure()
        print "caca"
        # ix = ix+period_inicial+1 # cambio el rango
        ix = arange(i_minimums_burda[i]-anchura_minimum, i_minimums_burda[i]+anchura_minimum)
        period,  period_interpolado,  iminimum  = period_senal(h, v,  ix = ix,  num_minimum = I[-1]+1)
        print period,  period_interpolado,  iminimum
        Periodo.append(period)
        Periodo_interpolado.append(period_interpolado)
        Iminimum.append(iminimum)
        I.append(I[-1]+1)
        if tmp_draw==True:
            plt.plot(h[ix], v[ix], 'ko')

    period = array(Periodo)
    period_interpolado = array(Periodo_interpolado)
    iminimum = array(Iminimum)
    i = array(I)
    print i

    return period,  period_interpolado,  iminimum,  i
"""


def posicion_minimums_burda(x, I):
    pos_minimums = []
    for i in range(1, len(x) - 1):
        if I[i - 1] > I[i] and I[i + 1] > I[i]:
            pos_minimums.append(i)
    return pos_minimums


"""
def calculo_period_INICIAL(x, z,  I):
    # calculo del period a partir del variogram considerando siempre el mismo rango
    num_z = len(z)

    visualizar_variogram(todos_variograms = False)
    raw_input("period aproximado")

    # print 100/2.2*num_minimum,  30/2.2
    posicion_central = 94

    ix  =  arange(round(630/2.2),  round(690/2.2))  # 1, 50
    num_minimum = 14
    ix_central = round(posicion_central/2.2*num_minimum)
    i_anchura = round(20/2.2)
    ix  =  arange(ix_central-i_anchura, ix_central+i_anchura)

    period = zeros_like(z)
    period_interpolado = zeros_like(z)

    plt.figure()
    print range(num_z)
    for i in range(num_z):
        print i
        profile = squeeze( I[i, :])
        points  =  data1D(x  =  x,  y  =  profile)

        var  =  variogram(data  =  points)
        var.compute_experimental(ipoints  =  ix,  recortarmaximum  =  False)
        # var.draw(kind  =  'experimental')

        h = var.varExp.x
        v = var.varExp.y
        n_points = var.varExp.num_data

        print "aqui",  len(h),  len(v),  len(n_points)
        print n_points
        plt.plot(h, v)
        plt.title('variogram_minimum %2.0f' %num_minimum, fontsize = 24)

        iminimum =  v.argmin()
        period[i] = h[iminimum]/num_minimum # *1000

        period_interpolado[i] = ajuste_minimum(h, v)/num_minimum # *1000

        print(period[i],  period_interpolado[i])

    plt.figure()
    plt.plot(z, period, 'ko',  ms = 10)
    plt.plot(z, period_interpolado, 'r',  lw = 2)
    plt.title('sin interpolation de los minimums', fontsize = 24)
    return 0
"""


def ajuste_polinomico(z, periods):
    coefs = sp.polyfit(z, periods, 1)
    print "coeficientes interpolation", coefs
    p_lineal = sp.polyval(coefs, z)
    residuos = (period_interpolado - p_lineal)

    return p_lineal, residuos


def ajuste_curva(x, y, func, confidence_interval=0.95):
    # Define confidence interval.
    ci = confidence_interval
    # Convert to percentile point of the normal distribution.
    # See: https://en.wikipedia.org/wiki/Standard_score
    pp = (1. + ci) / 2.
    # Convert to number of standard deviations.
    nstd = stats.norm.ppf(pp)
    print nstd

    # Find best fit.
    popt, pcov = curve_fit(func, x, y)
    # Standard deviation errors on the parameters.

    perr = sp.sqrt(sp.diag(pcov))
    # Add nstd standard deviations to parameters to obtain the upper confidence
    # interval.
    popt_up = popt + nstd * perr
    popt_dw = popt - nstd * perr

    return popt, popt_up, popt_dw, pcov


if __name__ == '__main__':
    print "hola"
    # filename_1 = archivos[0]
    # visualizar_variogram(filename_1, todos_variograms = False)
    # calculo_period_sin_mover(x, z,  I)
    # example_moviendo()
    # calculo_period_pendiente()
    plt.show()
