#!/usr/bin/env python
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------
# Name:		KrigingEstandard.py
# Purpose:	 Genera la estimacion del kriging estandard
#
# Author:	  Luis Miguel Sanchez Brea
#
# Created:	 11/08/04
# RCS-ID:
# Copyright:
# Licence:	 GPL
#----------------------------------------------------------------------
"""
Clases Scalar_field_XY y Fuentes2D con las fuentes utilizadas para propagación
"""

import numpy as np
import scipy as sp
import matplotlib.pyplot as plt

from phyton_kriging.variogram1D import *
from data_generator.data1D import data1D

um = 1
mm = 1000 * um
degrees = sp.pi / 180


class kriging1D(object):

    def __init__(self, sensors, variogram, sampling):
        """
		Inputs:
		* sensors: posiciones donde estan los sensors
		* varigorama: function variogram
		* mobservacion: matrix con los points de observacion
		Aqui solamente se verifica que las entradas son correctas
		"""
        self.sensors = sensors
        self.variogram = variogram
        self.sampling = sampling
        self.kriging = data1D(x=self.sampling.x,
                              y=sp.zeros(self.sampling.x.shape))

    def krigingEstandard(
            self,
            filtrado=True
    ):  #este no sigue al noise, pero va a toda pastilla...
        """
		Krigeado Estandard

		Inputs:
		* filtrado: Si true se aplica la operacion LM para poder filtrar (tesis y articulo de AO)
					Si False segun libros standard de kriging

		Outputs:
		* lambdas
		* interpolación
		* error
		"""

        #1. Detecta dimensión de los data
        #2. Calcula los values del kriging estándar
        #3. Guarda los data en data1D o data2D

        xsensors = self.sensors.x
        xsampling = self.sampling.x

        XS1, XS2 = sp.meshgrid(xsensors, xsensors)
        XS, XM = sp.meshgrid(xsampling, xsensors)

        #distance entre sensors
        distSensores = abs(XS2 - XS1)
        #distances entre sensors y points de sampling
        distSensoresMuestreo = abs(XS - XM)

        #matrices principales
        Gamma = sp.mat(self.variogram.f(distSensores))
        gamma = sp.mat(self.variogram.f(distSensoresMuestreo))

        #		x = sp.linspace(0, 3, 1000)
        #		y = self.variogram.f(x)
        #		temp = data1D(x=x, y=y, title='prueba')
        #		temp.draw()

        #Modificaciones LM según artículo AO
        if filtrado == True:
            s0 = self.variogram.f(0.)
            I1, I2 = sp.meshgrid(self.sensors.I, self.sensors.I)

            gamma = gamma - s0 / 2
            Gamma = Gamma - I1 * I2
            for i in range(len(self.sensors.x)):
                Gamma[i, i] = -self.sensors.I[i]**2

        if filtrado == False:  #según los libros
            for i in range(len(self.sensors.x)):
                Gamma[i, i] = 0

        #definición de matrices
        U = sp.mat(sp.ones((len(xsensors), 1), dtype='float'))
        u = sp.mat(sp.ones((len(xsampling), 1), dtype='float'))
        Ut = np.transpose(U)
        ut = np.transpose(u)
        invGamma = Gamma.I

        g = (ut - Ut * invGamma * gamma)
        h = (Ut * invGamma * U).I
        self.lambdas = np.array(sp.transpose(gamma + U * h * g) * invGamma)
        error2 = sp.squeeze(
            sp.diag(
                sp.transpose(gamma) * invGamma * gamma -
                sp.transpose(g) * h * g))
        self.error = np.array(sp.sqrt(error2))
        self.estimacion = sp.squeeze(
            np.array(self.lambdas * sp.transpose(sp.mat(self.sensors.y))))

        #salida de la función, devuelve data1D o data2D según sea la entrada
        #Esto último hay que verificar
        self.kriging.y = self.estimacion  # array de points con los values
        self.kriging.I = self.sensors.I  # array de points con la precision de los aparatos de medida
        self.kriging.sc = sp.sqrt(
            self.variogram.f(0.)
        )  #self.error					  # array de points con las fluctuaciones aleatorias
        self.kriging.yideal = sp.zeros(
            self.sampling.x.shape
        )  # array de points con el value "ideal" de la function
        self.kriging.error = self.error
        self.kriging.NEQ = self.kriging.sc**2 / (
            self.kriging.error**2 - self.kriging.I[0]**2
        )  #aquí pongo el I[0], pero deberían ser todos


#		print "NEQ=", self.kriging.NEQ
#		print "noise=", self.kriging.noise
#		print "sc ", self.kriging.sc
#		print "I ", self.kriging.I[0]

    def center_lambdas(self):
        """
		Esta función toma varias functiones lambdas
		y las traslada colocando los máximos en el centro
		Los que falta de la función (al ser trasladada hay partes
		no definidas) las rellena de Nulos
		"""
        m = self.sampling
        k = self.kriging
        s = self.sensors

        incr_x = m.x[1] - m.x[0]
        distance = (m.x[-1] - m.x[0]) / 2
        long_sampling = len(m.x)
        xmin = m.x[0] - 2 * distance
        xmax = m.x[-1] + 2 * distance
        X = sp.arange(xmin, xmax, incr_x)

        #maximum_lambdas = self.lambdas.max()
        pos_max = self.lambdas.argmax(axis=0)

        LAMBDAS = sp.zeros((len(X), len(s.x)), float) / 0  #llleno de nans
        #print incr_x, distance, xmin, xmax, X.shape
        #print "LAMBDAS:", LAMBDAS.shape
        for i in range(len(s.x)):
            #pos_inicial = m.x[0]-m.x[pos_max[i]]
            #print i, k.lambdas[:,i].shape, pos_max[i],pos_max[i]+long_sampling
            #plt.plot(m.x-m.x[pos_max[i]],k.lambdas[:,i])
            pos_ini = round(len(X) / 2) - pos_max[i]
            #print pos_ini, len(X), pos_max[i]
            #if k.lambdas[:,i].max() > 0.5*maximum_lambdas:
            LAMBDAS[pos_ini:pos_ini + long_sampling, i] = self.lambdas[:, i]

        lambda_media = sp.nanmean(LAMBDAS, axis=1)

        return X, lambda_media, LAMBDAS

    def get(self, kind):
        xm = self.sampling.x

        title = '$kriging\,estandar$'

        if kind == 'lambda':
            output = data1D(x=xm - xm.mean(),
                            y=self.lambdas,
                            xtext="$x(mm)$",
                            ytext="$\Lambda(x)$",
                            title=title)

        if kind == 'estimacion':
            output = data1D(x=xm,
                            y=self.estimacion,
                            xtext="$x(mm)$",
                            ytext="$estimacion(x)$",
                            title=title)

        if kind == 'error':
            output = data1D(x=xm,
                            y=self.error,
                            xtext="$x(mm)$",
                            ytext="$error(x)$",
                            title=title)

        if kind == 'errorReal':
            output = data1D(x=xm,
                            y=abs(self.sampling.y - self.estimacion),
                            xtext="$x(mm)$",
                            ytext="$error(x)$",
                            title=title)

        if kind == 'NEQ':
            output = data1D(x=xm,
                            y=abs(self.kriging.NEQ),
                            xtext="$x(mm)$",
                            ytext="$NEQ(x)$",
                            title=title)

        return output

    def draw(self, kind='kriging', num_lambda=all):
        """
		Dibuja las diferentes señales del kriging: señal, variogram, lambdas, error, kriging
		"""

        if kind == 'señal':
            self.sensors.draw(yideal=True)

        if kind == 'variogram':
            self.variogram.draw(kind='ambos')
            plt.ylim(ymin=0)

        if kind == 'lambda':
            output = self.get('lambda')
            output.draw()

        if kind == 'kriging':
            self.sensors.draw(yideal=True,
                              kind='ko',
                              label='data',
                              linewidth=2)
            plt.title("")
            #plt.plot(self.sensors.x, self.sensors.yideal,'r',lw=2, label='ideal')
            #plt.plot(self.sensors.x, self.sensors.y,'bo', lw=2, label='data')
            plt.plot(self.kriging.x,
                     self.kriging.y,
                     'k',
                     lw=2,
                     label='kriging')
            plt.plot(self.kriging.x,
                     self.kriging.y + self.error,
                     'k--',
                     lw=2,
                     label='error')
            plt.plot(self.kriging.x, self.kriging.y - self.error, 'k--', lw=2)
            plt.legend()

        if kind == 'error':
            output = self.get('error')
            output.draw(label='estimado')
            #plt.figure()
            #plt.plot(self.sampling.x, self.error, 'k', lw=3, label='error estimado')
            plt.plot(self.sensors.x,
                     abs(self.sensors.yideal - self.sensors.y),
                     'ko',
                     lw=2,
                     label='data')
            plt.plot(self.sampling.x,
                     abs(self.sampling.y - self.estimacion),
                     'k--',
                     lw=2,
                     label='real')
            plt.legend()
            plt.ylim(ymin=0)

        if kind == 'NEQ':
            output = self.get('NEQ')
            output.draw(label='NEQ')
            plt.ylim(ymin=0)

        if kind == 'all':
            self.draw(kind='señal')
            self.draw(kind='variogram')
            self.draw(kind='lambdas')
            self.draw(kind='kriging')
            self.draw(kind='error')
            self.draw(kind='NEQ')
