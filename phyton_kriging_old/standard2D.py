#!/usr/bin/env python
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------
# Name:		krigStandard2D.py
# Purpose:	 Genera la interpolation del kriging estandard 2D
#
# Author:	  Luis Miguel Sanchez Brea
#
# Created:	 11/12/09
# RCS-ID:
# Copyright:
# Licence:	 GPL
#----------------------------------------------------------------------
"""
Clases Scalar_field_XY y Fuentes2D con las fuentes utilizadas para propagación
"""

import numpy as np
import scipy as sp
import matplotlib.pyplot as plt

#from general.funcs_varias import ndgrid
from phyton_kriging.variogram2D_radial import variogram_gauss, variogram2D_radial
from data_generator.data2D import data2D
from data_generator.dataMatrix import dataMatrix

um = 1
mm = 1000 * um
degrees = sp.pi / 180


def update(val):
    global LAMBDAS, ax1

    iz = int(sS.val)

    ax1.imshow(sp.squeeze(LAMBDAS[:, :, iz]))
    #l2b.set_ydata(sp.squeeze(LAMBDAS[:,:,sc]))
    plt.draw()


class kriging2D(object):
    #s, m, v = functionEjemplo2D(A=1, p=1, noise=0.25, I0=0.01)

    def __init__(self, sensors, variogram, sampling):
        """
		Inputs:
		* sensors: posiciones donde estan los sensors
		* varigorama: function variogram
		* mobservacion: matrix con los points de observacion
		Aqui solamente se verifica que las entradas son correctas
		"""
        self.s = sensors
        self.v = variogram
        self.m = sampling
        self.k = dataMatrix(x=self.m.x,
                            y=self.m.y,
                            xtext=r"x(mm)",
                            ytext=r"y(mm)",
                            title=r"Z_{kriging}(x,y)")

    def krigStandard2D(self,
                       filtrado=True
                       ):  #este no sigue al noise, pero va a toda pastilla...
        """
		krigStandard2D

		Inputs:
		* filtrado: Si true se aplica la operacion LM para poder filtrar (tesis y articulo de AO)
					Si False segun libros standard de kriging

		Outputs:
		* lambdas
		* interpolación
		* error
		"""

        #1. Detecta dimensión de los data
        #2. Calcula los values del kriging estándar
        #3. Guarda los data en data1D o data2D

        xsensors = self.s.x
        xsampling = self.m.X.flatten()

        ysensors = self.s.y
        ysampling = self.m.Y.flatten()

        XS1, XS2 = sp.meshgrid(xsensors, xsensors)
        XS, XM = sp.meshgrid(xsampling, xsensors)

        YS1, YS2 = sp.meshgrid(ysensors, ysensors)
        YS, YM = sp.meshgrid(ysampling, ysensors)

        #distance entre sensors
        distSensores = sp.sqrt((XS2 - XS1)**2 + (YS2 - YS1)**2)
        #distances entre sensors y points de sampling
        distSensoresMuestreo = sp.sqrt((XS - XM)**2 + (YS - YM)**2)

        #matrices principales
        Gamma = sp.mat(self.v.f(distSensores))
        gamma = sp.mat(self.v.f(distSensoresMuestreo))

        #Modificaciones LM según artículo AO
        if filtrado == True:
            s0 = self.v.f(0.)
            I1, I2 = sp.meshgrid(self.s.I, self.s.I)

            gamma = gamma - s0 / 2
            Gamma = Gamma - I1 * I2
            for i in range(len(self.s.x)):
                Gamma[i, i] = -self.s.I[i]**2

        if filtrado == False:  #según los libros
            for i in range(len(self.s.x)):
                Gamma[i, i] = 0

        #definición de matrices
        U = sp.mat(sp.ones((len(xsensors), 1), dtype='float'))
        u = sp.mat(sp.ones((len(xsampling), 1), dtype='float'))
        Ut = np.transpose(U)
        ut = np.transpose(u)
        invGamma = Gamma.I

        g = (ut - Ut * invGamma * gamma)
        h = (Ut * invGamma * U).I
        lambdas = sp.mat(sp.transpose(gamma + U * h * g) * invGamma)

        error2 = sp.squeeze(
            sp.diag(
                sp.transpose(gamma) * invGamma * gamma -
                sp.transpose(g) * h * g))
        self.error = np.array(sp.sqrt(error2))
        self.interpolation = sp.squeeze(
            np.array(lambdas * sp.transpose(sp.mat(self.s.z))))

        self.error = sp.transpose(
            self.error.reshape(len(self.m.x), len(self.m.y)))
        self.interpolation = sp.transpose(
            self.interpolation.reshape(len(self.m.x), len(self.m.y)))

        lambdas = sp.array(lambdas)
        self.lambdas = lambdas.reshape(
            (len(self.m.x), len(self.m.y), len(self.s.x)))

        #plt.figure()
        #plt.imshow(self.lambdas[:,:,len(self.s.x)/2])
        #plt.colorbar()

        #Esto último hay que verificar
        self.k.Z = self.interpolation  # array de points con los values
        self.k.I = sp.zeros(
            self.m.x.shape
        )  # array de points con la precision de los aparatos de medida
        self.k.sc = self.error  # array de points con las fluctuaciones aleatorias
        self.k.yideal = sp.zeros(
            self.m.x.shape
        )  # array de points con el value "ideal" de la function

    def krigStandard2D_no_uniforme(
            self,
            filtrado=True
    ):  #este no sigue al noise, pero va a toda pastilla...
        """
		160115 - lo hago porque a veces los data de salida no tienen por que estar equimuestreados
		krigStandard2D

		Inputs:
		* filtrado: Si true se aplica la operacion LM para poder filtrar (tesis y articulo de AO)
					Si False segun libros standard de kriging

		Outputs:
		* lambdas
		* interpolación
		* error
		"""

        #1. Detecta dimensión de los data
        #2. Calcula los values del kriging estándar
        #3. Guarda los data en data1D o data2D

        xsensors = self.s.x
        xsampling = self.m.X

        ysensors = self.s.y
        ysampling = self.m.Y

        XS1, XS2 = sp.meshgrid(xsensors, xsensors)
        XS, XM = sp.meshgrid(xsampling, xsensors)

        YS1, YS2 = sp.meshgrid(ysensors, ysensors)
        YS, YM = sp.meshgrid(ysampling, ysensors)

        #distance entre sensors
        distSensores = sp.sqrt((XS2 - XS1)**2 + (YS2 - YS1)**2)
        #distances entre sensors y points de sampling
        distSensoresMuestreo = sp.sqrt((XS - XM)**2 + (YS - YM)**2)

        #matrices principales
        Gamma = sp.mat(self.v.f(distSensores))
        gamma = sp.mat(self.v.f(distSensoresMuestreo))

        #Modificaciones LM según artículo AO
        if filtrado == True:
            s0 = self.v.f(0.)
            I1, I2 = sp.meshgrid(self.s.I, self.s.I)

            gamma = gamma - s0 / 2
            Gamma = Gamma - I1 * I2
            for i in range(len(self.s.x)):
                Gamma[i, i] = -self.s.I[i]**2

        if filtrado == False:  #según los libros
            for i in range(len(self.s.x)):
                Gamma[i, i] = 0

        #definición de matrices
        U = sp.mat(sp.ones((len(xsensors), 1), dtype='float'))
        u = sp.mat(sp.ones((len(xsampling), 1), dtype='float'))
        Ut = np.transpose(U)
        ut = np.transpose(u)
        invGamma = Gamma.I

        g = (ut - Ut * invGamma * gamma)
        h = (Ut * invGamma * U).I
        lambdas = sp.mat(sp.transpose(gamma + U * h * g) * invGamma)

        error2 = sp.squeeze(
            sp.diag(
                sp.transpose(gamma) * invGamma * gamma -
                sp.transpose(g) * h * g))
        self.error = np.array(sp.sqrt(error2))
        self.interpolation = sp.squeeze(
            np.array(lambdas * sp.transpose(sp.mat(self.s.z))))

        self.error = sp.transpose(self.error)
        self.interpolation = sp.transpose(self.interpolation)

        lambdas = sp.array(lambdas)
        self.lambdas = lambdas

        #plt.figure()
        #plt.imshow(self.lambdas[:,:,len(self.s.x)/2])
        #plt.colorbar()

        #Esto último hay que verificar
        self.k.Z = self.interpolation  # array de points con los values
        self.k.I = sp.zeros(
            self.m.x.shape
        )  # array de points con la precision de los aparatos de medida
        self.k.sc = self.error  # array de points con las fluctuaciones aleatorias
        self.k.yideal = sp.zeros(
            self.m.x.shape
        )  # array de points con el value "ideal" de la function

    def center_lambdas(self):
        """
		Coge las functiones lambdas obtenidas en el kriging estándar y las centra y promedia
		"""
        """
				%[Colocadas]=centrar(x,Y)
		%__________________________________________________
		%LUIS MIGUEL SÁNCHEZ BREA 4/2/98
		%Esta función toma varias functiones (x,Y[NMedidas,NDatos->Puntos de estimacion])
		% y las traslada colocando los máximos en el centro
		% Los que falta de la función (al ser trasladada hay partes
		% no definidas) las rellena de Nulos
		%__________________________________________________

		function lambdaCentrada=centrarlambda(x,Y)

		NDatos=length(Y);
		[maximum,PosMax]=max(Y);
		PosMedia=round((NDatos-1)/2);
		Diferencia=PosMax-PosMedia;

		Pos=[1:length(x)]';

		Posx=Pos-Diferencia;

		%Busca donde están colocados los unos y el final
		final=Posx(min(NDatos,length(Posx)));
		lambdaCentrada=0*ones(size(Y));

		   if Diferencia>0
			  s=Posx(:,1);
			  uno=find(s==1);
			  lambdaCentrada([1:final-1])=Y([uno:final+uno-2]);
		   else
			  lambdaCentrada([-Diferencia+1:NDatos])=Y([1:min(length(Y),NDatos+Diferencia)]);
		   end
		"""
        pass

    def draw(self, kind='kriging', circles=True, clim=None):
        """
		Dibuja las diferentes señales del kriging: señal, variogram, lambdas, error, kriging
		"""

        if kind == 'data':
            self.s.draw()

        if kind == 'ideal':
            plt.figure(figsize=(8, 8))
            plt.imshow(self.Zideal,
                       interpolation='bilinear',
                       aspect='equal',
                       origin='lower',
                       extent=[
                           self.x.min(),
                           self.x.max(),
                           self.y.min(),
                           self.y.max()
                       ])
            plt.xlim(xmin=self.k.x.min(), xmax=self.k.x.max())
            plt.ylim(ymin=self.k.y.min(), ymax=self.k.y.max())
            if clim == None:
                maximum = max(sp.ravel(self.Zideal))
                minimum = min(sp.ravel(self.Zideal))
                plt.clim(maximum, minimum)
            else:
                plt.clim(clim[0], clim[1])

            plt.xlabel(self.xtext, fontsize=20)
            plt.ylabel(self.ytext, fontsize=20)
            plt.title(r"$Z_{ideal}(x,y)$", fontsize=26)
            plt.colorbar(cax=None, orientation='horizontal')
            if circles == True:
                plt.scatter(self.s.x, self.s.y, c='w',
                            s=35)  #,c=colors,s=length

        if kind == 'variogram':
            self.v.draw(kind='teorico')

        if kind == 'lambdas':
            global LAMBDAS, ax1, sS

            LAMBDAS = self.lambdas

            #print LAMBDAS.shape

            maximum = max(sp.ravel(LAMBDAS))
            minimum = min(sp.ravel(LAMBDAS))

            plt.figure(figsize=(8, 8))
            ax1 = plt.subplot(111)
            plt.subplots_adjust(left=0.15, bottom=0.25)
            plt.title('$\lambda(x)$')

            plt.imshow(sp.squeeze(LAMBDAS[:, :, 1]),
                       interpolation='bilinear',
                       aspect='equal',
                       origin='lower',
                       extent=[
                           self.k.x.min(),
                           self.k.x.max(),
                           self.k.y.min(),
                           self.k.y.max()
                       ])
            plt.xlim(xmin=self.k.x.min(), xmax=self.k.x.max())
            plt.ylim(ymin=self.k.y.min(), ymax=self.k.y.max())
            plt.colorbar()

            axcolor = 'lightgoldenrodyellow'
            axS = plt.axes([0.15, 0.15, 0.75, 0.03], axisbg=axcolor)
            plt.clim(vmin=minimum, vmax=maximum)

            sS = plt.Slider(axS, 'num sensor:', 0, len(self.s.x), valinit=0)
            sS.on_changed(update)

            #plt.show()

        if kind == 'error':
            plt.figure(figsize=(8, 8))
            plt.imshow(self.error,
                       interpolation='bilinear',
                       aspect='equal',
                       origin='lower',
                       extent=[
                           self.k.x.min(),
                           self.k.x.max(),
                           self.k.y.min(),
                           self.k.y.max()
                       ])
            plt.xlim(xmin=self.k.x.min(), xmax=self.k.x.max())
            plt.ylim(ymin=self.k.y.min(), ymax=self.k.y.max())

            plt.xlabel(self.k.xtext, fontsize=24)
            plt.ylabel(self.k.ytext, fontsize=24)

            if clim == None:
                maximum = sp.nanmax(sp.ravel(self.error))
                minimum = sp.nanmin(sp.ravel(self.error))
                plt.clim(minimum, maximum)
            else:
                plt.clim(clim[0], clim[1])

            plt.title(r"$error kriging(x,y)$", fontsize=26)
            plt.colorbar(cax=None, orientation='horizontal')
            if circles == True:
                plt.scatter(self.s.x, self.s.y, c='w',
                            s=35)  #,c=colors,s=length

        if kind == 'kriging':
            plt.figure(figsize=(8, 8))
            plt.imshow(self.interpolation,
                       interpolation='bilinear',
                       aspect='equal',
                       origin='lower',
                       extent=[
                           self.k.x.min(),
                           self.k.x.max(),
                           self.k.y.min(),
                           self.k.y.max()
                       ])
            plt.xlim(xmin=self.k.x.min(), xmax=self.k.x.max())
            plt.ylim(ymin=self.k.y.min(), ymax=self.k.y.max())

            plt.xlabel(self.k.xtext, fontsize=20)
            plt.ylabel(self.k.ytext, fontsize=20)

            if clim == None:
                pass
            else:
                plt.clim(clim[0], clim[1])

            plt.title(r"$Z_{kriging}(x,y)$", fontsize=26)
            plt.colorbar(cax=None, orientation='horizontal')
            if circles == True:
                plt.scatter(self.s.x, self.s.y, c='w',
                            s=35)  #,c=colors,s=length
