#!/usr/bin/env python
# -*- coding: utf-8 -*-
#------------------------------------
# Author:	Luis Miguel Sanchez Brea
# Fecha		2013/10/24 (version 1.0)
# License:	GPL
# Purpose: Tests para la optimización
#-------------------------------------

from optimizacion_clase import * 
import datetime
from phyton_optics.scalar_masks_XY import Scalar_mask_XY

directorio = 'videos_prueba_optimizacion/'

import sys

def test_circle_10_sensors():
	"""
	sacamos 10 sensors a la vez sobre un circle y lo sacamos
	"""

	#===============
	# Inicialización
	#================
	xlim = (-5 * mm, 5 * mm)
	ylim = (-5 * mm, 5 * mm)

	s = 0.25
	I0 = 0.01
	error_umbral = 1
	num_sensors = 10

	#zona de sampling
	n_sampling = 64
	xsampling = sp.linspace(xlim[0], xlim[1], n_sampling)
	ysampling = sp.linspace(ylim[0], ylim[1], n_sampling)

	#image
	image = '../imagees/circle.png'
	image = '../imagees/edificio5.png'

	angle_image = 0 * degrees
	m = dataMatrix(x = xsampling, y = ysampling, matrix = [], xtext = r"x(mm)", ytext = r"y(mm)", title = r"sampling")
	m.image(filename_1 = image, normalize = True, canal = 0, lengthImage = False, inverse = True, angle = angle_image) #no hace falta, pero por si acaso

	#zona donde se deben optimizar la función
	zona_optimizacion = dataMatrix(x = xsampling, y = ysampling, matrix = [], xtext = r"x(mm)", ytext = r"y(mm)", title = r"zona optimizacion")
	zona_optimizacion.image(filename_1 = image, normalize = True, canal = 0, lengthImage = False, inverse = True, angle = angle_image)

	#calculo del variogram
	v = variogram()
	v.establecer_parametros(x = sp.linspace(0, 5, 100), s0 = s, c0 = 1, l0 = 2 * mm, f_texto = "s0**2+c0**2*(1-sp.exp(-(x/l0)**2))")
	#v.draw()

	#generar DM y otras functiones
	kconv2 = krigingConvolucion2D(sensors = None, variogram = v, sampling = m, kind = 'moderno')

	#determinación del número de sensors
	area = sum(sp.ravel(zona_optimizacion.Z)) #en uds de pixeles
	print "Area: ", area
	print "num pixeles: ", len(zona_optimizacion.x) * len(zona_optimizacion.y)
	area_porcentaje = area / (len(zona_optimizacion.x) * len(zona_optimizacion.y))
	print "area_porcentaje: ", area_porcentaje

	area_cobertura_1sensor = (error_umbral ** 2 - I0 ** 2) * sum(sp.ravel(kconv2.DM)) / s ** 2
	print "area_cobertura_1sensor: ", area_cobertura_1sensor

	#Ubicación inicial de los sensors
	sensors = data2D(xtext = "x(mm)", ytext = "y(mm)", ztext = "z(mm)", title = "posicion inicial")
	sensors.senal_aleatoria_image(xlim = xlim, ylim = ylim, image = image, angle = angle_image, num_data = num_sensors, invert = True, f = 'x')
	#sensors.senal_helice(num_points = num_sensors, abertura = 5 * mm, potencia = 0.25, initial_phase = 0, nvueltas = 1)
	#sensors.regular_signal(xlim = xlim, ylim = ylim, num_data = (3, 3), f = '0*(x+y)')
	#sensors.generarSenalPeriodica(xlim = (-5 * mm, 5 * mm), ylim = (-5 * mm, 5 * mm), pos_inicial = (-10 * mm, -10 * mm), u = (1 * mm, 0 * mm) , v = (sp.cos(120 * degrees) * mm, sp.sin(120 * degrees) * mm), f = '0*(x+y)')
	#sensors.mask2(matrixXY = zona_optimizacion)
	sensors.add_noise(sc = (s,), I = (I0,))
	sensors.draw()
	plt.show()

	#calculo inicial del error
	kconv2.computeError(sensors = sensors)
	kconv2.error = kconv2.error * zona_optimizacion.Z
	kconv2.draw(kind = 'error', circles = True, clim = None)

	#=============
	# Optimización
	#=============

	num_pasos = 100

	kappa = 4

	e_maxima = 			0.0020
	e_minima = 			0.0001

	max_aleat = 		0.00000002
	paso_aleat_final = round(num_pasos / 2)

	max_separation = 	0.00002
	paso_separation_final = round(num_pasos)

	sensor_new_cada = 50000

	param_optim = {
		'numPasos':			num_pasos,
		'kappa':  			kappa, 			#2, 4
		}

	param_optim['anadir_sensor'] = sp.zeros((num_pasos,), dtype = bool)
	param_optim['anadir_sensor'][sensor_new_cada::sensor_new_cada] = True

	param_optim['energia'] = sp.linspace(e_maxima, e_minima, num_pasos) #porcentaje de aproximacion entre x0 y x1 para cambiar el point

	param_optim['aleat'] = sp.zeros((num_pasos,), dtype = 'float')
	param_optim['aleat'][0:paso_aleat_final] = sp.linspace(max_aleat, 0., paso_aleat_final)

	param_optim['separation'] = sp.zeros((num_pasos,), dtype = 'float')
	param_optim['separation'][0:paso_separation_final] = sp.linspace(max_separation, max_separation, paso_separation_final)

	param_visualizacion = {
		'guardar_drawing': sp.zeros((num_pasos,), dtype = bool),
		'generar_imagees': True,
		'nombre_drawings': 'circle_',
		'draw_cada' : 1000,
		}

	guardar_cada = 1000
	param_visualizacion['guardar_drawing'] = sp.zeros((num_pasos,), dtype = bool)
	param_visualizacion['guardar_drawing'][guardar_cada::guardar_cada ] = True

	#PROCESO DE OPTIMIZACIÓN
	o = optimizacion(zona_optimizacion = zona_optimizacion, variogram = v, sensors = sensors, param_optim = param_optim, param_visualizacion = param_visualizacion, error_umbral = 2, I0 = 0.1)
	o.optimizar()

	#===================
	# Dibujar resultados
	#===================
	zona_optimizacion.draw()
	o.draw_quality()
	o.draw_error(kind = 'vacio')

	now = datetime.datetime.now()
	fecha = now.strftime("%Y-%m-%d_%H_%M")
	nombre_video = directorio + "circle10_sensors_senalregular_" + fecha + ".avi"
	o.video(nombre_video)

	#los sensors están en:
	sensors_news = o.posiciones_sensors(title = 'posicion_final')
	sensors_news.draw()


def test_circle_10_sensors_1en1():
	"""
	sacamos 10 sensors a la vez sobre un circle y lo sacamos
	"""
	#===============
	# Inicialización
	#===============
	xlim = (-5.001 * mm, 5 * mm)
	ylim = (-5.001 * mm, 5 * mm)

	s = 0.25
	I0 = 0.01
	error_umbral = 1
	num_sensors = 2

	#zona de sampling
	n_sampling = 50
	xsampling = sp.linspace(xlim[0], xlim[1], n_sampling)
	ysampling = sp.linspace(ylim[0], ylim[1], n_sampling)

	#image
	image = '../imagees/circle.png'
	angle_image = 0 * degrees
	m = dataMatrix(x = xsampling, y = ysampling, matrix = [], xtext = r"x(mm)", ytext = r"y(mm)", title = r"sampling")
	m.image(filename_1 = image, normalize = True, canal = 0, lengthImage = False, inverse = True, angle = angle_image) #no hace falta, pero por si acaso

	#zona donde se deben optimizar la función
	zona_optimizacion = dataMatrix(x = xsampling, y = ysampling, matrix = [], xtext = r"x(mm)", ytext = r"y(mm)", title = r"zona optimizacion")
	zona_optimizacion.image(filename_1 = image, normalize = True, canal = 0, lengthImage = False, inverse = True, angle = angle_image)

	#calculo del variogram
	v = variogram()
	v.establecer_parametros(x = sp.linspace(0, 5, 100), s0 = s, c0 = 1, l0 = 3 * mm, f_texto = "s0**2+c0**2*(1-sp.exp(-(x/l0)**2))")
	#v.draw()

	#generar DM y otras functiones
	kconv2 = krigingConvolucion2D(sensors = None, variogram = v, sampling = m, kind = 'moderno')

	#determinación del número de sensors
	area = sum(sp.ravel(zona_optimizacion.Z)) #en uds de pixeles
	print "Area: ", area
	print "num pixeles: ", len(zona_optimizacion.x) * len(zona_optimizacion.y)
	area_porcentaje = area / (len(zona_optimizacion.x) * len(zona_optimizacion.y))
	print "area_porcentaje: ", area_porcentaje

	area_cobertura_1sensor = (error_umbral ** 2 - I0 ** 2) * sum(sp.ravel(kconv2.DM)) / s ** 2
	print "area_cobertura_1sensor: ", area_cobertura_1sensor

	#Ubicación inicial de los sensors
	sensors = data2D(xtext = "x(mm)", ytext = "y(mm)", ztext = "z(mm)", title = "posicion inicial")
	sensors.senal_aleatoria_image(xlim = xlim, ylim = ylim, image = image, angle = angle_image, num_data = num_sensors, invert = False, f = 'x')
	#sensors.senal_helice(num_points = num_sensors, abertura = 5 * mm, potencia = 0.25, initial_phase = 0, nvueltas = 1)
	sensors.add_noise(sc = (s,), I = (I0,))
	sensors.draw()

	#calculo inicial del error
	kconv2.computeError(sensors = sensors)
	kconv2.error = kconv2.error * zona_optimizacion.Z
	kconv2.draw(kind = 'error', circles = True, clim = None)

	#===========================================================================
	# Optimización
	#===========================================================================

	sensor_new_cada = 2
	param_optim = {
		'numPasos':100,
		'kappa':  	4., 			#2, 4
		'energia_Maxima':  0.002, 		# 0.02 porcentaje de aproximacion entre x0 y x1 para cambiar el point
		'energia_Minima':  0.0001, 	# 0.025 porcentaje de aproximacion entre x0 y x1 para cambiar el point
		'maxAleat':  .0000006,
		}

	param_optim['anadir_sensor'] = sp.zeros((param_optim['numPasos'],), dtype = bool)
	param_optim['anadir_sensor'][sensor_new_cada::sensor_new_cada] = True

	param_optim['energia_'] = sp.linspace(param_optim['energia_Maxima'], param_optim['energia_Minima'], param_optim['numPasos']) #porcentaje de aproximacion entre x0 y x1 para cambiar el point
	param_optim['aleat'] = sp.zeros((param_optim['numPasos'],), dtype = 'float')
	param_optim['aleat'][0:round(param_optim['numPasos'] / 2)] = sp.linspace(param_optim['maxAleat'], 0., round(param_optim['numPasos'] / 2))


	param_visualizacion = {
		'guardar_drawing': sp.zeros((param_optim['numPasos'],), dtype = bool),
		'generar_imagees': True,
		'nombre_drawings': 'circle_',
		'draw_cada' : 1000,
		}

	guardar_cada = 1000
	param_visualizacion['guardar_drawing'] = sp.zeros((param_optim['numPasos'],), dtype = bool)
	param_visualizacion['guardar_drawing'][guardar_cada::guardar_cada ] = True

	#PROCESO DE OPTIMIZACIÓN
	o = optimizacion(zona_optimizacion = zona_optimizacion, variogram = v, sensors = sensors, param_optim = param_optim, param_visualizacion = param_visualizacion, error_umbral = 2, I0 = 0.1)
	o.optimizar()

	#===========================================================================
	# Dibujar resultados
	#===========================================================================
	zona_optimizacion.draw()
	o.draw_quality()
	o.draw_error(kind = 'vacio')

	now = datetime.datetime.now()
	fecha = now.strftime("%Y-%m-%d_%H_%M")
	nombre_video = directorio + "circle10sensors_1en1_" + fecha + ".avi"
	o.video(nombre_video)

	#los sensors están en:
	sensors_news = o.posiciones_sensors(title = 'posicion_final')
	sensors_news.draw()


def test_circle_100_sensors():
	"""
	sacamos 10 sensors a la vez sobre un circle y lo sacamos
	"""

	#===========================================================================
	# Inicialización
	#===========================================================================
	xlim = (-5 * mm, 5 * mm)
	ylim = (-5 * mm, 5 * mm)

	s = 0.25
	I0 = 0.01
	error_umbral = 2
	num_sensors = 100

	#zona de sampling
	n_sampling = 32
	xsampling = sp.linspace(xlim[0], xlim[1], n_sampling)
	ysampling = sp.linspace(ylim[0], ylim[1], n_sampling)

	#image
	image = '../imagees/circle.png'
	angle_image = 0 * degrees
	m = dataMatrix(x = xsampling, y = ysampling, matrix = [], xtext = r"x(mm)", ytext = r"y(mm)", title = r"sampling")
	m.image(filename_1 = image, normalize = True, canal = 0, lengthImage = False, inverse = True, angle = angle_image) #no hace falta, pero por si acaso

	#zona donde se deben optimizar la función
	zona_optimizacion = dataMatrix(x = xsampling, y = ysampling, matrix = [], xtext = r"x(mm)", ytext = r"y(mm)", title = r"zona optimizacion")
	zona_optimizacion.image(filename_1 = image, normalize = True, canal = 0, lengthImage = False, inverse = True, angle = angle_image)

	#calculo del variogram
	v = variogram()
	v.establecer_parametros(x = sp.linspace(0, 5, 100), s0 = s, c0 = 1, l0 = 5 * mm, f_texto = "s0**2+c0**2*(1-sp.exp(-(x/l0)**2))")
	#v.draw()


	#generar DM y otras functiones
	kconv2 = krigingConvolucion2D(sensors = None, variogram = v, sampling = m, kind = 'moderno')

	#determinación del número de sensors
	area = sum(sp.ravel(zona_optimizacion.Z)) #en uds de pixeles
	print "Area: ", area
	print "num pixeles: ", len(zona_optimizacion.x) * len(zona_optimizacion.y)
	area_porcentaje = area / (len(zona_optimizacion.x) * len(zona_optimizacion.y))
	print "area_porcentaje: ", area_porcentaje

	area_cobertura_1sensor = (error_umbral ** 2 - I0 ** 2) * sum(sp.ravel(kconv2.DM)) / s ** 2
	print "area_cobertura_1sensor: ", area_cobertura_1sensor

	print "num sensors estimado", area / area_cobertura_1sensor


	#Ubicación inicial de los sensors
	sensors = data2D(xtext = "x(mm)", ytext = "y(mm)", ztext = "z(mm)", title = "posicion inicial")
	sensors.senal_aleatoria_image(xlim = xlim, ylim = ylim, image = image, angle = angle_image, num_data = num_sensors, invert = True, f = 'x')
	#sensors.regular_signal(xlim = xlim, ylim = ylim, num_data = (10, 10), f = '0*sp.sqrt(x**2+y**2)')
	#sensors.senal_helice(num_points = num_sensors, abertura = 10 * mm, potencia = 0.25, initial_phase = 0, nvueltas = 1)

	sensors.add_noise(sc = (s,), I = (I0,))
	sensors.draw()

	#calculo inicial del error
	kconv2.computeError(sensors = sensors)
	kconv2.error = kconv2.error * zona_optimizacion.Z
	kconv2.draw(kind = 'error', circles = True, clim = None)


	#===========================================================================
	# Optimización
	#===========================================================================

	sensor_new_cada = 50000
	param_optim = {
		'numPasos':100,
		'kappa':  	4., 			#2, 4
		'energia_Maxima':  0.0005, 		# 0.02 porcentaje de aproximacion entre x0 y x1 para cambiar el point
		'energia_Minima':  0.0001, 	# 0.025 porcentaje de aproximacion entre x0 y x1 para cambiar el point
		'maxAleat':  .0002,
		}

	param_optim['anadir_sensor'] = sp.zeros((param_optim['numPasos'],), dtype = bool)
	param_optim['anadir_sensor'][sensor_new_cada::sensor_new_cada] = True
	param_optim['energia_'] = sp.linspace(param_optim['energia_Maxima'], param_optim['energia_Minima'], param_optim['numPasos']) #porcentaje de aproximacion entre x0 y x1 para cambiar el point
	param_optim['aleat'] = sp.zeros((param_optim['numPasos'],), dtype = 'float')
	param_optim['aleat'][0:round(param_optim['numPasos'] / 2)] = sp.linspace(param_optim['maxAleat'], 0., round(param_optim['numPasos'] / 2))

	param_visualizacion = {
		'guardar_drawing': sp.zeros((param_optim['numPasos'],), dtype = bool),
		'generar_imagees': True,
		'nombre_drawings': 'circle_',
		'draw_cada' : 1000,
		}

	guardar_cada = 1000
	param_visualizacion['guardar_drawing'] = sp.zeros((param_optim['numPasos'],), dtype = bool)
	param_visualizacion['guardar_drawing'][guardar_cada::guardar_cada ] = True


	#PROCESO DE OPTIMIZACIÓN
	o = optimizacion(zona_optimizacion = zona_optimizacion, variogram = v, sensors = sensors, param_optim = param_optim, param_visualizacion = param_visualizacion, error_umbral = error_umbral, I0 = I0)
	o.optimizar()

	#===========================================================================
	# Dibujar resultados
	#===========================================================================
	zona_optimizacion.draw()
	o.draw_quality()
	o.draw_error(kind = 'vacio')

	now = datetime.datetime.now()
	fecha = now.strftime("%Y-%m-%d_%H_%M")
	nombre_video = directorio + "circle100sensors_" + fecha + ".avi"
	o.video(nombre_video)

	#los sensors están en:
	sensors_news = o.posiciones_sensors(title = 'posicion_final')
	sensors_news.draw()


def test_optimizacion_1():
	"voy expulsando un sensor cada x pasos y estos se van ubicando en un ring"

	#Generación de la población inicial
	s = 0.25
	I0 = 0.01
	xlim = (-5 * mm, 5 * mm)
	ylim = (-5 * mm, 5 * mm)

	image = '../../imagees/ring.png'
	angle_image = 0 * degrees

	error_umbral = .25

	#sampling
	n_sampling = 32
	xsampling = sp.linspace(xlim[0], xlim[1], n_sampling)
	ysampling = sp.linspace(ylim[0], ylim[1], n_sampling)

	m = dataMatrix(x = xsampling, y = ysampling, matrix = [], xtext = r"x(mm)", ytext = r"y(mm)", title = r"sampling")
	m.image(filename_1 = image, normalize = True, canal = 0, lengthImage = False, inverse = True, angle = angle_image) #no hace falta, pero por si acaso

	"""
	#zona donde se pueden ubicar los sensors
	zona_sensors=dataMatrix(x=xsampling, y=ysampling, matrix=[], xtext=r"x(mm)", ytext=r"y(mm)", title=r"zona sensors")
	zona_sensors.image(filename_1=image, normalize=True, canal=0, lengthImage=False, inverse=False, angle=angle_image)
	zona_sensors.draw()
	"""

	#zona donde se deben optimizar la función
	zona_optimizacion = dataMatrix(x = xsampling, y = ysampling, matrix = [], xtext = r"x(mm)", ytext = r"y(mm)", title = r"zona optimizacion")
	zona_optimizacion.image(filename_1 = image, normalize = True, canal = 0, lengthImage = False, inverse = True, angle = angle_image)
	zona_optimizacion.draw()


	#calculo del variogram
	v = variogram()
	v.establecer_parametros(x = sp.linspace(0, 5, 100), s0 = s, c0 = 1, l0 = .25 * mm, f_texto = "s0**2+c0**2*(1-sp.exp(-(x/l0)**2))")
	#v.draw()

	#generar DM y otras functiones
	kconv2 = krigingConvolucion2D(sensors = None, variogram = v, sampling = m, kind = 'moderno')

	#determinación del número de sensors
	area = sum(sp.ravel(zona_optimizacion.Z)) #en uds de pixeles
	print "Area: ", area
	print "num pixeles: ", len(zona_optimizacion.x) * len(zona_optimizacion.y)
	area_porcentaje = area / (len(zona_optimizacion.x) * len(zona_optimizacion.y))
	print "area_porcentaje: ", area_porcentaje

	area_cobertura_1sensor = (error_umbral ** 2 - I0 ** 2) * sum(sp.ravel(kconv2.DM)) / s ** 2
	print "area_cobertura_1sensor: ", area_cobertura_1sensor

	num_sensors = 4 * int(area / area_cobertura_1sensor) #el 4 es porque yo lo valgo
	print "num_sensors: ", num_sensors
	num_sensors = 10

	#Ubicación inicial de los sensors
	sensors = data2D(xtext = "x(mm)", ytext = "y(mm)", ztext = "z(mm)", title = "posicion inicial")
	sensors.senal_aleatoria_image(xlim = xlim, ylim = ylim, image = image, angle = angle_image, num_data = num_sensors, invert = True, f = 'x')
	sensors.add_noise(sc = (s,), I = (I0,))
	sensors.draw()

	#calculo inicial del error
	kconv2.computeError(sensors = sensors)

	"""
	kconv2.error=kconv2.error
	kconv2.draw(kind='error', circles=True, clim=None)
	kconv2.error=kconv2.error*zona_optimizacion.Z
	kconv2.draw(kind='error', circles=True, clim=None)
	"""

	sensor_new_cada = 5


	#parametros del codigo de optimizacion
	param_optim = {
		'numPasos':60,
		'kappa':  	4., 			#2, 4
		'energia_Maxima':  0.2, 		# 0.02 porcentaje de aproximacion entre x0 y x1 para cambiar el point
		'energia_Minima':  0.04, 	# 0.025 porcentaje de aproximacion entre x0 y x1 para cambiar el point
		'maxAleat':  .25,
		}

	param_optim['anadir_sensor'] = sp.zeros((param_optim['numPasos'],), dtype = bool)
	param_optim['anadir_sensor'][0::sensor_new_cada] = True
	param_optim['energia_'] = sp.linspace(param_optim['energia_Maxima'], param_optim['energia_Minima'], param_optim['numPasos']) #porcentaje de aproximacion entre x0 y x1 para cambiar el point
	param_optim['aleat'] = sp.linspace(param_optim['maxAleat'], 0., param_optim['numPasos'])


	param_visualizacion = {
		'guardar_drawing': sp.zeros((param_optim['numPasos'],), dtype = bool),
		'generar_imagees': True,
		'draw_cada': 100000,
		'nombre_drawings': 'test_optimizacion_1'
		}

	param_visualizacion['guardar_drawing'][sensor_new_cada - 1::sensor_new_cada] = True

	#PROCESO DE OPTIMIZACIÓN
	o = optimizacion(zona_optimizacion = zona_optimizacion, variogram = v, sensors = sensors, param_optim = param_optim, param_visualizacion = param_visualizacion, error_umbral = 2, I0 = 0.1)
	o.optimizar()

	o.draw_quality()
	o.draw_error()
	now = datetime.datetime.now()
	fecha = now.strftime("%Y-%m-%d_%H_%M")
	nombre_video = directorio + "test_optimizacion_1_" + fecha + ".avi"
	o.video(nombre_video)


def test_optimizacion_2():
	"voy expulsando un sensor cada x pasos y estos se van ubicando en un ring"

	#Generación de la población inicial
	s = 6
	I0 = 1
	error_umbral = 4

	xlim = (-5 * mm, 5 * mm)
	ylim = (-5 * mm, 5 * mm)

	#sampling
	n_sampling = 64
	xsampling = sp.linspace(xlim[0], xlim[1], n_sampling)
	ysampling = sp.linspace(ylim[0], ylim[1], n_sampling)

	"""
	t2 = Scalar_mask_XY(xsampling, ysampling)
	t2.circle(r0 = (0 * mm, 0 * mm), radius = (5 * mm, 5 * mm), angle = 0 * degrees)
	t2.draw(kind = 'intensity')
	#zona donde se deben optimizar la función
	zona_optimizacion = t2.a_dataMatrix()
	#zona_optimizacion.draw()
	"""

	image = 'edificio4.png'
	angle_image = 0 * degrees


	zona_optimizacion = dataMatrix(x = xsampling, y = ysampling, matrix = [], xtext = r"x (m)", ytext = r"y (m)", title = r"sampling")
	zona_optimizacion.image(filename_1 = image, normalize = True, canal = 0, lengthImage = False, inverse = True, angle = angle_image) #no hace falta, pero por si acaso

	"""
	#zona donde se pueden ubicar los sensors
	zona_sensors=dataMatrix(x=xsampling, y=ysampling, matrix=[], xtext=r"x(mm)", ytext=r"y(mm)", title=r"zona sensors")
	zona_sensors.image(filename_1=image, normalize=True, canal=0, lengthImage=False, inverse=False, angle=angle_image)
	zona_sensors.draw()
	"""


	#calculo del variogram
	v = variogram()
	v.establecer_parametros(x = sp.linspace(0, 5, 100), s0 = s, c0 = 1, l0 = 3.5 * mm, f_texto = "s0**2+c0**2*(1-sp.exp(-(x/l0)**2))")
	#v.draw()

	num_sensors_inicial = 4
	num_sensors_total = 8
	sensor_new_cada = 5


	#Ubicación inicial de los sensors
	sensors = data2D(xtext = "x(mm)", ytext = "y(mm)", ztext = "z(mm)", title = "posicion inicial")
	sensors.add_data(x = (0.,), y = (0.,), z = (0.,), nveces = num_sensors_inicial, zideal = (0.,), sc = (0.,), I = (0.,), ordenar = False)


	#parametros del codigo de optimizacion
	param_optim = {
		'numPasos': 20, # 15 * 25 + 1,
		'kappa':  	4., 			#2, 4
		'energia_Maxima':  0.04, 		# 0.02 porcentaje de aproximacion entre x0 y x1 para cambiar el point
		'energia_Minima':  0.04, 	# 0.025 porcentaje de aproximacion entre x0 y x1 para cambiar el point
		'maxAleat':  .0,
		}
	param_optim['energia_'] = sp.linspace(param_optim['energia_Maxima'], param_optim['energia_Minima'], param_optim['numPasos']) #porcentaje de aproximacion entre x0 y x1 para cambiar el point
	param_optim['aleat'] = sp.linspace(param_optim['maxAleat'], 0., param_optim['numPasos'])

	param_optim['anadir_sensor'] = sp.zeros((param_optim['numPasos'],), dtype = bool)
	param_optim['anadir_sensor'][0::sensor_new_cada] = True
	param_optim['anadir_sensor'][sensor_new_cada * (num_sensors_total - 1):-1] = False

	param_visualizacion = {
		'guardar_drawing': sp.zeros((param_optim['numPasos'],), dtype = bool),
		'generar_imagees': False,
		'draw_cada': 1,
		'nombre_drawings': 'prueba'
		}

	param_visualizacion['guardar_drawing'][sensor_new_cada - 1::sensor_new_cada] = True

	#PROCESO DE OPTIMIZACIÓN
	o = optimizacion(zona_optimizacion = zona_optimizacion, variogram = v, sensors = sensors, param_optim = param_optim, param_visualizacion = param_visualizacion, error_umbral = error_umbral, I0 = I0)
	print "numero sensors óptico: ", o.compute_numero_sensors()
	o.optimizar()
	o.draw_quality()
	o.draw_error()

	if param_visualizacion['generar_imagees'] == True:
		now = datetime.datetime.now()
		fecha = now.strftime("%Y-%m-%d_%H_%M")
		nombre_video = param_visualizacion['nombre_drawings'] + "_" + fecha + ".avi"
		o.video(nombre_video)



def test_optimizacion_3():
	"voy expulsando un sensor cada x pasos y estos se van ubicando en un ring"

	#Generación de la población inicial
	s = 0.25
	I0 = 0.01
	error_umbral = .25

	xlim = (-5 * mm, 5 * mm)
	ylim = (-5 * mm, 5 * mm)

	#sampling
	n_sampling = 64
	xsampling = sp.linspace(xlim[0], xlim[1], n_sampling)
	ysampling = sp.linspace(ylim[0], ylim[1], n_sampling)


	t2 = Scalar_mask_XY(xsampling, ysampling)
	t2.circle(r0 = (0 * mm, 0 * mm), radius = (5 * mm, 5 * mm), angle = 0 * degrees)
	t2.draw(kind = 'intensity')

	"""
	#zona donde se pueden ubicar los sensors
	zona_sensors=dataMatrix(x=xsampling, y=ysampling, matrix=[], xtext=r"x(mm)", ytext=r"y(mm)", title=r"zona sensors")
	zona_sensors.image(filename_1=image, normalize=True, canal=0, lengthImage=False, inverse=False, angle=angle_image)
	zona_sensors.draw()
	"""

	#zona donde se deben optimizar la función
	zona_optimizacion = t2.a_dataMatrix()
	#zona_optimizacion.draw()

	#calculo del variogram
	v = variogram()
	v.establecer_parametros(x = sp.linspace(0, 5, 100), s0 = s, c0 = 1, l0 = 2 * mm, f_texto = "s0**2+c0**2*(1-sp.exp(-(x/l0)**2))")
	#v.draw()

	num_sensors = 10 
	#Ubicación inicial de los sensors
	sensors = data2D(xtext = "x(mm)", ytext = "y(mm)", ztext = "z(mm)", title = "posicion inicial")
	sensors.regular_signal(xlim = (0, 5), ylim = (0, 5), num_data = (2, 2), f = 'sp.sqrt(x+y)')
	#parametros del codigo de optimizacion
	param_optim = {
		'numPasos': 15, # 15 * 25 + 1,
		'kappa':  	4., 			#2, 4
		'energia_Maxima':  0.04, 		# 0.02 porcentaje de aproximacion entre x0 y x1 para cambiar el point
		'energia_Minima':  0.04, 	# 0.025 porcentaje de aproximacion entre x0 y x1 para cambiar el point
		'maxAleat':  .25,
		}
	param_optim['energia_'] = sp.linspace(param_optim['energia_Maxima'], param_optim['energia_Minima'], param_optim['numPasos']) #porcentaje de aproximacion entre x0 y x1 para cambiar el point
	param_optim['aleat'] = sp.linspace(param_optim['maxAleat'], 0., param_optim['numPasos'])


	param_visualizacion = {
		'anadir_sensor': sp.zeros((param_optim['numPasos'],), dtype = bool),
		'guardar_drawing': sp.zeros((param_optim['numPasos'],), dtype = bool),
		'generar_imagees': True,
		'draw_cada': 1,
		'nombre_drawings': 'prueba'
		}

	#sensor_new_cada = 15
	#param_optim['anadir_sensor'][0::sensor_new_cada] = True
	#param_visualizacion['guardar_drawing'][sensor_new_cada - 1::sensor_new_cada] = True

	#PROCESO DE OPTIMIZACIÓN
	print "antes de optimización"
	o = optimizacion(zona_optimizacion = zona_optimizacion, variogram = v, sensors = sensors, param_optim = param_optim, param_visualizacion = param_visualizacion, error_umbral = error_umbral, I0 = I0)
	print "numero sensors óptico: ", o.compute_numero_sensors()
	o.distances_sensors()

	if param_visualizacion['generar_imagees'] == True:
		now = datetime.datetime.now()
		fecha = now.strftime("%Y-%m-%d_%H_%M")
		nombre_video = param_visualizacion['nombre_drawings'] + "_" + fecha + ".avi"
		o.video(nombre_video)



def tests_optimizacion():
	#test_circle_10_sensors()
	#test_circle_10_sensors_1en1()
	#test_circle_100_sensors()
	test_optimizacion_1()
	#test_optimizacion_2()
	#test_optimizacion_3()

if __name__ == '__main__':
	tests_optimizacion()
	plt.show()
