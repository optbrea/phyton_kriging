#!/usr/local/bin/python
# -*- coding: utf-8 -*-
#
#----------------------------------------------------------------------
# Name:		 kconvolucion.py
# Purpose:	 Kriging por convolucion
#
# Author:	 Luis Miguel Sanchez Brea
#
# Created:	 2011
# RCS-ID:
# Copyright:
# Licence:	 GPL
#----------------------------------------------------------------------


from phyton_kriging.convolucion1D import *			
from phyton_kriging.standard1D import kriging1D
from data_generator.dataMatrix import dataMatrix

"""Kriging por convolucion
- de momento solo implementar 1 variogram
- intentar meter varios, 1 por posicion
"""



def functionEjemplo1D(function , A = 1., p = 1., num_data = 100, xlim = (0., 5.), noise = 1.125, I0 = 0.00001, kind = 'regular'):

	#data "experimentales"
	s = data1D(xtext = "x(mm)", ytext = "I", title = "function Ejemplo 1D (krigConv)")

	if kind == 'irregular':
		s.senal_irregular(xlim = xlim, num_data = num_data, f = function)
	if kind == 'regular':
		s.regular_signal(xlim = xlim, num_data = num_data, f = function)

	s.add_noise(sc = (noise,), I = (I0,))

	#sampling 
	xsampling = sp.linspace(xlim[0], xlim[1], 500)
	m = data1D()
	m.generarF(x = xsampling, f = function)   #y en esta función es para compute el error cometido entre la estimación y esto

	#calculo del variogram
	v = variogram()
	v.establecer_parametros(x = sp.linspace(0, 2, 100), s0 = noise, c0 = A, l0 = p / 4, f_texto = "s0**2+c0*(1-sp.exp(-(x/l0)**2))")

	return s, m, v


function1 = '.1*self.x**2+0*sp.cos(2*sp.pi*self.x)'
function2 = 'sp.exp(-(self.x/2)**2)'
function = function1



def test_kriging():
	func1 = 'sp.sin(2*sp.pi*self.x)'
	rangoX = (0, 5)
	num_data = 200

	#generar señal muestra para la comparacion
	s_ideal = data1D()
	s_ideal.regular_signal(xlim = rangoX, num_data = num_data, f = func1)

	sc = .525
	I0 = 0.1
	nSensores = 4 * 46 + 1

	s_real = data1D()
	s_real.regular_signal(xlim = rangoX, num_data = num_data, f = func1)
	s_real.add_noise(sc = (sc,), I = (I0,))

	v = variogram(data = s_real)
	v.establecer_parametros(x = s_real.x, s0 = sc, c0 = 1, l0 = .25, f_texto = "s0**2+c0**2*(1-sp.exp(-(x/l0)**2))")


	#generar señal a utilizar
	s_irreg = data1D()
	s_irreg.senal_irregular(xlim = rangoX, num_data = nSensores, f = func1, edges = True)
	s_irreg.add_noise(sc = (sc,), I = (I0,))

	#points de sampling
	xsampling = sp.linspace(rangoX[0], rangoX[1], 250)
	y0 = sp.zeros_like(xsampling)
	m = data1D(x = xsampling, y = y0)

	#kriging estandard
	krig = kriging1D(sensors = s_irreg, variogram = v, sampling = m)
	krig.krigingEstandard(filtrado = True)

	#kriging convolucion 
	kconv = krigingConvolucion1D(sensors = s_irreg, variogram = v, sampling = m, kind = 'moderno')
	kconv.computeError(sensors = s_irreg)

	#drawing NEQ
	plt.figure(figsize = (8, 8), num = None, facecolor = 'w', edgecolor = 'k') #figsize=(4, 3), dpi=100
	plt.title('(a)', fontsize = 26)
	plt.xlabel('$x$', fontsize = 26)
	plt.ylabel('$N_{EQ}(x)$', fontsize = 26)

	plt.plot(krig.kriging.x, krig.kriging.NEQ, 'k--', lw = 2, label = 'kriging')
	plt.plot(kconv.sampling.x, kconv.NEQ, 'k', lw = 2, label = 'convolucion')
	plt.plot(s_irreg.x, sp.zeros_like(s_irreg.x) + .01, 'ko', lw = 8)
	plt.legend()

	#drawing Errores
	plt.figure(figsize = (8, 8), num = None, facecolor = 'w', edgecolor = 'k') #figsize=(4, 3), dpi=100
	plt.title('(b)', fontsize = 26)
	plt.xlabel('$x$', fontsize = 26)
	plt.ylabel('$\sigma(x)$', fontsize = 26)

	plt.plot(krig.kriging.x, krig.kriging.error, 'k--', lw = 2, label = 'kriging')
	plt.plot(kconv.sampling.x, kconv.error, 'k', lw = 2, label = 'convolucion')
	plt.plot(s_irreg.x, sp.zeros_like(s_irreg.x) + .01, 'ko', lw = 8)
	plt.legend()

	#drawing Estimacion
	plt.figure(figsize = (8, 8), num = None, facecolor = 'w', edgecolor = 'k') #figsize=(4, 3), dpi=100
	plt.title('(b)', fontsize = 26)
	plt.xlabel('$x$', fontsize = 26)
	plt.ylabel('$Z(x)$', fontsize = 26)

	plt.plot(krig.kriging.x, krig.kriging.y, 'k--', lw = 2, label = 'kriging')
	plt.plot(kconv.sampling.x, kconv.estimacion, 'k', lw = 2, label = 'convolucion')
	plt.plot(s_ideal.x, s_ideal.y, 'r', lw = 2, label = 'ideal')
	plt.plot(s_irreg.x, s_irreg.y, 'ko', lw = 8)
	plt.legend()

	#drawing lambdas
	plt.figure(figsize = (8, 8), num = None, facecolor = 'w', edgecolor = 'k') #figsize=(4, 3), dpi=100
	plt.plot(kconv.x, kconv.Lambda)
	plt.plot(krig.sampling.x, krig.lambdas, 'r')



def testRegular():

	s, m, v = functionEjemplo1D(function = function, A = 1., p = 6., num_data = 250, xlim = (-5., 5.), noise = 0.125, I0 = 0.00001, kind = 'regular')

	kconv = krigingConvolucion1D(sensors = s, variogram = v, sampling = m, kind = 'antiguo')

	kconv.computeError(sensors = s)
	kconv.estimar(sensors = s)

	kconv.draw('comparar')
#	kconv.draw('data')
#	kconv.draw('var')
#	kconv.draw('DM')
#	kconv.draw('NEQ')
#	kconv.draw('error')
#	kconv.draw('lambda')
#	kconv.draw('estimacion')
#	kconv.draw('all')
	return kconv



def testIrregular():

	s, m, v = functionEjemplo1D(function = function , A = 1., p = 6., num_data = 250, xlim = (-5., 5.), noise = 0.125, I0 = 0.00001, kind = 'irregular')

	kconv = krigingConvolucion1D(sensors = s, variogram = v, sampling = m, kind = 'antiguo')
	kconv.computeError(sensors = s)
	kconv.estimar(sensors = s)

#	kconv.draw('data')
#	kconv.draw('var')
	kconv.draw('DM')
#	kconv.draw('NEQ')
#	kconv.draw('error')
	kconv.draw('lambda')
#	kconv.draw('estimacion')
#	kconv.draw('all')
	return kconv



def testSinEstimacion():

	s, m, v = functionEjemplo1D(function = function , A = 1., p = 6., num_data = 26, xlim = (-5., 5.), noise = 0.125, I0 = 0.1, kind = 'regular')

	kconv = krigingConvolucion1D(sensors = s, variogram = v, sampling = m, kind = 'antiguo')
	kconv.computeError(sensors = s)
	kconv.draw('error')

	s.add_data(x = (.4, .8), y = (0., 0.), nveces = 1, yideal = (0., 0.), sc = (0., 0.), I = (0.1, 0.1), ordenar = True)

	kconv.computeError(sensors = s)
	kconv.draw('error')

	return kconv



def test_error_noise():

	xlim = (-5, 5)
	rango = xlim[1] - xlim[0] 
	func1 = 'sp.sin(2*sp.pi*self.x)'

	num_data = 10
	I0 = 0.01
	sc = 0.5
	A = 1.
	p = 1.


	#señal
	s = data1D()
	s.regular_signal(xlim = xlim, num_data = num_data, f = func1)
	s.add_noise(sc = (sc,), I = (I0,))

	#sampling 
	xsampling = sp.linspace(xlim[0], xlim[1], 200)
	m = data1D()
	m.generarF(x = xsampling, f = function)   #y en esta función es para compute el error cometido entre la estimación y esto

	#calculo del variogram
	v = variogram()
	#v.draw(kind='teorico')

	"""
	error en función del noise
	"""
	SC = sp.linspace(0.01, 1, 10)
	erroresmaximum = sp.zeros_like(SC, float)

	for sc, i in zip(SC, range(len(SC))):
		v.establecer_parametros(x = sp.linspace(0, 2, 100), s0 = sc, c0 = A, l0 = p / 4, f_texto = "s0**2+c0*(1-sp.exp(-(x/l0)**2))")
		kconv = krigingConvolucion1D(sensors = s, variogram = v, sampling = m, kind = 'moderno')
		erroresmaximum[i] = kconv.error.max()


	errores = data1D(x = SC, y = erroresmaximum, xtext = r"$s$", ytext = r"$error_{maximum}$")
	errores.draw()



def test_error_frecuencia():

	xlim = (-5, 5)
	rango = xlim[1] - xlim[0]
	func1 = 'sp.sin(2*sp.pi*self.x)'

	num_data = 10
	I0 = 0.01
	sc = 0.5
	A = 1.
	p = 1.

	#señal
	s = data1D()
	s.regular_signal(xlim = xlim, num_data = num_data, f = func1)
	s.add_noise(sc = (sc,), I = (I0,))

	#sampling 
	xsampling = sp.linspace(xlim[0], xlim[1], 200)
	m = data1D()
	m.generarF(x = xsampling, f = function)   #y en esta función es para compute el error cometido entre la estimación y esto

	#calculo del variogram
	v = variogram()
	#v.draw(kind='teorico')


	"""
	error en función de la frecuencia
	"""
	sc = 0.1
	v.establecer_parametros(x = sp.linspace(0, 2, 100), s0 = sc, c0 = A, l0 = p / 4, f_texto = "s0**2+c0*(1-sp.exp(-(x/l0)**2))")

	FREC = sp.linspace(1, 20, 21)
	erroresmaximum = sp.zeros_like(FREC)

	for frec, i in zip(FREC, range(len(FREC))):
		num_data = int(rango * frec)
		s.regular_signal(xlim = xlim, num_data = num_data, f = func1)
		s.add_noise(sc = (sc,), I = (I0,))

		kconv = krigingConvolucion1D(sensors = s, variogram = v, sampling = m, kind = 'moderno')
		erroresmaximum[i] = kconv.error.max()

	errores = data1D(x = FREC, y = erroresmaximum, xtext = r"$frec$", ytext = r"$error_{maximum}$")
	errores.draw()


def test_error_frecuencia_noise():

	xlim = (-5, 5)
	rango = xlim[1] - xlim[0]
	func1 = 'sp.sin(2*sp.pi*self.x)'
	num_data = 10
	I0 = 0.01
	sc = 0.5
	A = 1.
	p = 1.

	#señal
	s = data1D()
	s.regular_signal(xlim = xlim, num_data = num_data, f = func1)
	s.add_noise(sc = (sc,), I = (I0,))

	#sampling 
	xsampling = sp.linspace(xlim[0], xlim[1], 200)
	m = data1D()
	m.generarF(x = xsampling, f = function)   #y en esta función es para compute el error cometido entre la estimación y esto

	#calculo del variogram
	v = variogram()
	#v.draw(kind='teorico')

	"""
	error en función de la frecuencia y del error
	"""
	sc_v = sp.linspace(0.01, .5, 25)
	frec_v = sp.linspace(1, 50, 25)
	SC, FREC = sp.meshgrid(sc_v, frec_v)
	erroresmaximum = sp.zeros_like(SC, float)

	for sc, i in zip(sc_v, range(len(sc_v))):
		v.establecer_parametros(x = sp.linspace(0, 2, 100), s0 = sc, c0 = A, l0 = p / 4, f_texto = "s0**2+c0*(1-sp.exp(-(x/l0)**2))")

		for frec, j in zip(frec_v, range(len(frec_v))):
			num_data = int(rango * frec)
			s.regular_signal(xlim = xlim, num_data = num_data, f = func1)
			s.add_noise(sc = (sc,), I = (I0,))

			kconv = krigingConvolucion1D(sensors = s, variogram = v, sampling = m, kind = 'moderno')
			erroresmaximum[j, i] = kconv.error.max()


	errores = dataMatrix(x = frec_v, y = sc_v, matrix = erroresmaximum.transpose(), xtext = r"$f$", ytext = r"$frec$", title = r"$error(f,s)$")
	errores.draw()
	plt.figure()
	cs = plt.contour(FREC, SC, erroresmaximum, levels = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6], colors = "k", linestyles = "solid") #[0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
	plt.clabel(cs, inline = 2, fontsize = 12)
#	from mpl_toolkits.mplot3d import Axes3D
#	from matplotlib import cm
#	from matplotlib.ticker import LinearLocator, FormatStrFormatter
#	fig = plt.figure()
#	ax = fig.gca(projection='3d')
#	surf = ax.plot_surface(FREC, SC, erroresmaximum, rstride=1, cstride=1, linewidth=0, antialiased=True) #,cmap=cm.coolwarm, 
#	fig.colorbar(surf)
#	ax.set_xlabel('$\mu / \mu_{max}$')
#	ax.set_ylabel('sc')
#	ax.set_zlabel('$\sigma_{max}$')


def test_krigConv():
	test_kriging()
	#testRegular()
	#testIrregular()
	#testSinEstimacion()
	#test_error_frecuencia()
	#test_error_noise()
	#test_error_frecuencia_noise()



if __name__ == '__main__':
	checkTiempo = 0
	if checkTiempo == 1:
		from cProfile import run 
		run('test_krigConv()', sort = True)
	else:
		test_krigConv()

	plt.show()
