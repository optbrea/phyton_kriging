#!/usr/bin/env python
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------
# Name:		 krigStandard2D_test.py
# Purpose:	 Genera la interpolation del kriging estandard
#
# Author:	  Luis Miguel Sanchez Brea
#
# Created:	 11/08/04
# RCS-ID:
# Copyright:
# Licence:	 GPL
#----------------------------------------------------------------------
"""
Kriging ordinario para 2D
Aquí se hacen algunos tests
"""

from phyton_kriging.standard2D import *
from phyton_kriging.variogram2D_radial import *
import scipy as sp

function1 = "'{}*sp.cos(2*sp.pi*(x**2+y**2)/{})'.format(A,p)"
function2 = "'{}*sp.cos(2*sp.pi*x/{})*sp.cos(2*sp.pi*y/{})'.format(A,p,p)"
function3 = "'{}*sp.exp(-(x/{})**2-(y/{})**2)'.format(A,p,p)"
function4 = "'{}*sp.exp(-((x-5)/{})**2-(y/{})**2)'.format(A,p,p)"


def functionEjemplo2D(function=function1,
                      xlim=(-1, 1),
                      ylim=(-1, 1),
                      num_data=(40, 20),
                      A=1.,
                      p=1.,
                      noise=0.1,
                      I0=0.00001,
                      kind='regular'):

    function_ajuste = variogram_gauss

    #data "experimentales"
    s = data2D(xtext="x(mm)",
               ytext="y(mm)",
               ztext="z(mm)",
               title="data para kriging")

    f = eval(function)
    print f

    if kind == 'regular':
        s.regular_signal(xlim, ylim, num_data, func=f)
    if kind == 'irregular':
        s.senal_aleatoria(xlim, ylim, num_data,
                          func=f)  #sp.sin(2*sp.pi*sp.sqrt(x**2+y**2))
    if kind == 'manual':
        mask = '/home/luismiguel/Dropbox/python/python/imagees/bmps/anillo.bmp'
        s.senal_manual(xlim, ylim, num_data=5, mask=mask, func=function)

    s.add_noise(sc=(noise, ), I=(I0, ))

    #sampling
    m = dataMatrix()
    m.function(xlim, ylim, num_data=(50, 40), func=f)
    m.add_noise(sc=noise, I=I0)
    #calculo del variogram
    v = variogram2D_radial(
        s, function_ajuste)  #variogram_gauss variogram_exponencial
    v.experimental(incr_h=0.02)
    v.nugget('nearest')

    return s, m, v


def test_function_example():
    s, m, v = functionEjemplo2D(function=function4,
                                xlim=(-4, 6),
                                ylim=(-2, 2),
                                num_data=(40, 20),
                                A=1.,
                                p=5,
                                noise=0.25,
                                I0=0.00001,
                                kind='regular')
    param_ini = v.compute_param_ini()
    v.ajuste(param_ini, 0.75)
    s.draw()
    v.draw('all_')
    m.draw('all_')

    return s, m, v


def testKriging_1():
    """
	function muy variable
	"""

    s, m, v = functionEjemplo2D(function=function2,
                                xlim=(-4, 6),
                                ylim=(-2, 2),
                                num_data=(30, 30),
                                A=1.,
                                p=5,
                                noise=0.2,
                                I0=0.00001,
                                kind='regular')
    param_ini = v.compute_param_ini()
    v.ajuste(param_ini, 0.25)

    s.draw()
    v.draw('ajuste')
    m.draw('señal')

    k = kriging2D(sensors=s, variogram=v, sampling=m)
    k.krigStandard2D()

    k.draw(kind='kriging')
    k.draw(kind='error')
    k.draw(kind='data')
    #k.draw(kind='lambdas')

    diferencias = k.interpolation - m.Zideal.T
    m2 = dataMatrix(x=m.x, y=m.y, matrix=diferencias, title='diferencias')
    m2.draw()


def testKriging_2():
    """
	function lentamente variable
	"""

    s, m, v = functionEjemplo2D(function=function4,
                                xlim=(-4, 6),
                                ylim=(-2, 2),
                                num_data=(20, 30),
                                A=1.,
                                p=5,
                                noise=0.2,
                                I0=0.00001,
                                kind='regular')

    param_ini = v.compute_param_ini()
    v.ajuste(param_ini, 0.75)

    s.draw()
    v.draw('ajuste')
    m.draw('señal')

    k = kriging2D(sensors=s, variogram=v, sampling=m)
    k.krigStandard2D()

    k.draw(kind='kriging')
    k.draw(kind='error')
    k.draw(kind='data')
    #k.draw(kind='lambdas')

    diferencias = k.interpolation - m.Zideal.T
    m2 = dataMatrix(x=m.x, y=m.y, matrix=diferencias, title='diferencias')
    m2.draw()


def testKriging_3():
    """
	pocos data
	"""

    s, m, v = functionEjemplo2D(function=function4,
                                xlim=(-4, 6),
                                ylim=(-2, 2),
                                num_data=(5, 5),
                                A=1.,
                                p=5,
                                noise=0.2,
                                I0=0.00001,
                                kind='regular')
    v.experimental(incr_h=0.2)
    v.nugget('nearest')
    param_ini = v.compute_param_ini()
    v.ajuste(param_ini, 0.75)

    s.draw()
    v.draw('ajuste')
    m.draw('señal')

    k = kriging2D(sensors=s, variogram=v, sampling=m)
    k.krigStandard2D()

    k.draw(kind='kriging')
    k.draw(kind='error')
    k.draw(kind='data')
    #k.draw(kind='lambdas')

    diferencias = k.interpolation - m.Zideal.T
    m2 = dataMatrix(x=m.x, y=m.y, matrix=diferencias, title='diferencias')
    m2.draw()


def testKriging_4():
    """
	muchos data
	"""

    s, m, v = functionEjemplo2D(function=function4,
                                xlim=(-4, 6),
                                ylim=(-2, 2),
                                num_data=(40, 40),
                                A=1.,
                                p=5,
                                noise=0.2,
                                I0=0.00001,
                                kind='regular')
    v.experimental(incr_h=0.2)
    v.nugget('nearest')
    param_ini = v.compute_param_ini()
    v.ajuste(param_ini, 0.75)

    s.draw()
    v.draw('ajuste')
    m.draw('señal')

    k = kriging2D(sensors=s, variogram=v, sampling=m)
    k.krigStandard2D()

    k.draw(kind='kriging')
    k.draw(kind='error')
    k.draw(kind='data')
    #k.draw(kind='lambdas')

    diferencias = k.interpolation - m.Zideal.T
    m2 = dataMatrix(x=m.x, y=m.y, matrix=diferencias, title='diferencias')
    m2.draw()


def tests_kriging2D(all_=False):
    tests = {
        'test_function_example': 0,
        'testKriging_1': 0,
        'testKriging_2': 0,
        'testKriging_3': 0,
        'testKriging_4': 1,
    }

    for example in tests.keys():
        if tests[example] == 1 or all_ == True:
            print 'TEST for function ' + example
            eval(example + '()')


if __name__ == '__main__':
    import sys
    sys.path.append('tests/')
    tests_kriging2D(all_=False)
    plt.show()
