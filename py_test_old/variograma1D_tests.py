#!/usr/bin/env python
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------
# Name:		fuentes2D.py
# Purpose:	 Fuentes de luz 2D
#
# Author:	  Luis Miguel Sanchez Brea
#
# Created:	 2011
# RCS-ID:
# Copyright:
# Licence:	 GPL
#----------------------------------------------------------------------
"""
Clases Scalar_field_XY y Fuentes2D con las fuentes utilizadas para propagacion
"""

from phyton_kriging.variogram1D import *


def functionEjemplo(A=1, p=0.5, noise=0, I0=0.00001):
    #generamos data "experimentales"
    x = sp.linspace(0, 2, 500)
    y = A * sp.sin(2 * sp.pi * x / p)
    points = data1D(x, y)
    points.add_noise(sc=(noise, ), I=(I0, ))
    #points.draw()

    return points


def test_variogramExperimental():
    points = functionEjemplo(A=1, p=0.5, noise=1, I0=0.00001)
    ix = sp.arange(1, round(len(points.x)))

    var = variogram(points, variogram_gauss)
    var.compute_experimental(ipoints=ix)
    var.draw(kind='experimental')


def test_variogramTeorico():
    """
	Variograma por parámetros sin compute nada
	"""

    var = variogram(data=data1D(), function_ajuste=variogram_gauss)
    var.establecer_parametros(s0=1, c0=1, l0=0.125)
    x = sp.linspace(0, 5, 100)
    y = var.f(x)
    var.draw('teorico', sp.linspace(0, 1, 100))


def test_comparacion_variograms_teoricos():

    parametros = (0.25, 1, 2)  #s0 = .25, c0 = 1, l0 = 2
    parametros_potencia = (0.25, 1, 2, 3)
    print parametros_potencia
    x = sp.linspace(0, 12, 100)
    data = data1D()

    Tipos_variograms.remove(variogram_potencial)

    plt.figure()
    for function_variogram in Tipos_variograms:
        var1 = variogram(data, function_variogram)
        if var1.function_ajuste in [variogram_potencial]:
            param = parametros_potencia
        else:
            param = parametros

        var1.establecer_parametros(*param)
        y1 = var1.f(x)
        print x.shape, y1.shape
        plt.plot(x, y1, lw=2, label=function_variogram.__name__)

    plt.legend()
    """
	#gauss
	var1 = variogram(data, variogram_gauss)
	var1.establecer_parametros(*parametros)
	y1=var1.f(x)

	#exponencial
	var2 = variogram(data, variogram_exponencial)
	var2.establecer_parametros(*parametros)
	y2=var2.f(x)

	#exponencial
	var3 = variogram(data, variogram_esferico)
	var3.establecer_parametros(*parametros)
	y3=var3.f(x)

	#cúbico
	var4 = variogram(data, variogram_cubico)
	var4.establecer_parametros(*parametros)
	y4=var4.f(x)

	#cúbico
	var5 = variogram(data, variogram_potencial)
	var5.establecer_parametros(*parametros_potencia)
	print parametros_potencia
	y5=var5.f(x)


	plt.figure()
	plt.plot(x,y1,'k',lw=2,label='gauss')
	plt.plot(x,y2,'r',lw=2,label='exponencial')
	plt.plot(x,y3,'b',lw=2,label='esferico')
	plt.plot(x,y4,'g',lw=2,label='cubico')
	plt.legend()

	plt.figure()
	plt.plot(x,y5,'k',lw=2,label='potencial')
	plt.legend()
	"""


def test_comparacion():
    points = functionEjemplo(A=.6, p=1, noise=0.025, I0=0.1)
    ix = sp.arange(1, round(len(points.x)) / 2)

    var = variogram(points, variogram_gauss)
    var.compute_experimental(ipoints=ix)
    var.nugget('lineal')
    param_ini = var.compute_param_ini()
    var.ajuste(param_ini)

    #var.draw(kind = 'ambos')
    var.draw(kind='all')


def test5_recorte():  #recorte forzado

    points = functionEjemplo(A=1, p=0.5, noise=1, I0=0.1)
    ix = sp.arange(1, round(len(points.x)) / 2)

    var = variogram(points, variogram_esferico)
    var.compute_experimental(ipoints=ix)
    param_ini = var.compute_param_ini()
    var.ajuste(param_ini, corte=0.5)
    var.draw(kind='ambos')
    var.draw(kind='ajuste')


def test5_eliminar_fuctuaciones():  #recorte forzado

    points = functionEjemplo(A=1, p=0.5, noise=1, I0=0.1)
    ix = sp.arange(1, round(len(points.x)) / 2)

    var = variogram(points, variogram_gauss)
    var.compute_experimental(ipoints=ix)
    var.eliminar_fluctuaciones(porcentaje=.5)
    param_ini = var.compute_param_ini()

    var.ajuste(param_ini, corte=0.5)
    var.draw(kind='ambos')
    var.draw(kind='ajuste')


def test7_ajuste_manual():
    points = functionEjemplo(A=.6, p=1, noise=0.025, I0=0.1)
    ix = sp.arange(1, round(len(points.x)) / 2)

    var = variogram(points, variogram_gauss)
    var.compute_experimental(ipoints=ix)
    var.nugget('lineal')
    param_ini = var.compute_param_ini()
    var.ajuste(param_ini)
    var.draw(kind='ambos')

    paramV = var.ajuste_manual(title='hola')

    print 'parametros calculados:', paramV
    plt.show()


def tests_variogram(all_=False):
    tests = {
        'test_variogramTeorico': 1,
        'test_comparacion_variograms_teoricos': 0,
        'test_variogramExperimental': 1,
        'test_comparacion': 0,
        'test5_recorte': 0,
        'test5_eliminar_fuctuaciones': 0,
        'test7_ajuste_manual': 0,
    }

    for example in tests.keys():
        if tests[example] == 1 or all_ == True:
            print 'TEST for function ' + example
            eval(example + '()')


if __name__ == '__main__':

    tests_variogram(all_=False)
    plt.show()
