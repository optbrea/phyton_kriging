# !/usr/bin/env python
# -*- coding: utf-8 -*-
#

from calculo_period import *
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

filename_1 = 'test_period_a.mat'


def test_cargar_archivo():
    x, z, I = cargar_data(filename_1,
                          nombrex='x',
                          nombrez='Pos',
                          nombreI='profile',
                          rotate=False)

    print x.shape, z.shape, I.shape


def test_mostrar_figura():
    # mostrar figura
    x, z, I = cargar_data(filename_1,
                          nombrex='x',
                          nombrez='Pos',
                          nombreI='profile',
                          rotate=False)

    num_x = len(x)
    num_z = len(z)

    plt.figure(figsize=(6, 5), facecolor='w', edgecolor='k')
    extension = [x.min(), x.max(), z.min(), z.max()]

    IDimage = plt.imshow(sp.real(I),
                         interpolation='bilinear',
                         aspect='auto',
                         origin='lower',
                         extent=extension)

    plt.xlabel('$x_{3}\,(mm)$', fontsize=20)
    plt.ylabel('$\Delta z+c\,(mm)$', fontsize=20)
    plt.axis(extension)
    cbar = plt.colorbar(orientation='horizontal')
    cbar.set_label('$g.l.$', rotation=0, labelpad=-25, y=1.07, fontsize=20)
    plt.set_cmap("jet")  #
    # plt.savefig("Fig2.pdf", dpi = 600, bbox_inches = 'tight', pad_inches = 0.1)


def test_visualizar_variogram():
    # visualiza parte de un variogram
    x, z, I = cargar_data(filename_1,
                          nombrex='x',
                          nombrez='Pos',
                          nombreI='profile',
                          rotate=False)
    tmp_draw = True
    visualizar_variogram(x,
                         z,
                         I,
                         zi=7.92105263,
                         ix=arange(300, 600),
                         todos_variograms=True)
    tmp_draw = False


def test_ajustar_1_minimum(linea=0,
                           long_variogram=500,
                           period_inicial=41,
                           anchura=7):
    # visualiza un variogram con un mínimo ajustado.
    x, z, I = cargar_data(filename_1,
                          nombrex='x',
                          nombrez='Pos',
                          nombreI='profile',
                          rotate=False)
    profile = squeeze(I[linea, :])
    points = data1D(x=x, y=profile)
    var = variogram(data=points, function_ajuste=variogram_gauss)
    var.compute_experimental(ipoints=arange(1, long_variogram))
    # var.draw(kind  =  'experimental')
    h = var.h
    v = var.variogram_exp

    num_minimum = 1
    ix = arange(period_inicial - anchura, period_inicial + anchura)
    iminimum = v[ix].argmin() + ix[0]
    period = h[iminimum] / num_minimum
    print iminimum

    plt.figure()
    plt.plot(h, v, lw=2)
    plt.plot(h[ix], v[ix], 'ko')

    # realiza un ajuste cuadrático
    period_interpolado = ajuste_minimum(h[ix], v[ix]) / num_minimum

    print period, period_interpolado, iminimum


def test_ajustar_n_minimums(linea=0,
                            long_variogram=500,
                            period_inicial=42,
                            anchura=7):
    # calculamos los n primeros mínimos por medio de interpolación.
    x, z, I = cargar_data(filename_1,
                          nombrex='x',
                          nombrez='Pos',
                          nombreI='profile',
                          rotate=False)
    profile = squeeze(I[linea, :])
    points = data1D(x=x, y=profile)
    var = variogram(data=points, function_ajuste=variogram_gauss)
    var.compute_experimental(ipoints=arange(1, long_variogram))
    # var.draw(kind  =  'experimental')
    h = var.h
    v = var.variogram_exp

    ix = arange(period_inicial - anchura, period_inicial + anchura)

    for num_minimum in range(1, 4):
        period, period_interpolado, iminimum = period_senal(
            h, v, ix=ix, num_minimum=num_minimum)
        print period, period_interpolado, iminimum

        ix = ix + iminimum
        plt.figure()
        plt.plot(h, v, lw=2)
        plt.plot(h[ix], v[ix], 'ko')


def prueba_5(linea=-1, period_inicial=42, anchura=7):
    x, z, I = cargar_data(filename_1,
                          nombrex='x',
                          nombrez='Pos',
                          nombreI='profile',
                          rotate=False)

    ix = arange(1, round(len(x) / 4))
    profile = squeeze(I[linea, :])
    points = data1D(x=x, y=profile)
    var = variogram(data=points, function_ajuste=variogram_gauss)
    var.compute_experimental(ipoints=ix)
    var.draw(kind='experimental')
    h = var.h
    v = var.variogram_exp

    period, period_interpolado, iminimum, i = calculo_period(
        h, v, anchura_minimum=anchura)

    plt.figure()
    plt.plot(i, period, 'ko', ms=10)
    plt.plot(i, period_interpolado, 'r', lw=2)
    plt.title('sin interpolation de los minimums', fontsize=24)


def prueba_6(period_inicial=45, anchura=7):
    # ahora hacemos un bucle for para todos los z
    tmp_draw = False
    x, z, I = cargar_data(filename_1,
                          nombrex='x',
                          nombrez='Pos',
                          nombreI='profile',
                          rotate=False)

    ix = arange(1, len(x) / 2)

    Periodo = zeros_like(z)
    Periodo_interpolado = zeros_like(z)
    for iz in range(len(z)):  # range(5)
        profile = squeeze(I[iz, :])
        points = data1D(x=x, y=profile)
        var = variogram(data=points, function_ajuste=variogram_gauss)
        var.compute_experimental(ipoints=ix)
        # var.draw(kind  =  'experimental')
        h = var.h
        v = var.variogram_exp
        # plt.figure()
        # plt.plot(h, v, 'k')
        tmp_draw = False
        period, period_interpolado, iminimum, i = calculo_period(
            h,
            v,
            anchura_minimum=anchura,
            period_inicial=int(period_inicial + 2))
        Periodo[iz] = sp.mean(period)
        Periodo_interpolado[iz] = sp.mean(period_interpolado)
        period_inicial = Periodo_interpolado[iz]

    plt.figure()
    plt.plot(z, Periodo, 'rx', ms=10, label='sin interpolar')
    plt.plot(z, Periodo_interpolado, 'ko', ms=10, label='interpolado')
    plt.title('ajuste', fontsize=24)
    plt.legend(frameon=False)


def mostrar_figura():
    x, z, I = cargar_data(filename_1)
    num_x = len(x)
    num_z = len(z)

    plt.figure(figsize=(6, 5), facecolor='w', edgecolor='k')
    extension = [x.min(), x.max(), z.min(), z.max()]

    IDimage = plt.imshow(sp.real(I),
                         interpolation='bilinear',
                         aspect='auto',
                         origin='lower',
                         extent=extension)

    plt.xlabel('$x_{3}\,(mm)$', fontsize=20)
    plt.ylabel('$\Delta z+c\,(mm)$', fontsize=20)
    plt.axis(extension)
    cbar = plt.colorbar(orientation='horizontal')
    cbar.set_label('$g.l.$', rotation=0, labelpad=-25, y=1.07, fontsize=20)
    plt.set_cmap("jet")  #

    plt.savefig("Fig2.pdf", dpi=600, bbox_inches='tight', pad_inches=0.1)


def Figura3():
    # visualiza senal y variogram
    x, z, I = cargar_data(filename_1,
                          nombrex='x',
                          nombrez='Pos',
                          nombreI='profile',
                          rotate=False)
    num_x = len(x)
    num_z = len(z)

    i_z, tmp, tmp = cercano(z, 0)
    i_x, tmp, tmp = cercano(x, 0)

    profile = sp.squeeze(I[i_x, :])
    points = data1D(x=x,
                    y=profile,
                    xtext=r"$x_3\,(mm)$",
                    ytext=r"$I(x_3)\,\,(g.l.)$",
                    title="(a)")
    posicion = z[i_z]

    id_fig = points.draw()
    id_fig.set_size_inches(6, 6)

    plt.tight_layout()
    # plt.savefig('Fig3a.pdf', dpi = 600, bbox_inches = 'tight', pad_inches = 0.1)

    # ___________________________________________________

    ix = arange(300, 600)

    print "z profile = ", posicion
    var = variogram(data=points, function_ajuste=variogram_gauss)
    var.compute_experimental(ipoints=ix)
    var.draw(kind='experimental')
    h = var.h
    v = var.variogram_exp
    n_points = var.num_h
    v1 = data1D(x=h,
                y=v,
                xtext=r"$h\,(mm)$",
                ytext=r"$\gamma (h)\,\,(g.l.^2)$",
                title="(b)")

    id_fig = v1.draw()
    id_fig.set_size_inches(6, 6)
    plt.ylim(ymin=0, ymax=80)
    plt.xlim(xmin=0.0, xmax=0.9)
    plt.tight_layout()
    # plt.savefig('Fig3b1.pdf', dpi = 600, bbox_inches = 'tight', pad_inches = 0.1)

    h_trozo = h[44 - 9:44 + 9]
    v_trozo = v[44 - 9:44 + 9]
    coefs = sp.polyfit(h_trozo, v_trozo, 2)
    print "coeficientes interpolation", coefs
    h_ajuste = sp.linspace(h_trozo[2], h_trozo[-2], 100)
    v_ajuste = sp.polyval(coefs, h_ajuste)

    plt.figure(figsize=(6, 6), facecolor='w', edgecolor='k')
    plt.plot(h_trozo, v_trozo, 'k', lw=4)
    plt.plot(h_trozo, v_trozo, 'ko', ms=8)
    iminimum = v_ajuste.argmin()
    plt.plot(h_ajuste[iminimum], v_ajuste[iminimum], 'ro', ms=12)

    plt.plot(h_ajuste, v_ajuste, 'r--', lw=4)
    id_fig.set_size_inches(6, 6)
    plt.ylim(ymin=0, ymax=30)
    plt.xlim(xmin=0.08, xmax=0.115)
    plt.xlabel("")
    plt.ylabel("")
    plt.title("")
    plt.tight_layout()
    # plt.savefig('Fig3b2.pdf', dpi = 600, bbox_inches = 'tight', pad_inches = 0.1)

    # ________________________________________________
    fig = plt.figure(figsize=(6, 6), facecolor='w', edgecolor='k')
    ax = fig.add_subplot(111)
    plt.plot(h, v, 'k', lw=2)
    plt.ylim(ymin=0, ymax=80)
    plt.xlim(xmin=0.0, xmax=0.9)
    plt.xlabel(r"$h\,(mm)$", fontsize=20)
    plt.ylabel(r"$\gamma (h)\,\,(g.l.^2)$", fontsize=20)
    plt.title("(b)", fontsize=26)

    axins = zoomed_inset_axes(ax, 14, loc=7)  # zoom = 6 #
    axins.plot(h, v, 'k', lw=2)
    axins.plot(h_ajuste, v_ajuste, 'r--', lw=2)
    iminimum = v_ajuste.argmin()
    axins.plot(h_ajuste[iminimum], v_ajuste[iminimum], 'ro', ms=8)
    axins.set_aspect(.125 / axins.get_data_ratio())

    x1, x2, y1, y2 = 0.080, 0.115, 0, 30

    # sub region of the original image
    axins.set_xlim(x1, x2)
    axins.set_ylim(y1, y2)
    plt.xticks(visible=False)
    plt.yticks(visible=False)

    # draw a bbox of the region of the inset axes in the parent axes and
    # connecting lines between the bbox and the inset axes area
    mark_inset(ax, axins, loc1=2, loc2=4, fc="none", ec="0.75")
    print "esto: ", axins.get_data_ratio()

    # plt.savefig('Fig3b.pdf', dpi = 600, bbox_inches = 'tight', pad_inches = 0.1)

    # FIGURE3C
    x = sp.array(range(10))
    y1 = 0.1 + x
    y2 = y1 + 0.001 * sp.rand(len(x))
    plt.figure(figsize=(6, 6), facecolor='w', edgecolor='k')
    plt.plot(x, y2, 'ko', ms=10, label=r"experimental minima")
    plt.plot(x, y1, 'k', lw=2, label=r"linear fit")
    plt.xlabel('$\# \, minimum$', fontsize=20)
    plt.ylabel('$position \, of \, minimum \, (mm)$', fontsize=20)
    plt.legend(loc="upper left")
    plt.title("(c)", fontsize=26)
    plt.tight_layout()
    # plt.savefig("Fig3c.pdf", dpi = 600, bbox_inches = 'tight', pad_inches = 0.1)


def Figura4_5(ordenes=10):
    # se hace un ajuste a los n primeros órdenes
    p0 = -0.02 * um
    z0 = 9.265
    pred = 100 * um

    PRUEBA = False
    ordenes = sp.array(range(1, ordenes))

    filename_1 = "test_period_a.mat"
    xi, zi, I = cargar_data(filename_1,
                            nombrex='x',
                            nombrez='Pos',
                            nombreI='profile',
                            rotate=False)

    Residuos = sp.ones(len(ordenes))
    Coefs = sp.ones((len(ordenes), 2))
    Periodo_interpolado = sp.ones((len(ordenes), len(zi) - 2))

    # algoritmo que calcula el period para cada mínimo del variogram y para diversos órdenes
    for orden, i in zip(ordenes, range(len(ordenes))):
        z, period, period_interpolado, H, V = calculo_period_moviendo(
            xi, zi, I, orden=orden)
        coefs = sp.polyfit(z, period_interpolado, 1)
        print "coeficientes interpolation", coefs
        p_lineal = sp.polyval(coefs, z)
        residuos = (period_interpolado - p_lineal) * 1000  # nanometros
        Residuos[i] = sp.std(residuos)
        Periodo_interpolado[i, :] = period_interpolado
        Coefs[i, :] = coefs

    zf = zi[1:-1]
    Dz = (zf - z0) * 1000  # micras
    Periodo = sp.zeros_like(zf)

    # algoritmo que hace un ajuste lineal a la posición de los mínimos del variogram
    for z, i in zip(zf, range(len(zf))):
        coefs_period = sp.polyfit(ordenes,
                                  Periodo_interpolado[:, i] * (ordenes + 1), 1)
        Periodo[i] = coefs_period[0]  # periods para cada distance

    # ajuste lineal al cálculo de periods
    coefs = sp.polyfit(zf, Periodo, 1)
    print "coeficientes interpolation", coefs
    p_lineal = sp.polyval(coefs, zf)

    residuos = (Periodo - p_lineal) * 1000  # nanometros

    # FIGURA donde se calcula el period en function de z
    plt.figure(figsize=(6, 6), facecolor='w', edgecolor='k')
    plt.plot(Dz,
             Periodo - p0 - pred,
             'r',
             lw=2,
             ms=10,
             label='linear fit - 10 minima')
    plt.plot(Dz,
             p_lineal - p0 - pred,
             'k',
             lw=1.5,
             label="linear interpolation")
    # print "z, z0", z[0], z0
    # print "p, p0", period_interpolado[0], p0
    plt.xlabel('$\Delta z\,(\mu m)$', fontsize=20)
    plt.xlim(Dz.min(), Dz.max())
    plt.ylabel('$\Delta p\, (nm)$', fontsize=20)
    # plt.title("(b)", fontsize=24)
    # plt.legend(loc='best')
    plt.tight_layout()
    plt.savefig("Fig4.pdf", dpi=600, bbox_inches='tight', pad_inches=0.1)

    # FIGURA donde se calcula los residuos para la última
    plt.figure(figsize=(6, 6), facecolor='w', edgecolor='k')
    plt.plot(Dz, residuos, 'k', lw=2)
    plt.xlabel('$\Delta z\,(\mu m)$', fontsize=20)
    plt.xlim(Dz.min(), Dz.max())
    plt.ylabel('$residuals\, (nm)$', fontsize=20)
    plt.title("(a)", fontsize=24)
    plt.tight_layout()
    plt.savefig("Fig5aremove.eps",
                dpi=600,
                bbox_inches='tight',
                pad_inches=0.1)

    # FIGURA donde se calcula el histograma de los residuos para la última
    plt.figure(figsize=(6, 6), facecolor='w', edgecolor='k')
    n, bins, patches = plt.hist(residuos, 50,
                                lw=2)  # color='k', histkind='step',
    plt.title("(a)", fontsize=24)
    plt.xlabel('$residuals\, (nm)$', fontsize=20)
    plt.ylabel('$N$', fontsize=20)
    plt.tight_layout()
    plt.savefig("Fig5a.pdf", dpi=600, bbox_inches='tight', pad_inches=0.1)

    print "std: ", sp.std(residuos)
    print "Dz: ", sp.std(residuos) / coefs[0]

    print Periodo_interpolado

    # FIGURA donde se calcula la variación de la std en función del número de ordenes considerado
    plt.figure(figsize=(6, 6), facecolor='w', edgecolor='k')
    plt.plot(ordenes, Residuos, 'k', lw=2)
    plt.title("(b)", fontsize=24)
    plt.xlabel('$number\,of\,minima$', fontsize=20)
    plt.ylabel('$std(residuals) \, (nm)$', fontsize=20)
    plt.tight_layout()
    plt.savefig("Fig5b.pdf", dpi=600, bbox_inches='tight', pad_inches=0.1)


if __name__ == '__main__':
    # test_cargar_archivo()
    # test_mostrar_figura()
    # test_visualizar_variogram()
    # test_ajustar_1_minimum(linea=0, long_variogram=500, period_inicial=42, anchura=7)
    # test_ajustar_n_minimums(linea=0, long_variogram=500, period_inicial=42, anchura=7)
    prueba_5()
    # prueba_6(period_inicial = 45, anchura = 7)
    # prueba_7()
    # Figura3()
    # Figura4_5(ordenes=10)
    plt.show()
