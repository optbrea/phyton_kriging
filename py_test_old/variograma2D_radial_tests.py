#!/usr/bin/env python
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------
# Name:		fuentes2D.py
# Purpose:	 Fuentes de luz 2D
#
# Author:	  Luis Miguel Sanchez Brea
#
# Created:	 2011
# RCS-ID:
# Copyright:
# Licence:	 GPL
#----------------------------------------------------------------------

"""
Clases Scalar_field_XY y Fuentes2D con las fuentes utilizadas para propagacion
"""


from phyton_kriging.variogram2D_radial import *
import scipy.io
from general.funcs_varias import cercano

"""
algunos examples de functiones
"""

def function2D_example_aleatoria(A = 1, p = 0.25, noise = 0, I0 = 0.00001):
	#generamos data "experimentales"
	function = "{}*sp.cos(2*sp.pi*(sp.sqrt(x**2+y**2))/{}))".format(A,p)
	function = "{}*sp.cos(2*sp.pi*x/{})*sp.cos(2*sp.pi*y/{})".format(A,p,p)
	function = "{}*sp.exp(-(x**2+y**2)/(2*{}**2))".format(A,p)
	function = "{}*sp.cos(2*sp.pi*x/{})*sp.cos(2*sp.pi*y/{})".format(A,p,p)

	points = data2D()
	points.senal_aleatoria(xlim=(-5, 5), ylim=(-5, 5), num_data=1000, func=function)
	points.add_noise(sc=(noise,), I=(I0,))
	#points.draw()
	return points


def function2D_example_regular(A = 1, p = 0.5, noise = 0.25, I0 = 0.00001):
	#generamos data "experimentales"
	function = "{}*sp.sin(2*sp.pi*(sp.sqrt(x**2+y**2)/{}))".format(A,p)
	function = "{}*sp.cos(2*sp.pi*x/{})*sp.cos(2*sp.pi*y/{})".format(A,p,p)
	function = "{}*sp.exp(-(x**2+y**2)/(2*{}**2))".format(A,p)

	points = data2D()
	points.regular_signal(xlim=(-7, 7), ylim=(-5, 5), num_data=(40, 40), func=function)
	points.add_noise(sc=(noise,), I=(I0,))
	#points.draw()
	return points


def function_boqui(kind = 'all', num_data = 1500, rango = 1):
	"""
	kind = 'mayores', 'equidistribuidos', 'all'
	"""
	# Creación de data y aconditionamiento
	archivo_entrada='experimental_ARF_Alvarez_Rios.mat'
	data = sp.io.loadmat(archivo_entrada)
	xyz = data['xyz_usar']

	if kind == 'mayores':
		#elijo los rango primeros para utilizar los más significativos
		xs = xyz[:, 0]
		ys = xyz[:, 1]
		I = xyz[:, 2]
		#orden los rango primeros para coger los de mayor intensity
		isort = (-I).argsort()  #los mayores primero
		isort = isort[0:num_data]
		xs = xs[isort]
		ys = ys[isort]
		I = I[isort]

	elif kind == 'equidistribuidos':
		xs = xyz[0:-1:rango, 0]
		ys = xyz[0:-1:rango, 1]
		I = xyz[0:-1:rango, 2]

	else:
		xs = xyz[:, 0]
		ys = xyz[:, 1]
		I = xyz[:, 2]

	points = data2D(xtext="x(mm)", ytext="y(mm)", ztext="z(mm)", title="data para kriging")

	points.generar_senal(x=xs, y=ys, z=I)

	return points


"""
algunos tests
"""

def test_generar_senales():
	"""
	se muestra la generación de data mediante una clase data2D
	"""

	points = function2D_example_regular(A = 1, p = 3, noise = 0.1, I0 = 0.00001)
	points.draw()

	points = function2D_example_aleatoria(A = 1, p = 3, noise = 0.1, I0 = 0.00001)
	points.draw()

	#var = variogram(data = points)
	#var.compute_experimental(ipoints = ix, recortarmaximum = False)
	#var.draw(kind = 'experimental')


def test_ajuste_variogram():
	points = function2D_example_regular(A = 1, p = 8, noise = 0.01, I0 = 0.00001)

	v=variogram2D_radial(points, variogram_gauss) #variogram_gauss variogram_exponencial
	v.experimental(incr_h=0.25)
	v.nugget('nearest')

	param_ini=v.compute_param_ini()


	v.ajuste(param_ini)
	v.draw('all_')



def test_ajuste_teorico():
	"""
	comprobar que sacamos function teórico a través de f y que coincide con la
	parte experimental
	"""

	points = function2D_example_regular(A = 4, p = 1, noise = 0.25, I0 = .2)

	v=variogram2D_radial(points, variogram_gauss) #variogram_gauss variogram_exponencial
	v.experimental(incr_h=0.25)
	v.nugget('nearest')

	param_ini=v.compute_param_ini()
	v.ajuste(param_ini)

	h=v.h
	v_teorico=v.f(h)

	plt.figure()
	plt.plot(v.h,v.variogram_exp,'r',lw=2)
	plt.plot(v.h,v_teorico,'k',lw=2)
	plt.plot(v.h[:v.imax],v.variogram_exp[:v.imax],'b',lw=2)






def test_ARF_boqui():
	"""
	Ejemplo del paper 2015 con Álvarez Rios
	"""
	points = function_boqui(kind = 'all', num_data = 1500, rango = 1)

	v=variogram2D_radial(points,variogram_gauss)
	v.experimental(incr_h=5)
	v.nugget('nearest')

	param_ini=v.compute_param_ini()

	v.ajuste(param_ini)
	v.draw('all_')





def tests_variogram(all_=False):
	tests = {
		'test_generar_senales': 1,
		'test_ajuste_variogram': 1,
		'test_ajuste_teorico': 1,
		'test_ARF_boqui': 1,
			}

	for example in  tests.keys():
		if tests[example] == 1 or all_==True:
			print 'TEST for function ' + example
			eval(example + '()')



if __name__ == '__main__':

	tests_variogram(all_=False)
	plt.show()


