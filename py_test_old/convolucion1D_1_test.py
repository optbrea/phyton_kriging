#!/usr/bin/env python
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------
# Name:		articuloKrigingIrregulares.py
# Purpose:	 Funciones y drawings del artículo de kriging por convolucion
#			  con posiciones irregulares
#
# Author:	  Luis Miguel Sanchez Brea
#
# Created:	 12/08/10
# Licence:	 GPL
#----------------------------------------------------------------------
"""
Funciones y drawings del artículo de kriging convolucion con medidas irregulares
"""

import scipy as sp
import matplotlib.pyplot as plt

import sys

from phyton_kriging.variogram import variogram
from data_generator.data1D import data1D
from phyton_kriging.standard1D import kriging1D
from phyton_kriging.convolucion1D import krigingConvolucion1D

um = 1
mm = 1000 * um
degrees = sp.pi / 180


def completo():

    #Señal
    func1 = 'sp.sin(2*sp.pi*self.x)'
    rangoX = (0, 4)
    num_data = 250

    #generar señal muestra para la comparacion
    s_ideal = data1D()
    s_ideal.regular_signal(xlim=rangoX, num_data=num_data, func=func1)

    sc = .1
    I0 = 0.01
    nSensores = 4 * 15 + 1

    s_real = data1D()
    s_real.regular_signal(xlim=rangoX, num_data=nSensores, func=func1)
    s_real.add_noise(sc=(sc, ), I=(I0, ))

    v = variogram(data=s_real)
    ix = sp.arange(1, round(len(s_real.x)))
    v.compute_experimental(ipoints=ix, recortarmaximum=True)
    v.establecer_parametros(x=s_real.x,
                            s0=sc,
                            c0=1,
                            l0=.25,
                            f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")
    v.draw(kind='ambos')

    calculoTeorico = True
    if calculoTeorico == True:
        paramV = v.ajuste_manual(title='ajuste manual')
        v.establecer_parametros(x=s_real.x,
                                s0=paramV[0],
                                c0=paramV[1],
                                l0=paramV[2],
                                f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")
        v.draw(kind='ambos')
        print 'parametros calculados:', paramV

    #points de sampling
    xsampling = sp.linspace(rangoX[0], rangoX[1], 1000)
    y0 = sp.zeros_like(xsampling)
    m = data1D(x=xsampling, y=y0)

    #kriging convolucion
    kconv = krigingConvolucion1D(sensors=s_real,
                                 variogram=v,
                                 sampling=m,
                                 normalizeDM=True)
    kconv.computeError(sensors=s_real)
    kconv.draw('all')
    plt.xlim(xmin=-0.6, xmax=0.6)
    plt.ylim(ymin=0, ymax=1.0)


def medidasRegularmenteDistribuidas():
    """
	data irregularente distribuidos
	"""

    #Señal
    func1 = 'sp.sin(2*sp.pi*self.x)'
    rangoX = (0, 4)
    num_data = 100

    #generar señal muestra para la comparacion
    s_ideal = data1D()
    s_ideal.regular_signal(xlim=rangoX, num_data=num_data, func=func1)

    sc = .125
    I0 = 0.01
    nSensores = 4 * 11 + 1

    s_real = data1D()
    s_real.regular_signal(xlim=rangoX, num_data=num_data, func=func1)
    s_real.add_noise(sc=(sc, ), I=(I0, ))

    v = variogram(data=s_real)
    ix = sp.arange(1, round(len(s_real.x)))
    v.compute_experimental(ipoints=ix, recortarmaximum=True)
    v.establecer_parametros(x=s_real.x,
                            s0=sc,
                            c0=1,
                            l0=.25,
                            f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")
    v.draw(kind='ambos')

    calculoTeorico = False
    if calculoTeorico == True:
        paramV = v.ajuste_manual(title='ajuste manual')
        v.establecer_parametros(x=s_real.x,
                                s0=paramV[0],
                                c0=paramV[1],
                                l0=paramV[2],
                                f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")
        v.draw(kind='ambos')
        print 'parametros calculados:', paramV

    #generar señal a utilizar
    s_irreg = data1D()
    s_irreg.regular_signal(xlim=rangoX, num_data=nSensores, func=func1)
    s_irreg.senal_irregular(xlim=rangoX,
                            num_data=nSensores,
                            func=func1,
                            edges=True)

    s_irreg.add_noise(sc=(sc, ), I=(I0, ))

    #points de sampling
    xsampling = sp.linspace(rangoX[0], rangoX[1], 250)
    y0 = sp.zeros_like(xsampling)
    m = data1D(x=xsampling, y=y0)

    #kriging convolucion
    kconv = krigingConvolucion1D(sensors=s_irreg,
                                 variogram=v,
                                 sampling=m,
                                 normalizeDM=True)
    kconv.computeError(sensors=s_irreg)

    kconv.draw('all')


def medidasIrregularmenteDistribuidas():
    """
	data irregularente distribuidos
	"""

    #Señal
    func1 = 'sp.sin(2*sp.pi*self.x)'
    rangoX = (0, 4)
    num_data = 100

    #generar señal muestra para la comparacion
    s_ideal = data1D()
    s_ideal.regular_signal(xlim=rangoX, num_data=num_data, func=func1)

    sc = .25
    I0 = 0.01
    nSensores = 4 * 5 + 1

    s_real = data1D()
    s_real.regular_signal(xlim=rangoX, num_data=num_data, func=func1)
    s_real.add_noise(sc=(sc, ), I=(I0, ))

    v = variogram(data=s_real)
    ix = sp.arange(1, round(len(s_real.x)))
    v.compute_experimental(ipoints=ix, recortarmaximum=True)
    v.establecer_parametros(x=s_real.x,
                            s0=sc,
                            c0=1,
                            l0=.25,
                            f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")
    v.draw(kind='ambos')

    calculoTeorico = False
    if calculoTeorico == True:
        paramV = v.ajuste_manual(title='ajuste manual')
        v.establecer_parametros(x=s_real.x,
                                s0=paramV[0],
                                c0=paramV[1],
                                l0=paramV[2],
                                f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")
        v.draw(kind='ambos')
        print 'parametros calculados:', paramV

    #generar señal a utilizar
    s_irreg = data1D()
    s_irreg.senal_irregular(xlim=rangoX,
                            num_data=nSensores,
                            func=func1,
                            edges=True)
    s_irreg.add_noise(sc=(sc, ), I=(I0, ))

    #points de sampling
    xsampling = sp.linspace(rangoX[0], rangoX[1], 250)
    y0 = sp.zeros_like(xsampling)
    m = data1D(x=xsampling, y=y0)

    #kriging convolucion
    kconv = krigingConvolucion1D(sensors=s_irreg,
                                 variogram=v,
                                 sampling=m,
                                 normalizeDM=True)
    kconv.computeError(sensors=s_irreg)

    kconv.draw('all')


def variasMediasUnaPosicion():
    """
	data regularmente distribuidos y varios data en una posición
	"""
    #Señal
    func1 = 'sp.sin(2*sp.pi*self.x)'
    rangoX = (0, 4)
    num_data = 100

    #generar señal muestra para la comparacion
    s_ideal = data1D()
    s_ideal.regular_signal(xlim=rangoX, num_data=num_data, func=func1)

    sc = .5
    I0 = 0.01
    nSensores = 4 * 10 + 1

    s_real = data1D()
    s_real.regular_signal(xlim=rangoX, num_data=num_data, func=func1)
    s_real.add_noise(sc=(sc, ), I=(I0, ))

    v = variogram(data=s_real)
    ix = sp.arange(1, round(len(s_real.x)))
    v.compute_experimental(ipoints=ix, recortarmaximum=True)
    v.establecer_parametros(x=s_real.x,
                            s0=sc,
                            c0=1,
                            l0=.25,
                            f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")
    v.draw(kind='ambos')

    calculoTeorico = False
    if calculoTeorico == True:
        paramV = v.ajuste_manual(title='ajuste manual')
        v.establecer_parametros(x=s_real.x,
                                s0=paramV[0],
                                c0=paramV[1],
                                l0=paramV[2],
                                f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")
        v.draw(kind='ambos')
        print 'parametros calculados:', paramV

    s_irreg = data1D(title="data")
    s_irreg.regular_signal(xlim=rangoX, num_data=nSensores, func=func1)
    s_irreg.add_data(x=[2.],
                     y=[0.],
                     nveces=10,
                     yideal=(0., ),
                     sc=(0., ),
                     I=(0., ),
                     ordenar=True)
    s_irreg.add_noise(sc=(sc, ), I=(I0, ))
    s_irreg.draw(kind='ko')

    #variogram
    ix = sp.arange(1, round(len(s_ideal.x)))
    v = variogram(data=s_irreg)
    v.compute_experimental(ipoints=ix, recortarmaximum=True)
    v.establecer_parametros(x=s_ideal.x,
                            s0=sc,
                            c0=1,
                            l0=.25,
                            f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")
    v.draw(kind='ambos')

    #points de sampling
    xsampling = sp.linspace(rangoX[0], rangoX[1], 1000)
    y0 = sp.zeros_like(xsampling)
    m = data1D(x=xsampling, y=y0)

    #kriging convolucion
    kconv = krigingConvolucion1D(sensors=s_irreg,
                                 variogram=v,
                                 sampling=m,
                                 normalizeDM=True,
                                 kmin=1)
    kconv.computeError(sensors=s_irreg)

    kconv.draw('all')


def removeMedida():
    """
	data irregularente distribuidos
	"""

    #Señal
    func1 = 'sp.sin(2*sp.pi*self.x)'
    rangoX = (-2, 2)
    num_data = 100

    #generar señal muestra para la comparacion
    s_ideal = data1D()
    s_ideal.regular_signal(xlim=rangoX, num_data=num_data, func=func1)

    sc = .5
    I0 = 0.01
    nSensores = 4 * 10 + 1

    s_real = data1D()
    s_real.regular_signal(xlim=rangoX, num_data=num_data, func=func1)
    s_real.add_noise(sc=(sc, ), I=(I0, ))

    v = variogram(data=s_real)
    ix = sp.arange(1, round(len(s_real.x)))
    v.compute_experimental(ipoints=ix, recortarmaximum=True)
    v.establecer_parametros(x=s_real.x,
                            s0=sc,
                            c0=1,
                            l0=.25,
                            f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")
    v.draw(kind='ambos')

    calculoTeorico = False
    if calculoTeorico == True:
        paramV = v.ajuste_manual(title='ajuste manual')
        v.establecer_parametros(x=s_real.x,
                                s0=paramV[0],
                                c0=paramV[1],
                                l0=paramV[2],
                                f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")
        v.draw(kind='ambos')
        print 'parametros calculados:', paramV

    #generar señal a utilizar
    s_irreg = data1D()
    s_irreg.regular_signal(xlim=rangoX, num_data=nSensores, func=func1)
    #s_irreg.senal_irregular(xlim=rangoX, num_data=nSensores, func=func1, edges=True)
    s_irreg.remove_dato(xremove=0.)
    s_irreg.add_noise(sc=(sc, ), I=(I0, ))

    #points de sampling
    xsampling = sp.linspace(rangoX[0], rangoX[1], 250)
    y0 = sp.zeros_like(xsampling)
    m = data1D(x=xsampling, y=y0)

    #kriging convolucion
    kconv = krigingConvolucion1D(sensors=s_irreg,
                                 variogram=v,
                                 sampling=m,
                                 normalizeDM=True,
                                 kmin=0.9)
    kconv.computeError(sensors=s_irreg)

    kconv.draw('all')


def interpolation():
    """
	data irregularente distribuidos
	"""

    #Señal
    func1 = 'sp.sin(2*sp.pi*self.x)'
    rangoX = (-2, 2)
    num_data = 100

    #generar señal muestra para la comparacion
    s_ideal = data1D()
    s_ideal.regular_signal(xlim=rangoX, num_data=num_data, func=func1)

    sc = .5
    I0 = 0.01
    nSensores = 4 * 10 + 1

    s_real = data1D()
    s_real.regular_signal(xlim=rangoX, num_data=num_data, func=func1)
    s_real.add_noise(sc=(sc, ), I=(I0, ))

    v = variogram(data=s_real)
    ix = sp.arange(1, round(len(s_real.x)))
    v.compute_experimental(ipoints=ix, recortarmaximum=True)
    v.establecer_parametros(x=s_real.x,
                            s0=sc,
                            c0=1,
                            l0=.25,
                            f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")
    v.draw(kind='ambos')

    calculoTeorico = False
    if calculoTeorico == True:
        paramV = v.ajuste_manual(title='ajuste manual')
        v.establecer_parametros(x=s_real.x,
                                s0=paramV[0],
                                c0=paramV[1],
                                l0=paramV[2],
                                f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")
        v.draw(kind='ambos')
        print 'parametros calculados:', paramV

    #generar señal a utilizar
    s_irreg = data1D()
    s_irreg.regular_signal(xlim=(rangoX[0] + 0.5, rangoX[1] - 0.5),
                           num_data=nSensores,
                           func=func1)
    s_irreg.add_noise(sc=(sc, ), I=(I0, ))

    #points de sampling
    xsampling = sp.linspace(rangoX[0], rangoX[1], 250)
    y0 = sp.zeros_like(xsampling)
    m = data1D(x=xsampling, y=y0)

    #kriging estandard
    krig = kriging1D(sensors=s_irreg, variogram=v, sampling=m)
    krig.krigingEstandard(filtrado=True)

    #kriging convolucion
    kconv = krigingConvolucion1D(sensors=s_irreg,
                                 variogram=v,
                                 sampling=m,
                                 normalizeDM=True,
                                 kmin=0.9)
    kconv.computeError(sensors=s_irreg)

    kconv.draw('all')


def test_krigConv():

    tests = {
        'completo': 1,
        'medidasRegularmenteDistribuidas': 0,
        'medidasIrregularmenteDistribuidas': 0,
        'variasMediasUnaPosicion': 0,
        'removeMedida': 0,
        'interpolation': 0,
    }

    if tests['completo'] == 1:
        completo()

    if tests['medidasRegularmenteDistribuidas'] == 1:
        medidasRegularmenteDistribuidas()

    if tests['medidasIrregularmenteDistribuidas'] == 1:
        medidasIrregularmenteDistribuidas()

    if tests['variasMediasUnaPosicion'] == 1:
        variasMediasUnaPosicion()

    if tests['removeMedida'] == 1:
        removeMedida()

    if tests['interpolation'] == 1:
        interpolation()


if __name__ == '__main__':
    checkTiempo = 0
    if checkTiempo == 1:
        from cProfile import run
        run('test_krigConv()', sort=True)
    else:
        test_krigConv()

    plt.show()
    raw_input("pulse enter para terminar")
