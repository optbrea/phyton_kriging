# !/usr/bin/env python
#  -*- coding: utf-8 -*-

# ----------------------------------------------------------------------
#  Name:		KrigingEstandard.py
#  Purpose:	 Genera la interpolation del kriging estandard
#
#  Author:	  Luis Miguel Sanchez Brea
#
#  Created:	 11/08/04
#  RCS-ID:
#  Copyright:
#  Licence:	 GPL
# ----------------------------------------------------------------------
"""
Clases Scalar_field_XY y Fuentes2D con las fuentes utilizadas para propagación
"""
from numpy import cos, exp
from phyton_kriging import sp
from phyton_kriging.standard1D import kriging1D, data1D
from phyton_kriging.variogram1D import *


def functionEjemplo1D(A=1.,
                      p=1.,
                      num_data=250,
                      xlim=(-7., 7.),
                      noise=0.1,
                      I0=0.1,
                      kind='irregular',
                      function_ajuste=variogram_gauss):

    function = '{}*sp.cos(2*pi*self.x/{})'.format(A, p)
    function = '{}*sp.exp(-(self.x/(2*{}))**2)'.format(A, p)

    # data "experimentales"
    s = data1D(xtext="x(mm)",
               ytext="I",
               title="function Ejemplo 1D (krigConv)")

    if kind == 'irregular':
        s.senal_irregular(xlim=xlim, num_data=num_data, func=function)
    if kind == 'regular':
        s.regular_signal(xlim=xlim, num_data=num_data, func=function)

    s.add_noise(sc=(noise, ), I=(I0, ))

    # sampling
    xsampling = sp.linspace(xlim[0] - 0.25, xlim[1] + 0.25, 1500)
    m = data1D()
    m.senal_func(
        x=xsampling, func=function
    )  # y en esta función es para compute el error cometido entre la estimación y esto

    # calculo del variogram
    v = variogram(s, function_ajuste)
    i_points = sp.arange(1, len(s.x))
    v.compute_experimental(i_points)
    v.eliminar_fluctuaciones(porcentaje=.35)
    param_ini = v.compute_param_ini()
    print param_ini
    v.ajuste(param_ini, corte=.75)

    return s, m, v


def test_function_example():
    s, m, v = functionEjemplo1D(A=1.,
                                p=1.,
                                num_data=250,
                                xlim=(-8., 8.),
                                noise=0.01,
                                I0=0.01,
                                kind='regular')
    s.draw()
    v.draw('ambos')
    v.draw('ajuste')
    m.draw()
    plt.title('sampling')


def testKriging_1():
    # posiciones y values de los sensors
    s, m, v = functionEjemplo1D(A=1.,
                                p=3.,
                                num_data=250,
                                xlim=(-8., 8.),
                                noise=0.1,
                                I0=0.01,
                                kind='regular')

    # v.popt[0]=0.001
    v.draw('ajuste')
    # kriging
    k = kriging1D(sensors=s, variogram=v, sampling=m)
    k.krigingEstandard(filtrado=True)
    k.draw(kind='error')
    k.draw(kind='kriging')
    k.draw(kind='NEQ')


def test_comparacion_variograms():
    """se comprueba el functionamiento de diferentes kinds de variograms
	en la estimación y en el error

	Tipos_variograms está en variogram1D y es referencia a las functiones
	"""

    for function_variogram in Tipos_variograms:
        s, m, v = functionEjemplo1D(A=1.,
                                    p=1.,
                                    num_data=250,
                                    xlim=(-8., 8.),
                                    noise=0.01,
                                    I0=0.01,
                                    kind='regular',
                                    function_ajuste=function_variogram)
        v.nugget(fill_nugget='nearest')
        v.draw('ajuste')
        plt.title(v.function_ajuste.__name__)
        k = kriging1D(sensors=s, variogram=v, sampling=m)
        k.krigingEstandard(filtrado=True)
        k.draw(kind='error')
        plt.title(v.function_ajuste.__name__)
        k.draw(kind='kriging')
        plt.title(v.function_ajuste.__name__)


def testKriging_DM():
    s, m, v = functionEjemplo1D(A=1.,
                                p=1.,
                                num_data=300,
                                xlim=(-4., 4.),
                                noise=0.5,
                                I0=0.1,
                                kind='regular')

    k = kriging1D(sensors=s, variogram=v, sampling=m)
    k.krigingEstandard()

    s2 = s
    s2.remove_dato(xremove=0.001)
    k2 = kriging1D(sensors=s, variogram=v, sampling=m)
    k2.krigingEstandard()

    DM_kriging = k2.error - k.error
    DM_kriging = DM_kriging / DM_kriging.max()

    data = data1D(xtext="x(mm)", ytext="DM", title=" ")
    data.generar_senal(k.kriging.x, DM_kriging)
    data.draw()


def test_centrar_lambdas():
    """
    toma las lambdas y las centra
	"""
    s, m, v = functionEjemplo1D(A=1.,
                                p=1.,
                                num_data=300,
                                xlim=(-4., 4.),
                                noise=0.5,
                                I0=0.1,
                                kind='regular')

    k = kriging1D(sensors=s, variogram=v, sampling=m)
    k.krigingEstandard()

    X, lambda_media, LAMBDAS = k.center_lambdas()

    plt.figure()
    plt.subplot(2, 1, 1)
    plt.plot(X, LAMBDAS)
    plt.plot(X, lambda_media, 'k', lw=2)
    plt.subplot(2, 1, 2)
    plt.plot(X, lambda_media, 'k', lw=2)


def test_lambdas():
    s, m, v = functionEjemplo1D(A=1.,
                                p=1.,
                                num_data=300,
                                xlim=(-4., 4.),
                                noise=0.5,
                                I0=0.1,
                                kind='regular')

    k = kriging1D(sensors=s, variogram=v, sampling=m)
    k.krigingEstandard()

    k.draw(kind='lambda')
    plt.figure()
    plt.plot(k.lambdas[:, 50], 'k', lw=2)
    return s, m, v, k


def testKriging_3():
    # posiciones y values de los sensors
    A = 1
    p = 1.
    num_data = 250
    xlim = (-3, 8.)
    noise = 0.025
    I0 = 0.00001

    function = '{}*sp.exp(-(self.x/(2*{}))**2)'.format(A, p)

    # sampling
    xsampling = sp.linspace(xlim[0] - 0.25, xlim[1] + 0.25, 300)
    m = data1D()
    m.senal_func(
        x=xsampling, func=function
    )  # y en esta función es para compute el error cometido entre la estimación y esto

    # calculo del variogram

    # data
    s = data1D(xtext="x(mm)",
               ytext="I",
               title="function Ejemplo 1D (krigConv)")
    s.regular_signal(xlim=xlim, num_data=num_data, func=function)
    s.add_noise(sc=(noise, ), I=(I0, ))

    v = variogram(data=s, function_ajuste=variogram_gauss)
    v = variogram(data=s, function_ajuste=variogram_gauss)
    i_points = sp.arange(1, len(s.x))
    v.compute_experimental(i_points)
    # v.eliminar_fluctuaciones(porcentaje=1.)
    param_ini = v.compute_param_ini()
    print param_ini
    v.ajuste(param_ini, corte=1.)

    v.draw('ajuste')
    # kriging
    k = kriging1D(sensors=s, variogram=v, sampling=m)
    k.krigingEstandard(filtrado=True)
    k.draw(kind='error')
    k.draw(kind='kriging')


def tests_kriging(all_=False):

    tests = {
        'test_function_example': 0,
        'testKriging_1': 0,
        'test_comparacion_variograms': 1,
        'testKriging_DM': 0,
        'testKriging_3': 0,
        'test_lambdas': 0,
        'test_centrar_lambdas': 0,
    }

    for example in tests.keys():
        if tests[example] == 1 or all_ == True:
            print 'TEST for function ' + example
            eval(example + '()')


if __name__ == '__main__':
    checkTiempo = 0
    if checkTiempo == 1:
        from cProfile import run
        run('tests_kriging(all=False)', sort=True)
    else:
        tests_kriging(all_=False)
    plt.show()
