#!/usr/local/bin/python
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------
# Name:		kconvolucion.py
# Purpose:	 Kriging por convolucion
#
# Author:	  Luis Miguel Sanchez Brea
#
# Created:	 2011
# RCS-ID:
# Copyright:
# Licence:	 GPL
#----------------------------------------------------------------------


from convolucion2D import *	

"""Kriging por convolucion
- de momento solo implementar 1 variogram
- intentar meter varios, 1 por posicion
"""


def functionEjemplo2D(function, A=1., p=.5, num_data=(10, 10), xlim=(-2., 2.), ylim=(-2., 2.), noise=0.125, I0=0.00001, kind='regular'):

	#data "experimentales"
	s = data2D(xtext="x(mm)", ytext="y(mm)", ztext="z(mm)", title="function Ejemplo 2D (krigConv2)")

	if kind == 'irregular':
		s.senal_aleatoria(xlim=xlim, ylim=ylim, num_data=num_data[0] * num_data[1], func=function)
	if kind == 'regular':
		s.regular_signal(xlim=xlim, ylim=ylim, num_data=num_data, func=function)
	if kind == 'manual':
		s.senal_manual(xlim=xlim, ylim=ylim, num_data=num_data[0] * num_data[1], mask='anillo.bmp', func=function)


	s.add_noise(sc=(noise,), I=(I0,))

	#sampling
	xsampling = sp.linspace(xlim[0], xlim[1], 100)
	ysampling = sp.linspace(ylim[0], ylim[1], 100)

	m = dataMatrix()
	m.generarF(x=xsampling, y=ysampling, func=function)   #y en esta función es para compute el error cometido entre la estimación y esto

	#calculo del variogram
	v = variogram()
	v.establecer_parametros(x=sp.linspace(0, 2, 100), s0=noise, c0=A, l0=p / 2, f_texto="s0**2+c0*(1-sp.exp(-(x/l0)**2))")

	return s, m, v

function1 = 'sp.cos(2*sp.pi*x)*sp.cos(2*sp.pi*y)'
function2 = 'sp.cos(2*sp.pi*sp.sqrt(x**2+y**2))'
function = function1



def testRegular():

	s, m, v = functionEjemplo2D(function=function, A=1., p=1., num_data=(20, 20), xlim=(-.5, .5), ylim=(-.5, .5), noise=0.1, I0=0.00001, kind='regular')
# 	s.draw()
# 	m.draw()
# 	v.draw()

	kconv2 = krigingConvolucion2D(sensors=s, variogram=v, sampling=m, kind='moderno')
	kconv2.computeError(sensors=s)
	kconv2.estimar(sensors=s)

	kconv2.draw('senalReal')
	kconv2.draw('data')
	kconv2.draw('var')
	kconv2.draw('DM')
	kconv2.draw('NEQ')
	kconv2.draw('error')
	kconv2.draw('errorReal')
	kconv2.draw('lambda')
	kconv2.draw('estimacion')
#	kconv2.draw('all')

	return kconv2


def testIrregular():

	s, m, v = functionEjemplo2D(function=function, A=1., p=.5, num_data=(20, 20), xlim=(-.5, .5), ylim=(-.5, .5), noise=0.25, I0=0.00001, kind='irregular')
# 	s.draw()
# 	m.draw()
# 	v.draw()

	kconv2 = krigingConvolucion2D(sensors=s, variogram=v, sampling=m, kind='moderno')
	kconv2.computeError(sensors=s)
	#kconv2.estimar(sensors=s)

	kconv2.draw('senalReal')
	kconv2.draw('data')
	kconv2.draw('error')
	kconv2.draw('errorReal')
	#kconv2.draw('estimacion')
#	kconv2.draw('all')

	return kconv2


def testSinEstimacion():

	s, m, v = functionEjemplo2D(function=function, A=1., p=1., num_data=(10, 10), xlim=(-1., 1.), ylim=(-1., 1.), noise=0.5, I0=0.00001, kind='regular')
# 	s.draw()
# 	m.draw()
# 	v.draw()

	kconv2 = krigingConvolucion2D(sensors=s, variogram=v, sampling=m, kind='moderno')
	kconv2.computeError(sensors=s)

	kconv2.draw('error')

	return kconv2




def test_krigConv2():

		tests = {
			'testRegular':  1,
			'testIrregular':  0,
			'testSinEstimacion':  0,
		}

		if tests['testIrregular'] == 1:
			testIrregular()

		if tests['testRegular'] == 1:
			testRegular()

		if tests['testSinEstimacion'] == 1:
			testSinEstimacion()



if __name__ == '__main__':
	checkTiempo = 0
	if checkTiempo == 1:
		from cProfile import run
		run('test_krigConv2()', sort=True)
	else:
		test_krigConv2()

	plt.show()
